using System;
using System.Collections.Generic;
using System.Data;
using WorkTimesheet.Entities.MetaData;

namespace WorkTimesheet.Repositories.MetaData
{
    public interface INavigationMenuRepository
    {
        NavigationMenu GetByPK(string Id);

        IEnumerable<NavigationMenu> GetAll();
        IEnumerable<NavigationMenu> Insert(DataTable data);
        IEnumerable<NavigationMenu> Update(DataTable data);
        IEnumerable<NavigationMenu> Delete(DataTable data);
        IEnumerable<NavigationMenu> Restore(DataTable data);
    }
}
