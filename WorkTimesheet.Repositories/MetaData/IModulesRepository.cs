using System;
using System.Collections.Generic;
using System.Data;
using WorkTimesheet.Entities.MetaData;

namespace WorkTimesheet.Repositories.MetaData
{
    public interface IModulesRepository
    {
        Modules GetByPK(string Id);

        IEnumerable<Modules> GetAll();
        IEnumerable<Modules> Insert(DataTable data);
        IEnumerable<Modules> Update(DataTable data);
        IEnumerable<Modules> Delete(DataTable data);
        IEnumerable<Modules> Restore(DataTable data);
    }
}
