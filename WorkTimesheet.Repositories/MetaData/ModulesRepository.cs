using System;
using Dapper;
using WorkTimesheet.Entities.MetaData;
using System.Data;
using System.Linq;
using System.Collections.Generic;

namespace WorkTimesheet.Repositories.MetaData
{
    public class ModulesRepository : IModulesRepository
    {
        protected IDbConnection connection = null;
        protected IDbTransaction transaction = null;

        public ModulesRepository(IDbConnection connection, IDbTransaction transaction)
        {
            this.connection = connection;
            this.transaction = transaction;
        }

        public Modules GetByPK(string Id)
        {
            var query = Modules.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "PK");
            param.Add("@Id", Id);
            var list = SqlMapper.Query<Modules>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list.FirstOrDefault();
        }

        public IEnumerable<Modules> GetAll()
        {
            var query = Modules.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "All");
            var list = SqlMapper.Query<Modules>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list;
        }

        public IEnumerable<Modules> GetAllIncludeDeleted()
        {
            var query = Modules.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "ALL_INCL_DELETED");
            var list = SqlMapper.Query<Modules>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list;
        }

        public IEnumerable<Modules> Insert(DataTable data)
        {
            var query = Modules.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "INSERT");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttAspNetModules]"));
            var result = SqlMapper.Query<Modules>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<Modules> Update(DataTable data)
        {
            var query = Modules.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "UPDATE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttAspNetModules]"));
            var result = SqlMapper.Query<Modules>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<Modules> Delete(DataTable data)
        {
            var query = Modules.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "DELETE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttAspNetModules]"));
            var result = SqlMapper.Query<Modules>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<Modules> Restore(DataTable data)
        {
            var query = Modules.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "RESTORE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttAspNetModules]"));
            var result = SqlMapper.Query<Modules>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }
    }
}
