﻿using System;
using Dapper;
using WorkTimesheet.Entities.Timesheet;
using System.Data;
using System.Linq;
using System.Collections.Generic;

namespace WorkTimesheet.Repositories.Timesheet
{
    public class TimeTrackerRepository : ITimeTrackerRepository
    {
        protected IDbConnection connection = null;
        protected IDbTransaction transaction = null;

        public TimeTrackerRepository(IDbConnection connection, IDbTransaction transaction)
        {
            this.connection = connection;
            this.transaction = transaction;
        }

        public TimeTracker GetByPK(string Id)
        {
            var query = TimeTracker.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "PK");
            param.Add("@Id", Id);
            var list = SqlMapper.Query<TimeTracker>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list.FirstOrDefault();
        }

        public IEnumerable<TimeTracker> GetAll()
        {
            var query = TimeTracker.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "All");
            var list = SqlMapper.Query<TimeTracker>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list;
        }

        public IEnumerable<TimeTracker> GetAllIncludeDeleted()
        {
            var query = TimeTracker.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "ALL_INCL_DELETED");
            var list = SqlMapper.Query<TimeTracker>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list;
        }

        public IEnumerable<TimeTracker> Insert(DataTable data)
        {
            var query = TimeTracker.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "INSERT");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttTimeTracker]"));
            var result = SqlMapper.Query<TimeTracker>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<TimeTracker> Update(DataTable data)
        {
            var query = TimeTracker.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "UPDATE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttTimeTracker]"));
            var result = SqlMapper.Query<TimeTracker>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<TimeTracker> Delete(DataTable data)
        {
            var query = TimeTracker.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "DELETE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttTimeTracker]"));
            var result = SqlMapper.Query<TimeTracker>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<TimeTracker> Restore(DataTable data)
        {
            var query = TimeTracker.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "RESTORE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttTimeTracker]"));
            var result = SqlMapper.Query<TimeTracker>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }
    }
}
