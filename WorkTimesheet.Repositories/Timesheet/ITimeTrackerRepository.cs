﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using WorkTimesheet.Entities.Timesheet;

namespace WorkTimesheet.Repositories.Timesheet
{
    public interface ITimeTrackerRepository
    {
        TimeTracker GetByPK(string Id);

        IEnumerable<TimeTracker> GetAll();
        IEnumerable<TimeTracker> Insert(DataTable data);
        IEnumerable<TimeTracker> Update(DataTable data);
        IEnumerable<TimeTracker> Delete(DataTable data);
        IEnumerable<TimeTracker> Restore(DataTable data);
    }
}
