﻿using WorkTimesheet.Repositories.Masters;
using System;
using System.Collections.Generic;
using System.Text;
using WorkTimesheet.Repositories.MetaData;
using WorkTimesheet.Repositories.Operations;
using WorkTimesheet.Repositories.ProjectTask;
using WorkTimesheet.Repositories.Timesheet;

namespace WorkTimesheet.Repositories
{
    public interface IUnitOfWorkDapper
    {
        INavigationMenuRepository _NavigationMenuRepository { get; }
        IRoleMenuPermissionRepository _RoleMenuPermissionRepository { get; }
        IModulesRepository _ModulesRepository { get; }
        IUsersRepository _UsersRepository { get; }
        IRolesRepository _RolesRepository { get; }
        IUserRolesRepository _UserRolesRepository { get; }
        IUserGroupsRepository _UserGroupsRepository { get; }
        IRolesClaimsRepository _RolesClaimsRepository { get; }
        IListOfValuesRepository _ListOfValuesRepository { get; }
        IOrganizationUnitsRepository _OrganizationUnitsRepository { get; }
        IGroupsRepository _GroupsRepository { get; }
        IProjectsRepository _ProjectsRepository { get; }
        IProjectTasksRepository _ProjectTasksRepository { get; }
        ITimeTrackerRepository _TimeTrackerRepository { get; }
        IProjectTaskHistoryRepository _ProjectTaskHistoryRepository { get; }
        bool Commit();
    }
}
