using System;
using Dapper;
using WorkTimesheet.Entities.Operations;
using System.Data;
using System.Linq;
using System.Collections.Generic;

namespace WorkTimesheet.Repositories.Operations
{
    public class OrganizationUnitsRepository : IOrganizationUnitsRepository
    {
        protected IDbConnection connection = null;
        protected IDbTransaction transaction = null;

        public OrganizationUnitsRepository(IDbConnection connection, IDbTransaction transaction)
        {
            this.connection = connection;
            this.transaction = transaction;
        }

        public OrganizationUnits GetByPK(string Id)
        {
            var query = OrganizationUnits.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "PK");
            param.Add("@Id", Id);
            var list = SqlMapper.Query<OrganizationUnits>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list.FirstOrDefault();
        }

        public IEnumerable<OrganizationUnits> GetAll()
        {
            var query = OrganizationUnits.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "All");
            var list = SqlMapper.Query<OrganizationUnits>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list;
        }

        public IEnumerable<OrganizationUnits> GetAllIncludeDeleted()
        {
            var query = OrganizationUnits.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "ALL_INCL_DELETED");
            var list = SqlMapper.Query<OrganizationUnits>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list;
        }

        public IEnumerable<OrganizationUnits> Insert(DataTable data)
        {
            var query = OrganizationUnits.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "INSERT");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttAspNetOrganizationUnits]"));
            var result = SqlMapper.Query<OrganizationUnits>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<OrganizationUnits> Update(DataTable data)
        {
            var query = OrganizationUnits.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "UPDATE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttAspNetOrganizationUnits]"));
            var result = SqlMapper.Query<OrganizationUnits>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<OrganizationUnits> Delete(DataTable data)
        {
            var query = OrganizationUnits.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "DELETE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttAspNetOrganizationUnits]"));
            var result = SqlMapper.Query<OrganizationUnits>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<OrganizationUnits> Restore(DataTable data)
        {
            var query = OrganizationUnits.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "RESTORE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttAspNetOrganizationUnits]"));
            var result = SqlMapper.Query<OrganizationUnits>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }
    }
}
