using System;
using System.Collections.Generic;
using System.Data;
using WorkTimesheet.Entities.Operations;

namespace WorkTimesheet.Repositories.Operations
{
    public interface IRoleMenuPermissionRepository
    {
        RoleMenuPermission GetByPK(int Id);

        IEnumerable<RoleMenuPermission> GetAll();
        IEnumerable<RoleMenuPermission> Insert(DataTable data);
        IEnumerable<RoleMenuPermission> Update(DataTable data);
        IEnumerable<RoleMenuPermission> Delete(DataTable data);
        IEnumerable<RoleMenuPermission> Restore(DataTable data);
    }
}
