using System;
using Dapper;
using WorkTimesheet.Entities.Operations;
using System.Data;
using System.Linq;
using System.Collections.Generic;

namespace WorkTimesheet.Repositories.Operations
{
    public class RoleMenuPermissionRepository : IRoleMenuPermissionRepository
    {
        protected IDbConnection connection = null;
        protected IDbTransaction transaction = null;

        public RoleMenuPermissionRepository(IDbConnection connection, IDbTransaction transaction)
        {
            this.connection = connection;
            this.transaction = transaction;
        }

        public RoleMenuPermission GetByPK(int Id)
        {
            var query = RoleMenuPermission.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "PK");
            param.Add("@Id", Id);
            var list = SqlMapper.Query<RoleMenuPermission>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list.FirstOrDefault();
        }

        public IEnumerable<RoleMenuPermission> GetAll()
        {
            var query = RoleMenuPermission.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "All");
            var list = SqlMapper.Query<RoleMenuPermission>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list;
        }

        public IEnumerable<RoleMenuPermission> GetAllIncludeDeleted()
        {
            var query = RoleMenuPermission.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "ALL_INCL_DELETED");
            var list = SqlMapper.Query<RoleMenuPermission>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list;
        }

        public IEnumerable<RoleMenuPermission> Insert(DataTable data)
        {
            var query = RoleMenuPermission.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "INSERT");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttAspNetRoleMenuPermission]"));
            var result = SqlMapper.Query<RoleMenuPermission>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<RoleMenuPermission> Update(DataTable data)
        {
            var query = RoleMenuPermission.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "UPDATE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttAspNetRoleMenuPermission]"));
            var result = SqlMapper.Query<RoleMenuPermission>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<RoleMenuPermission> Delete(DataTable data)
        {
            var query = RoleMenuPermission.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "DELETE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttAspNetRoleMenuPermission]"));
            var result = SqlMapper.Query<RoleMenuPermission>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<RoleMenuPermission> Restore(DataTable data)
        {
            var query = RoleMenuPermission.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "RESTORE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttAspNetRoleMenuPermission]"));
            var result = SqlMapper.Query<RoleMenuPermission>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }
    }
}
