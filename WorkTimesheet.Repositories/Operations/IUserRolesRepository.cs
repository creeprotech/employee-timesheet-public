using System;
using System.Collections.Generic;
using System.Data;
using WorkTimesheet.Entities.Operations;

namespace WorkTimesheet.Repositories.Operations
{
    public interface IUserRolesRepository
    {
        UserRoles GetByPK(int Id);

        IEnumerable<UserRoles> GetAll();
        IEnumerable<UserRoles> Insert(DataTable data);
        IEnumerable<UserRoles> Update(DataTable data);
        IEnumerable<UserRoles> Delete(DataTable data);
        IEnumerable<UserRoles> Restore(DataTable data);
    }
}
