using System;
using Dapper;
using WorkTimesheet.Entities.Operations;
using System.Data;
using System.Linq;
using System.Collections.Generic;

namespace WorkTimesheet.Repositories.Operations
{
    public class GroupsRepository : IGroupsRepository
    {
        protected IDbConnection connection = null;
        protected IDbTransaction transaction = null;

        public GroupsRepository(IDbConnection connection, IDbTransaction transaction)
        {
            this.connection = connection;
            this.transaction = transaction;
        }

        public Groups GetByPK(string Id)
        {
            var query = Groups.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "PK");
            param.Add("@Id", Id);
            var list = SqlMapper.Query<Groups>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list.FirstOrDefault();
        }

        public IEnumerable<Groups> GetAll()
        {
            var query = Groups.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "All");
            var list = SqlMapper.Query<Groups>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list;
        }

        public IEnumerable<Groups> GetAllIncludeDeleted()
        {
            var query = Groups.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "ALL_INCL_DELETED");
            var list = SqlMapper.Query<Groups>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list;
        }

        public IEnumerable<Groups> Insert(DataTable data)
        {
            var query = Groups.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "INSERT");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttAspNetGroups]"));
            var result = SqlMapper.Query<Groups>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<Groups> Update(DataTable data)
        {
            var query = Groups.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "UPDATE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttAspNetGroups]"));
            var result = SqlMapper.Query<Groups>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<Groups> Delete(DataTable data)
        {
            var query = Groups.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "DELETE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttAspNetGroups]"));
            var result = SqlMapper.Query<Groups>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<Groups> Restore(DataTable data)
        {
            var query = Groups.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "RESTORE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttAspNetGroups]"));
            var result = SqlMapper.Query<Groups>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }
    }
}
