using System;
using System.Collections.Generic;
using System.Data;
using WorkTimesheet.Entities.Operations;

namespace WorkTimesheet.Repositories.Operations
{
    public interface IOrganizationUnitsRepository
    {
        OrganizationUnits GetByPK(string Id);

        IEnumerable<OrganizationUnits> GetAll();
        IEnumerable<OrganizationUnits> Insert(DataTable data);
        IEnumerable<OrganizationUnits> Update(DataTable data);
        IEnumerable<OrganizationUnits> Delete(DataTable data);
        IEnumerable<OrganizationUnits> Restore(DataTable data);
    }
}
