using System;
using System.Collections.Generic;
using System.Data;
using WorkTimesheet.Entities.Operations;

namespace WorkTimesheet.Repositories.Operations
{
    public interface IRolesClaimsRepository
    {
        RoleClaims GetByPK(int Id);

        IEnumerable<RoleClaims> GetAll();
        IEnumerable<RoleClaims> Insert(DataTable data);
        IEnumerable<RoleClaims> Update(DataTable data);
        IEnumerable<RoleClaims> Delete(DataTable data);
        IEnumerable<RoleClaims> Restore(DataTable data);
    }
}
