using System;
using System.Collections.Generic;
using System.Data;
using WorkTimesheet.Entities.Operations;

namespace WorkTimesheet.Repositories.Operations
{
    public interface IUsersRepository
    {
        Users GetByPK(string Id);

        IEnumerable<Users> GetAll();
        IEnumerable<Users> Insert(DataTable data);
        IEnumerable<Users> Update(DataTable data);
        IEnumerable<Users> Delete(DataTable data);
        IEnumerable<Users> Restore(DataTable data);
    }
}
