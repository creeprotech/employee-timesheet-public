using System;
using System.Collections.Generic;
using System.Data;
using WorkTimesheet.Entities.Operations;

namespace WorkTimesheet.Repositories.Operations
{
    public interface IGroupsRepository
    {
        Groups GetByPK(string Id);

        IEnumerable<Groups> GetAll();
        IEnumerable<Groups> Insert(DataTable data);
        IEnumerable<Groups> Update(DataTable data);
        IEnumerable<Groups> Delete(DataTable data);
        IEnumerable<Groups> Restore(DataTable data);
    }
}
