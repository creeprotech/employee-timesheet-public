using System;
using System.Collections.Generic;
using System.Data;
using WorkTimesheet.Entities.Operations;

namespace WorkTimesheet.Repositories.Operations
{
    public interface IRolesRepository
    {
        Roles GetByPK(string Id);

        IEnumerable<Roles> GetAll();
        IEnumerable<Roles> Insert(DataTable data);
        IEnumerable<Roles> Update(DataTable data);
        IEnumerable<Roles> Delete(DataTable data);
        IEnumerable<Roles> Restore(DataTable data);
    }
}
