using System;
using System.Collections.Generic;
using System.Data;
using WorkTimesheet.Entities.Operations;

namespace WorkTimesheet.Repositories.Operations
{
    public interface IUserGroupsRepository
    {
        UserGroups GetByPK(int Id);

        IEnumerable<UserGroups> GetAll();
        IEnumerable<UserGroups> Insert(DataTable data);
        IEnumerable<UserGroups> Update(DataTable data);
        IEnumerable<UserGroups> Delete(DataTable data);
        IEnumerable<UserGroups> Restore(DataTable data);
    }
}
