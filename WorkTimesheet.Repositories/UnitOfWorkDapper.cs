﻿using WorkTimesheet.Repositories.Masters;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WorkTimesheet.Repositories.MetaData;
using WorkTimesheet.Repositories.Operations;
using WorkTimesheet.Repositories.ProjectTask;
using WorkTimesheet.Repositories.Timesheet;

namespace WorkTimesheet.Repositories
{
    public class UnitOfWorkDapper : IUnitOfWorkDapper
    {
        private IDbConnection _connection;
        private IDbTransaction _transaction;
        private INavigationMenuRepository _navigationMenuRepository;
        private IRoleMenuPermissionRepository _roleMenuPermissionRepository;
        private IModulesRepository _modulesRepository;
        private IUsersRepository _usersRepository;
        private IRolesRepository _rolesRepository;
        private IUserRolesRepository _userRolesRepository;
        private IUserGroupsRepository _userGroupsRepository;
        private IRolesClaimsRepository _rolesClaimsRepository;
        private IListOfValuesRepository _listOfValuesRepository;
        private IOrganizationUnitsRepository _organizationUnitsRepository;
        private IGroupsRepository _groupsRepository;
        private IProjectsRepository _projectsRepository;
        private IProjectTasksRepository _projectTasksRepository;
        private ITimeTrackerRepository _timeTrackerRepository;
        private IProjectTaskHistoryRepository _projectTaskHistoryRepository;

        private bool _disposed;
        public UnitOfWorkDapper(IConfiguration configuration)
        {
            _connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection"));
            _connection.Open();
            _transaction = _connection.BeginTransaction();
        }

        public INavigationMenuRepository _NavigationMenuRepository => _navigationMenuRepository ??= new NavigationMenuRepository(_connection, _transaction);
        public IRoleMenuPermissionRepository _RoleMenuPermissionRepository => _roleMenuPermissionRepository ??= new RoleMenuPermissionRepository(_connection, _transaction);
        public IModulesRepository _ModulesRepository => _modulesRepository ??= new ModulesRepository(_connection, _transaction);
        public IUsersRepository _UsersRepository => _usersRepository ??= new UsersRepository(_connection, _transaction);
        public IRolesRepository _RolesRepository => _rolesRepository ??= new RolesRepository(_connection, _transaction);
        public IUserRolesRepository _UserRolesRepository => _userRolesRepository ??= new UserRolesRepository(_connection, _transaction);
        public IUserGroupsRepository _UserGroupsRepository => _userGroupsRepository ??= new UserGroupsRepository(_connection, _transaction);
        public IRolesClaimsRepository _RolesClaimsRepository => _rolesClaimsRepository ??= new RolesClaimsRepository(_connection, _transaction);
        public IListOfValuesRepository _ListOfValuesRepository => _listOfValuesRepository ??= new ListOfValuesRepository(_connection, _transaction);
        public IOrganizationUnitsRepository _OrganizationUnitsRepository => _organizationUnitsRepository ??= new OrganizationUnitsRepository(_connection, _transaction);
        public IGroupsRepository _GroupsRepository => _groupsRepository ??= new GroupsRepository(_connection, _transaction);
        public IProjectsRepository _ProjectsRepository => _projectsRepository ??= new ProjectsRepository(_connection, _transaction);
        public IProjectTasksRepository _ProjectTasksRepository => _projectTasksRepository ??= new ProjectTasksRepository(_connection, _transaction);
        public ITimeTrackerRepository _TimeTrackerRepository => _timeTrackerRepository ??= new TimeTrackerRepository(_connection, _transaction);
        public IProjectTaskHistoryRepository _ProjectTaskHistoryRepository => _projectTaskHistoryRepository ??= new ProjectTaskHistoryRepository(_connection, _transaction);

        public bool Commit()
        {
            bool returnValue = true;
            try
            {
                _transaction.Commit();
            }
            catch
            {
                returnValue = false;
                _transaction.Rollback();
            }
            finally
            {
                _transaction.Dispose();
                _connection.Close();
                ResetRepositories();
            }
            return returnValue;
        }

        private void ResetRepositories()
        {
            _navigationMenuRepository = null;
            _roleMenuPermissionRepository = null;
            _modulesRepository = null;
            _usersRepository = null;
            _rolesRepository = null;
            _userRolesRepository = null;
            _userGroupsRepository = null;
            _rolesClaimsRepository = null;
            _listOfValuesRepository = null;
            _organizationUnitsRepository = null;
            _groupsRepository = null;
            _projectsRepository = null;
            _projectTasksRepository = null;
            _timeTrackerRepository = null;
            _projectTaskHistoryRepository = null;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_transaction != null)
                    {
                        _transaction.Dispose();
                        _transaction = null;
                    }
                    if (_connection != null)
                    {
                        _connection.Dispose();
                        _connection = null;
                    }
                }
                _disposed = true;
            }
        }

        ~UnitOfWorkDapper()
        {
            Dispose(false);
        }
    }
}
