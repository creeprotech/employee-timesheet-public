using System;
using Dapper;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using WorkTimesheet.Entities.Masters;

namespace WorkTimesheet.Repositories.Masters
{
    public class ListOfValuesRepository : IListOfValuesRepository
    {
        protected IDbConnection connection = null;
        protected IDbTransaction transaction = null;

        public ListOfValuesRepository(IDbConnection connection, IDbTransaction transaction)
        {
            this.connection = connection;
            this.transaction = transaction;
        }

        public ListOfValues GetByPK(int Id)
        {
            var query = ListOfValues.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "PK");
            param.Add("@Id", Id);
            var list = SqlMapper.Query<ListOfValues>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list.FirstOrDefault();
        }

        public IEnumerable<ListOfValues> GetAll()
        {
            var query = ListOfValues.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "All");
            var list = SqlMapper.Query<ListOfValues>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list;
        }

        public IEnumerable<ListOfValues> GetAllIncludeDeleted()
        {
            var query = ListOfValues.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "ALL_INCL_DELETED");
            var list = SqlMapper.Query<ListOfValues>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list;
        }

        public IEnumerable<ListOfValues> Insert(DataTable data)
        {
            var query = ListOfValues.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "INSERT");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttListOfValues]"));
            var result = SqlMapper.Query<ListOfValues>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<ListOfValues> Update(DataTable data)
        {
            var query = ListOfValues.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "UPDATE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttListOfValues]"));
            var result = SqlMapper.Query<ListOfValues>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<ListOfValues> Delete(DataTable data)
        {
            var query = ListOfValues.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "DELETE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttListOfValues]"));
            var result = SqlMapper.Query<ListOfValues>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<ListOfValues> Restore(DataTable data)
        {
            var query = ListOfValues.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "RESTORE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttListOfValues]"));
            var result = SqlMapper.Query<ListOfValues>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }
    }
}
