using System;
using System.Collections.Generic;
using System.Data;
using WorkTimesheet.Entities.Masters;

namespace WorkTimesheet.Repositories.Masters
{
    public interface IListOfValuesRepository
    {
        ListOfValues GetByPK(int Id);

        IEnumerable<ListOfValues> GetAll();
        IEnumerable<ListOfValues> Insert(DataTable data);
        IEnumerable<ListOfValues> Update(DataTable data);
        IEnumerable<ListOfValues> Delete(DataTable data);
        IEnumerable<ListOfValues> Restore(DataTable data);
    }
}
