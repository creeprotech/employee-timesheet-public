﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using WorkTimesheet.Entities.ProjectTask;

namespace WorkTimesheet.Repositories.ProjectTask
{
    public interface IProjectTaskHistoryRepository
    {
        ProjectTaskHistory GetByPK(string Id);

        IEnumerable<ProjectTaskHistory> GetAll();
        IEnumerable<ProjectTaskHistory> Insert(DataTable data);
        IEnumerable<ProjectTaskHistory> Update(DataTable data);
        IEnumerable<ProjectTaskHistory> Delete(DataTable data);
        IEnumerable<ProjectTaskHistory> Restore(DataTable data);
    }
}
