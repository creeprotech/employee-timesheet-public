using System;
using System.Collections.Generic;
using System.Data;
using WorkTimesheet.Entities.ProjectTask;

namespace WorkTimesheet.Repositories.ProjectTask
{
    public interface IProjectsRepository
    {
        Projects GetByPK(string Id);

        IEnumerable<Projects> GetAll();
        IEnumerable<Projects> Insert(DataTable data);
        IEnumerable<Projects> Update(DataTable data);
        IEnumerable<Projects> Delete(DataTable data);
        IEnumerable<Projects> Restore(DataTable data);
    }
}
