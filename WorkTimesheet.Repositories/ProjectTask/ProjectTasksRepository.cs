﻿using System;
using Dapper;
using WorkTimesheet.Entities.ProjectTask;
using System.Data;
using System.Linq;
using System.Collections.Generic;

namespace WorkTimesheet.Repositories.ProjectTask
{
    public class ProjectTasksRepository : IProjectTasksRepository
    {
        protected IDbConnection connection = null;
        protected IDbTransaction transaction = null;

        public ProjectTasksRepository(IDbConnection connection, IDbTransaction transaction)
        {
            this.connection = connection;
            this.transaction = transaction;
        }

        public ProjectTasks GetByPK(string Id)
        {
            var query = ProjectTasks.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "PK");
            param.Add("@Id", Id);
            var list = SqlMapper.Query<ProjectTasks>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list.FirstOrDefault();
        }

        public IEnumerable<ProjectTasks> GetAll()
        {
            var query = ProjectTasks.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "All");
            var list = SqlMapper.Query<ProjectTasks>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list;
        }

        public IEnumerable<ProjectTasks> GetByUserId(string UserId)
        {
            var query = ProjectTasks.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "GET_BY_USERID");
            param.Add("@UserId", UserId);
            var list = SqlMapper.Query<ProjectTasks>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list;
        }

        public IEnumerable<ProjectTasks> GetAllIncludeDeleted()
        {
            var query = ProjectTasks.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "ALL_INCL_DELETED");
            var list = SqlMapper.Query<ProjectTasks>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list;
        }

        public IEnumerable<ProjectTasks> Insert(DataTable data)
        {
            var query = ProjectTasks.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "INSERT");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttProjectTasks]"));
            var result = SqlMapper.Query<ProjectTasks>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<ProjectTasks> Update(DataTable data)
        {
            var query = ProjectTasks.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "UPDATE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttProjectTasks]"));
            var result = SqlMapper.Query<ProjectTasks>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<ProjectTasks> Delete(DataTable data)
        {
            var query = ProjectTasks.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "DELETE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttProjectTasks]"));
            var result = SqlMapper.Query<ProjectTasks>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<ProjectTasks> Restore(DataTable data)
        {
            var query = ProjectTasks.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "RESTORE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttProjectTasks]"));
            var result = SqlMapper.Query<ProjectTasks>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }
    }
}
