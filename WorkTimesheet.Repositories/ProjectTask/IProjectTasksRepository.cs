﻿using System;
using System.Collections.Generic;
using System.Data;
using WorkTimesheet.Entities.ProjectTask;

namespace WorkTimesheet.Repositories.ProjectTask
{
    public interface IProjectTasksRepository
    {
        ProjectTasks GetByPK(string Id);

        IEnumerable<ProjectTasks> GetAll();
        IEnumerable<ProjectTasks> GetByUserId(string UserId);
        IEnumerable<ProjectTasks> Insert(DataTable data);
        IEnumerable<ProjectTasks> Update(DataTable data);
        IEnumerable<ProjectTasks> Delete(DataTable data);
        IEnumerable<ProjectTasks> Restore(DataTable data);
    }
}
