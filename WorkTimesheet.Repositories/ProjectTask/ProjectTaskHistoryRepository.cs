﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WorkTimesheet.Entities.ProjectTask;

namespace WorkTimesheet.Repositories.ProjectTask
{
    public class ProjectTaskHistoryRepository : IProjectTaskHistoryRepository
    {
        protected IDbConnection connection = null;
        protected IDbTransaction transaction = null;

        public ProjectTaskHistoryRepository(IDbConnection connection, IDbTransaction transaction)
        {
            this.connection = connection;
            this.transaction = transaction;
        }

        public ProjectTaskHistory GetByPK(string Id)
        {
            var query = ProjectTaskHistory.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "PK");
            param.Add("@Id", Id);
            var list = SqlMapper.Query<ProjectTaskHistory>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list.FirstOrDefault();
        }

        public IEnumerable<ProjectTaskHistory> GetAll()
        {
            var query = ProjectTaskHistory.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "All");
            var list = SqlMapper.Query<ProjectTaskHistory>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list;
        }

        public IEnumerable<ProjectTaskHistory> GetByUserId(string UserId)
        {
            var query = ProjectTaskHistory.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "GET_BY_USERID");
            param.Add("@UserId", UserId);
            var list = SqlMapper.Query<ProjectTaskHistory>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list;
        }

        public IEnumerable<ProjectTaskHistory> GetAllIncludeDeleted()
        {
            var query = ProjectTaskHistory.PROC_NAME_GET;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "ALL_INCL_DELETED");
            var list = SqlMapper.Query<ProjectTaskHistory>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return list;
        }

        public IEnumerable<ProjectTaskHistory> Insert(DataTable data)
        {
            var query = ProjectTaskHistory.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "INSERT");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttProjectTaskHistory]"));
            var result = SqlMapper.Query<ProjectTaskHistory>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<ProjectTaskHistory> Update(DataTable data)
        {
            var query = ProjectTaskHistory.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "UPDATE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttProjectTaskHistory]"));
            var result = SqlMapper.Query<ProjectTaskHistory>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<ProjectTaskHistory> Delete(DataTable data)
        {
            var query = ProjectTaskHistory.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "DELETE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttProjectTaskHistory]"));
            var result = SqlMapper.Query<ProjectTaskHistory>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }

        public IEnumerable<ProjectTaskHistory> Restore(DataTable data)
        {
            var query = ProjectTaskHistory.PROC_NAME_STORE;
            var param = new DynamicParameters();
            param.Add("@ProcOption", "RESTORE");
            param.Add("@Data", data.AsTableValuedParameter("[dbo].[ttProjectTaskHistory]"));
            var result = SqlMapper.Query<ProjectTaskHistory>(connection, query, param, transaction: this.transaction, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            return result;
        }
    }
}
