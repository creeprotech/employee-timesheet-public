(function () {
	"use strict";
	console.log('Welcome to secure biometrics sdn bhd');
	var treeviewMenu = $('.app-menu');

	// Toggle Sidebar
	$('[data-toggle="sidebar"]').click(function(event) {
		event.preventDefault();
		$('.app').toggleClass('sidenav-toggled');
	});

	// Activate sidebar treeview toggle
	$("[data-toggle='treeview']").click(function (event) {
		console.log('data-toggle = treeview');
		event.preventDefault();
		if (!$(this).parent().hasClass('is-expanded')) {
			console.log('data-toggle = treeview is-expanded');
			treeviewMenu.find("[data-toggle='treeview']").parent().removeClass('is-expanded');
		}
		$(this).parent().toggleClass('is-expanded');
	});
    	
	// Set initial active toggle
	$("[data-toggle='treeview.'].is-expanded").parent().toggleClass('is-expanded');

	//Activate bootstrip tooltips
    $("[data-toggle='tooltip']").tooltip();

    $("a.treeview-item.active").parent().parent(".treeview-menu").parent().toggleClass('is-expanded');

})();

var url = window.location.href.split('/');
var baseUrl = url[0] + '//' + url[2];

var BASE_URL = 'https://localhost:44366';

ko.validation.registerExtenders();

ko.validation.init({
    registerExtenders: true,
    messagesOnModified: true,
    insertMessages: true,
    parseInputAttributes: true,
    messageTemplate: null,
    decorateInputElement: true,
    errorElementClass: 'is-invalid',
    errorsAsTitle: false,
    errorMessageClass: 'invalid-feedback'
}, true);

ko.bindingHandlers.dataTablesForEach = {
    page: 0,
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var binding = ko.utils.unwrapObservable(valueAccessor());

        ko.unwrap(binding.data);

        if (binding.options.paging) {
            binding.data.subscribe(function (changes) {
                var table = $(element).closest('table').DataTable();
                ko.bindingHandlers.dataTablesForEach.page = table.page();
                table.destroy();
            }, null, 'arrayChange');
        }

        var nodes = Array.prototype.slice.call(element.childNodes, 0);
        ko.utils.arrayForEach(nodes, function (node) {
            if (node && node.nodeType !== 1) {
                node.parentNode.removeChild(node);
            }
        });

        return ko.bindingHandlers.foreach.init(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
    },
    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var binding = ko.utils.unwrapObservable(valueAccessor()),
            key = 'DataTablesForEach_Initialized';

        ko.unwrap(binding.data);

        var table;
        if (!binding.options.paging) {
            table = $(element).closest('table').DataTable();
            table.destroy();
        }

        ko.bindingHandlers.foreach.update(element, valueAccessor, allBindings, viewModel, bindingContext);

        table = $(element).closest('table').DataTable(binding.options);

        if (binding.options.paging) {
            if (table.page.info().pages - ko.bindingHandlers.dataTablesForEach.page === 0) {
                table.page(--ko.bindingHandlers.dataTablesForEach.page).draw(false);
            } else {
                table.page(ko.bindingHandlers.dataTablesForEach.page).draw(false);
            }
        }

        if (!ko.utils.domData.get(element, key) && (binding.data || binding.length)) {
            ko.utils.domData.set(element, key, true);
        }

        return {
            controlsDescendantBindings: true
        };
    }
};

$.fn.bootstrapDP = $.fn.datepicker;
ko.bindingHandlers.bootstrapDP = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        var options = allBindingsAccessor().datepickerOptions || {};
        $(element).bootstrapDP(options).on("changeDate", function (ev) {
            var observable = valueAccessor();
            try {
                observable($(element).val());
            }
            catch (ex) { }
        });
    },
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).bootstrapDP("update", value);
    }
};

ko.validation.rules['checked'] = {
    validator: function (value) {
        console.log(value);
        if (!value) {
            return false;
        }
        return true;
    }
};

ko.bindingHandlers.multiselect2 = {
    init: function (el, valueAccessor, allBindingsAccessor, viewModel) {
        ko.utils.domNodeDisposal.addDisposeCallback(el, function () {
            $(el).select2('destroy');
        });

        var allBindings = allBindingsAccessor(),
            select2 = ko.utils.unwrapObservable(allBindings.select2);
        $(el).select2(select2);
    },
    update: function (el, valueAccessor, allBindingsAccessor, viewModel) {
        var allBindings = allBindingsAccessor();
        if ("value" in allBindings) {
            if ((allBindings.select2.multiple || el.multiple) && allBindings.value().constructor != Array) {
                $(el).val(allBindings.value().split(',')).trigger('change');
            }
            else {
                $(el).val(allBindings.value()).trigger('change');
            }
        } else if ("selectedOptions" in allBindings) {
            var converted = [];
            var textAccessor = function (value) { return value; };
            if ("optionsText" in allBindings) {
                textAccessor = function (value) {
                    var valueAccessor = function (item) {
                        return item;
                    }
                    if ("optionsValue" in allBindings) {
                        valueAccessor = function (item) {
                            return item[allBindings.optionsValue];
                        }
                    }
                    var items = $.grep(allBindings.options(), function (e) { return valueAccessor(e) == value });
                    if (items.length == 0 || items.length > 1) {
                        return "UNKNOWN";
                    }
                    return items[0][allBindings.optionsText];
                }
            }
            $.each(allBindings.selectedOptions(), function (key, value) {
                converted.push({ id: value, text: textAccessor(value) });
            });
            $(el).select2("data", converted);
        }
        $(el).trigger("change");
    }
};

ko.bindingHandlers["bsChecked"] = {
    init: function (element, valueAccessor) {
        ko.utils.registerEventHandler(element, "change", function (event) {
            var check = $(event.target);
            console.log(check.html());
            console.log(check.val());
            valueAccessor()(check.val());
        });
    }
};

ko.validation.rules['bscomponent'] = {
    validator: function (value) {
        console.log(value);
        if (!value) {
            return value;
        }
        return value;
    }
};

ko.bindingHandlers.attrIf = {
    update: function (element, valueAccessor, allBindingsAccessor) {
        var h = ko.utils.unwrapObservable(valueAccessor());
        var show = ko.utils.unwrapObservable(h._if);
        if (show) {
            ko.bindingHandlers.attr.update(element, valueAccessor, allBindingsAccessor);
        } else {
            for (var k in h) {
                if (h.hasOwnProperty(k) && k.indexOf("_") !== 0) {
                    $(element).removeAttr(k);
                }
            }
        }
    }
};

function GetValueFromPossibleKnockout(value) {
    if (ko.isObservable(value)) {
        console.log(value());
        return value();
    }
    else
        return value;
}

function GetDisabledFromPossibleKnockout(value) {
    if (ko.isObservable(value))
        return 'disabled';
    else
        return '';
}

function GetBooleanFromPossibleKnockout(value) {
    var baseVal = '';
    if (ko.isObservable(value))
        baseVal = value();
    else
        baseVal = value;

    if (typeof baseVal === "boolean")
        return baseVal;
    else if (baseVal.toLowerCase() === 'true')
        return true;
    else
        return false;
}