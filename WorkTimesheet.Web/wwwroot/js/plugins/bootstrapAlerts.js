﻿(function () {

    var alertService = {
        showAlert: showAlert,
        success: success,
        info: info,
        warning: warning,
        error: error,
        clear: clear
    };

    window.alerts = alertService;

    function showAlert(alert) {
        $.notify({
            title: alert.messageType,
            message: alert.message,
            icon: alert.faIcon
        },
            {
                type: alert.alertClass
            });
    }

    function success(message) {
        showAlert({ alertClass: "success", message: message, faIcon: "fa fa-check", messageType: "Success : " });
    }

    function info(message) {
        showAlert({ alertClass: "info", message: message, faIcon: "fa fa-info", messageType: "Info : " });
    }

    function warning(message) {
        showAlert({ alertClass: "warning", message: message, faIcon: "fa fa-warning", messageType: "Warning : " });
    }

    function error(message) {
        showAlert({ alertClass: "danger", message: message, faIcon: "fa fa-times", messageType: "Error : " });
    }
    function clear() {

    }

})();
