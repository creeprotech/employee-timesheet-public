﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkTimesheet.Web.Data.Services;
using Microsoft.AspNetCore.Mvc;

namespace WorkTimesheet.Web.Views.Shared.Components.SideBar
{
    public class SideBarViewComponent : ViewComponent
    {
		private readonly IMenuAccessService _menuAccessService;

		public SideBarViewComponent(IMenuAccessService menuAccessService)
		{
			_menuAccessService = menuAccessService;
		}

		public async Task<IViewComponentResult> InvokeAsync()
		{
			var items = await _menuAccessService.GetMenuItemsAsync(HttpContext.User);

			return View(items);
		}
	}
}
