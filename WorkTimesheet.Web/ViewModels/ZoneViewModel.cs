﻿using WorkTimesheet.Web.Data.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.ViewModels
{
    public class ZoneMasterModel
    {
        [ScaffoldColumn(false)]
        public List<ZoneViewModel> Individuals { get; set; }

        public ZoneMasterModel()
        {
            Individuals = new List<ZoneViewModel>();
        }
    }

    public class ZoneViewModel
    {
        [ScaffoldColumn(false)]
        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("ZoneName")]
        public string ZoneName { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Select the Parent Zone"), KnockoutSelectBinding("ParentZoneType", "Name", "Id", true)]
        public string ParentZone { get; set; }
    }
}
