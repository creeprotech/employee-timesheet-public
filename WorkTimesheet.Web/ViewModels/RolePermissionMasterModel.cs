﻿using WorkTimesheet.Web.Data.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.ViewModels
{
    public class RolePermissionMasterModel
    {
        [ScaffoldColumn(false)]
        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("RoleName")]
        public string RoleName { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        public List<RolePermissionViewModel> RolePermissions { get; set; }

        public RolePermissionMasterModel()
        {
            RolePermissions = new List<RolePermissionViewModel>();
        }
    }

    public class RolePermissionViewModel
    {
        [ScaffoldColumn(false)]
        [DisplayName("Id")]
        public int Id { get; set; }

        [ScaffoldColumn(false)]
        [DisplayName("PermissionUuid")]
        public string PermissionUuid { get; set; }

        [ScaffoldColumn(false)]
        [DisplayName("ModuleValue")]
        public int ModuleValue { get; set; }

        [ScaffoldColumn(false)]
        [DisplayName("ControlValue")]
        public int ControlValue { get; set; }

        [ScaffoldColumn(false)]
        [DisplayName("PermissionId")]
        public String PermissionId { get; set; }

        [DisplayName("FeatureName")]
        public string FeatureName { get; set; }

        [DisplayName("PermissionValue")]
        [AnimatedCheckboxAttributeValue]
        public int PermissionValue { get; set; }

        [DisplayName("CanView")]
        [AnimatedCheckboxAttribute(1)]
        public bool CanView { get; set; }

        [DisplayName("CanAdd")]
        [AnimatedCheckboxAttribute(2)]
        public bool CanAdd { get; set; }

        [DisplayName("CanEdit")]
        [AnimatedCheckboxAttribute(4)]
        public bool CanEdit { get; set; }

        [DisplayName("CanDelete")]
        [AnimatedCheckboxAttribute(8)]
        public bool CanDelete { get; set; }

        [DisplayName("CanSpecialRights")]
        [AnimatedCheckboxAttribute(16)]
        public bool CanSpecialRights { get; set; }

        [DisplayName("CanExport")]
        [AnimatedCheckboxAttribute(32)]
        public bool CanExport { get; set; }

        [ScaffoldColumn(false)]
        [AnimatedCheckboxAttribute(0)]
        public bool FlagDeleted { get; set; }
    }
}
