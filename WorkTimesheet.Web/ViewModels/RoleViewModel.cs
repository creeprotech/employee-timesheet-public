﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.ViewModels
{
    public class RoleMasterModel
    {
        [ScaffoldColumn(false)]
        public List<RoleViewModel> Individuals { get; set; }

        public RoleMasterModel()
        {
            Individuals = new List<RoleViewModel>();
        }
    }

    public class RoleViewModel
    {
        [ScaffoldColumn(false)]
        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("RoleName")]
        public string RoleName { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }
    }
}
