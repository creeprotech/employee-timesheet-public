﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.ViewModels
{
    public class SessionViewModel
    {
        public string OUId { get; set; }
        public string OUName { get; set; }
        public string GroupId { get; set; }
        public string GroupName { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public string AuthenticationToken { get; set; }
    }
}
