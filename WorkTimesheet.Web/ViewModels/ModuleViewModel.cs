﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.ViewModels
{
    public class ModuleMasterModel
    {
        [ScaffoldColumn(false)]
        public List<ModuleViewModel> Individuals { get; set; }

        public ModuleMasterModel()
        {
            Individuals = new List<ModuleViewModel>();
        }
    }

    public class ModuleViewModel
    {
        [ScaffoldColumn(false)]
        [DisplayName("Id")]
        public string Id { get; set; }

        [ScaffoldColumn(false)]
        [DisplayName("ModuleUUID")]
        public string ModuleUUID { get; set; }

        [DisplayName("ModuleName")]
        public string ModuleName { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("ModuleValue")]
        public int ModuleValue { get; set; }

        [DisplayName("ControlValue")]
        public int ControlValue { get; set; }

        [DisplayName("PermissionValue")]
        public int PermissionValue { get; set; }

        [DisplayName("CanView")]
        public bool CanView { get; set; }

        [DisplayName("CanAdd")]
        public bool CanAdd { get; set; }

        [DisplayName("CanEdit")]
        public bool CanEdit { get; set; }

        [DisplayName("CanDelete")]
        public bool CanDelete { get; set; }

        [DisplayName("CanSpecialRights")]
        public bool CanSpecialRights { get; set; }

        [DisplayName("CanExport")]
        public bool CanExport { get; set; }
    }
}
