﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.ViewModels
{
    public class ProjectMasterModel
    {
        [ScaffoldColumn(false)]
        public List<ProjectViewModel> Individuals { get; set; }

        public ProjectMasterModel()
        {
            Individuals = new List<ProjectViewModel>();
        }
    }

    public class ProjectViewModel
    {
        [ScaffoldColumn(false)]
        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("ProjectCode")]
        public string ProjectCode { get; set; }

        [DisplayName("ProjectName")]
        public string ProjectName { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("NatureofIndustry")]
        public string NatureofIndustry { get; set; }
    }
}
