﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.ViewModels
{
    public class TimeTrackerMasterModel
    {
        [ScaffoldColumn(false)]
        public List<TimesheetViewModel> Individuals { get; set; }

        public TimeTrackerMasterModel()
        {
            Individuals = new List<TimesheetViewModel>();
        }
    }

    public class TimeTrackerViewModel
    {
        [ScaffoldColumn(false)]
        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("UserId")]
        public string UserId { get; set; }

        [DisplayName("DateStarted")]
        public string DateStarted { get; set; }

        [DisplayName("DateEnded")]
        public string DateEnded { get; set; }

        [DisplayName("TotalHours")]
        public int TotalHours { get; set; }

        [DisplayName("Status")]
        public int Status { get; set; }

        [DisplayName("CreatedOn")]
        public DateTime CreatedOn { get; set; }

        [DisplayName("Comments")]
        public string Comments { get; set; }

        [DisplayName("NetworkUsedBy")]
        public int NetworkUsedBy { get; set; }

        [DisplayName("EnvironmentUsedBy")]
        public int EnvironmentUsedBy { get; set; }

        [DisplayName("ApprovalStatus")]
        public int ApprovalStatus { get; set; }

        [DisplayName("ApprovalId")]
        public string ApprovalId { get; set; }
    }

    public class TimesheetViewModel
    {
        [ScaffoldColumn(false)]
        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("UserId")]
        public string UserId { get; set; }

        [DisplayName("UserName")]
        public string UserName { get; set; }

        [DisplayName("TimesheetDate")]
        public string TimesheetDate { get; set; }

        [DisplayName("DaysofWeek")]
        public string DaysofWeek { get; set; }

        [DisplayName("TotalHours")]
        public int TotalHours { get; set; }

        [DisplayName("Status")]
        public int Status { get; set; }

        [DisplayName("CreatedOn")]
        public int ApprovalStatus { get; set; }

        [DisplayName("Comments")]
        public string Comments { get; set; }

        [DisplayName("NetworkUsedBy")]
        public string NetworkUsedBy { get; set; }
    }
}
