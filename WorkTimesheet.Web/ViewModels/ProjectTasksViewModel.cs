﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.ViewModels
{
    public class ProjectTaskMasterModel
    {
        [ScaffoldColumn(false)]
        public List<ProjectTaskViewModel> Individuals { get; set; }

        public ProjectTaskMasterModel()
        {
            Individuals = new List<ProjectTaskViewModel>();
        }
    }

    public class ProjectTaskViewModel
    {
        [ScaffoldColumn(false)]
        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("UserId")]
        public string UserId { get; set; }

        [DisplayName("UserName")]
        public string UserName { get; set; }

        [DisplayName("ProjectId")]
        public string ProjectId { get; set; }

        [DisplayName("ProjectName")]
        public string ProjectName { get; set; }

        [DisplayName("TaskCode")]
        [Data.Helpers.ReadOnly]
        public string TaskCode { get; set; }

        [DisplayName("Title")]
        public string Title { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Status")]
        public int Status { get; set; }

        [DisplayName("StatusName")]
        public string StatusName { get; set; }

        [DisplayName("DaysToBeCompleted")]
        public int DaysToBeCompleted { get; set; }

        [DisplayName("CreatedOn")]
        public DateTime CreatedOn { get; set; }

        [DisplayName("DateStarted")]
        public DateTime DateStarted { get; set; }

        [DisplayName("DateEnded")]
        public DateTime DateEnded { get; set; }

        [DisplayName("DateCompleted")]
        public DateTime DateCompleted { get; set; }

        [DisplayName("ApprovalStatus")]
        public int ApprovalStatus { get; set; }

        [DisplayName("ApprovalStatusName")]
        public string ApprovalStatusName { get; set; }

        [DisplayName("ApprovalId")]
        public string ApprovalId { get; set; }
    }
}
