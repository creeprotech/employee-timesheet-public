﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.ViewModels
{
    public class MenuMasterModel
    {
        public List<MenuViewModel> Individuals { get; set; }
        public MenuMasterModel()
        {
            Individuals = new List<MenuViewModel>();
        }
    }

    public class MenuViewModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ParentId { get; set; }
        public string ParentName { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string IconClass { get; set; }
        public int MenuOrder { get; set; }
        public string MenuUUID { get; set; }
        public int SiteId { get; set; }
        public bool IsExternalLink { get; set; }
        public string URL { get; set; }
    }
}
