﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.ViewModels
{
    public class RulesViewModel
    {
        public List<RuleViewModel> Rules { get; set; }
        public RulesViewModel()
        {
            Rules = new List<RuleViewModel>();
        }
    }

    public class RuleViewModel
    {
        public int Id { get; set; }
        public string RuleId { get; set; }
        public string RuleName { get; set; }
        public string RuleUUID { get; set; }
        public int RuleValue { get; set; }
        public int ModuleValue { get; set; }
        public int ControlValue { get; set; }
        public int PermissionValue { get; set; }
    }
}
