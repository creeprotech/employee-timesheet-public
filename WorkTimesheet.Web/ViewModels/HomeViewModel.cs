﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.ViewModels
{
    public class HomeViewModel
    {
        public string TotalEmployee { get; set; }
        public string TotalProject { get; set; }
        public string TotalProgressTask { get; set; }
        public string TotalCompletedTask { get; set; }
    }
}
