﻿using WorkTimesheet.Web.Data.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.ViewModels
{
    public class UserMasterModel
    {
        [ScaffoldColumn(false)]
        public List<UserViewModel> Individuals { get; set; }

        public UserMasterModel()
        {
            Individuals = new List<UserViewModel>();
        }
    }

    public class UserViewModel
    {
        [ScaffoldColumn(false)]
        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("UserName")]
        public string UserName { get; set; }

        [DisplayName("Password")]
        public string Password { get; set; }

        [DisplayName("PhoneNumber")]
        public string PhoneNumber { get; set; }

        [DisplayName("Select the Role"), KnockoutSelectBinding("RoleType", "Name", "Id", true)]
        public string RoleId { get; set; }

        [DisplayName("Select the Group"), KnockoutSelectBinding("GroupType", "Name", "Id", true)]
        public string GroupId { get; set; }
    }
}
