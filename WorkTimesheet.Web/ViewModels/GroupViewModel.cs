﻿using WorkTimesheet.Web.Data.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.ViewModels
{
    public class GroupMasterModel
    {
        [ScaffoldColumn(false)]
        public List<GroupViewModel> Individuals { get; set; }

        public GroupMasterModel()
        {
            Individuals = new List<GroupViewModel>();
        }
    }

    public class GroupViewModel
    {
        [ScaffoldColumn(false)]
        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("GroupName")]
        public string GroupName { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Select the Parent Group"), KnockoutSelectBinding("ParentGroupType", "Name", "Id", true)]
        public string ParentGroup { get; set; }
    }
}
