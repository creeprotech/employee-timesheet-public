﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.ViewModels
{
    public class ListOfValuesModel
    {
        [ScaffoldColumn(false)]
        public List<LovViewModel> Individuals { get; set; }

        public ListOfValuesModel()
        {
            Individuals = new List<LovViewModel>();
        }
    }

    public class LovViewModel
    {
        [ScaffoldColumn(false)]
        [DisplayName("LOV ID")]
        public int Id { get; set; }

        [DisplayName("Project Type")]
        [Required]
        public string ProjectType { get; set; }

        [DisplayName("Project Description")]
        [Required]
        public string ProjectDescription { get; set; }
        public List<LovValue> Values { get; set; }
    }

    public class LovValue
    {
        [ScaffoldColumn(false)]
        [DisplayName("LOVValue ID")]
        public int Id { get; set; }

        [DisplayName("Project Label")]
        [Required]
        public string ProjectLabel { get; set; }

        [DisplayName("Project Value")]
        [Required]
        public string ProjectValue { get; set; }

        [DisplayName("Order")]
        [Required]
        public int Order { get; set; }

        [ScaffoldColumn(false)]
        public bool FlagDeleted { get; set; }
    }
}
