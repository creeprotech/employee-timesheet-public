﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkTimesheet.Entities.ValueDataType;
using WorkTimesheet.Entities.ValueDataType.Modules;
using WorkTimesheet.Repositories;
using WorkTimesheet.Web.Data;
using WorkTimesheet.Web.Data.Authorization;
using WorkTimesheet.Web.Data.Filters;
using WorkTimesheet.Web.Data.Helpers;
using WorkTimesheet.Web.ViewModels;

namespace WorkTimesheet.Web.Controllers
{
    [AuthorizeSessionAttribute]
    public class ReportsController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger<ReportsController> _logger;
        private readonly ApplicationDbContext _context;
        private readonly IUnitOfWorkDapper _unitOfWorkDapper;

        public ReportsController(
                UserManager<IdentityUser> userManager,
                RoleManager<IdentityRole> roleManager,
                ApplicationDbContext context,
                ILogger<ReportsController> logger,
                IUnitOfWorkDapper unitOfWorkDapper)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _context = context;
            _logger = logger;
            _unitOfWorkDapper = unitOfWorkDapper;
        }

        public IActionResult TimesheetReportView()
        {
            //Method 2
            var permissions = User?.Claims.Where(x => x.Type == "Permission").FirstOrDefault();
            bool isGetAllTimesheet = false;
            if (permissions != null)
            {
                RuleClaims ruleClaims = JsonConvert.DeserializeObject<RuleClaims>(permissions.Value);
                RulePermission rulePermission = ruleClaims.rulePermissions.Where(r => r.moduleValue == (int)ModuleValueType.Reports && r.controlValue == (int)ReportValueType.GetAllTimesheetReports).FirstOrDefault();
                if (rulePermission != null)
                    isGetAllTimesheet = rulePermission.permissionValue.HasFlag(PermissionType.SpecialRights);
            }

            string UserId = null;
            if (AllSessionKeys.UserId != null)
                UserId = HttpContext.Session.GetString(AllSessionKeys.UserId);

            var usersRepository = _unitOfWorkDapper._UsersRepository;
            var timeTrackerRepository = _unitOfWorkDapper._TimeTrackerRepository;
            var listOfValuesRepository = _unitOfWorkDapper._ListOfValuesRepository;
            var timeTrackers = timeTrackerRepository.GetAll();

            if (!isGetAllTimesheet)
                timeTrackers = timeTrackers.Where(t => t.UserId == UserId);

            TimeTrackerMasterModel timeTrackerMasterModel = new TimeTrackerMasterModel();
            List<TimesheetViewModel> timesheetModels = new List<TimesheetViewModel>();
            foreach (Entities.Timesheet.TimeTracker timeTracker in timeTrackers)
            {
                TimesheetViewModel timesheetModel = new TimesheetViewModel();
                timesheetModel.Id = timeTracker.Id.ToString();
                timesheetModel.UserId = timeTracker.UserId;
                timesheetModel.UserName = usersRepository.GetAll().Where(u => u.Id == timeTracker.UserId).FirstOrDefault().UserName;
                timesheetModel.TimesheetDate = timeTracker.DateStarted.ToString("d");
                timesheetModel.DaysofWeek = timeTracker.DateStarted.ToString("ddd");
                timesheetModel.TotalHours = timeTracker.TotalHours;
                timesheetModel.Status = timeTracker.Status;
                timesheetModel.ApprovalStatus = timeTracker.ApprovalStatus;
                timesheetModel.Comments = timeTracker.Comments;
                timesheetModel.NetworkUsedBy = listOfValuesRepository.GetAll().Where(l => l.Type == "NETWORK_STATUS" && l.Value == timeTracker.NetworkUsedBy.ToString()).FirstOrDefault().Label;
                timesheetModels.Add(timesheetModel);
            }

            timeTrackerMasterModel.Individuals = timesheetModels;
            return View(timeTrackerMasterModel);
        }

        public IActionResult TaskReportView()
        {
            return View();
        }
    }
}
