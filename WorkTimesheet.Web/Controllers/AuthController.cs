﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkTimesheet.Web.Data;
using WorkTimesheet.Web.Data.Helpers;
using WorkTimesheet.Web.Data.Models;

namespace WorkTimesheet.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly ApplicationDbContext _context;

        public AuthController(SignInManager<IdentityUser> signInManager,
            ApplicationDbContext context)
        {
            _signInManager = signInManager;
            _context = context;
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login(AuthRequest loginRequest)
        {
            if (loginRequest == null || string.IsNullOrEmpty(loginRequest.Username) || string.IsNullOrEmpty(loginRequest.Password))
            {
                return BadRequest("Missing login details");
            }

            var result = await _signInManager.PasswordSignInAsync(loginRequest.Username, loginRequest.Password, true, lockoutOnFailure: false);

            if (result == null)
            {
                return BadRequest($"Invalid credentials");
            }

            if (result.Succeeded)
            {
                if (result.IsLockedOut)
                {
                    return BadRequest($"User account locked out");
                }

                var user = _context.Users.Where(u => u.UserName == loginRequest.Username).FirstOrDefault();
                if (user != null)
                {

                    var token = await Task.Run(() => TokenHelper.GenerateToken(user.Id, false));

                    var loginResponse = new AuthResponse { Username = user.UserName, FirstName = user.Email, LastName = user.Email, Token = token };

                    return Ok(loginResponse);
                }
            }

            return BadRequest("Invalid login attempt");
        }
    }
}
