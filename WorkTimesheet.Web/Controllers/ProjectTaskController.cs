﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkTimesheet.Entities.ProjectTask;
using WorkTimesheet.Entities.ValueDataType;
using WorkTimesheet.Entities.ValueDataType.Modules;
using WorkTimesheet.Repositories;
using WorkTimesheet.Web.Data;
using WorkTimesheet.Web.Data.Authorization;
using WorkTimesheet.Web.Data.Filters;
using WorkTimesheet.Web.Data.Helpers;
using WorkTimesheet.Web.ViewModels;

namespace WorkTimesheet.Web.Controllers
{
    [AuthorizeSessionAttribute]
    public class ProjectTaskController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger<ProjectTaskController> _logger;
        private readonly ApplicationDbContext _context;
        private readonly IUnitOfWorkDapper _unitOfWorkDapper;

        public ProjectTaskController(
                UserManager<IdentityUser> userManager,
                RoleManager<IdentityRole> roleManager,
                ApplicationDbContext context,
                ILogger<ProjectTaskController> logger,
                IUnitOfWorkDapper unitOfWorkDapper)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _context = context;
            _logger = logger;
            _unitOfWorkDapper = unitOfWorkDapper;
        }

        public IActionResult ProjectListView()
        {
            var projectRepository = _unitOfWorkDapper._ProjectsRepository;
            var projects = projectRepository.GetAll();
            ProjectMasterModel projectMasterModel = new ProjectMasterModel();
            List<ProjectViewModel> projectViewModels = new List<ProjectViewModel>();
            foreach (Entities.ProjectTask.Projects project in projects)
            {
                ProjectViewModel zoneViewModel = new ProjectViewModel();
                zoneViewModel.Id = project.Id.ToString();
                zoneViewModel.ProjectCode = project.ProjectCode;
                zoneViewModel.ProjectName = project.ProjectName;
                zoneViewModel.Description = project.Description;
                zoneViewModel.NatureofIndustry = project.NatureofIndustry;
                projectViewModels.Add(zoneViewModel);
            }
            projectMasterModel.Individuals = projectViewModels;
            return View(projectMasterModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult SaveProject(ProjectViewModel model)
        {
            try
            {
                var projectRepository = _unitOfWorkDapper._ProjectsRepository;
                if (model != null)
                {
                    bool isInsert = false;
                    WorkTimesheet.Entities.ProjectTask.Projects project = projectRepository.GetByPK(model.Id);
                    if (project == null)
                    {
                        isInsert = true;
                        project = new WorkTimesheet.Entities.ProjectTask.Projects();
                        project.Id = Guid.NewGuid();
                    }

                    project.ProjectCode = model.ProjectCode;
                    project.ProjectName = model.ProjectName;
                    project.Description = model.Description;
                    project.NatureofIndustry = model.NatureofIndustry;
                    project.UserAction = 1;// LoggedInUserID.Value;

                    if (isInsert)
                        projectRepository.Insert(project.ConvertToDataTable()).FirstOrDefault();
                    else
                        projectRepository.Update(project.ConvertToDataTable());


                    _unitOfWorkDapper.Commit();
                }
                return Json(model);
            }
            catch (Exception ex)
            {
                //LogError(ex, model);
                return Json(ex.Message);
            }
        }

        public IActionResult CurrentTaskView()
        {
            //Method 2
            var permissions = User?.Claims.Where(x => x.Type == "Permission").FirstOrDefault();
            bool isGetAllTask = false;
            if(permissions != null)
            {
                RuleClaims ruleClaims = JsonConvert.DeserializeObject<RuleClaims>(permissions.Value);
                RulePermission rulePermission = ruleClaims.rulePermissions.Where(r => r.moduleValue == (int)ModuleValueType.ProjectTask && r.controlValue == (int)ProjectTaskValueType.GetAllTask).FirstOrDefault();
                if (rulePermission != null)
                    isGetAllTask = rulePermission.permissionValue.HasFlag(PermissionType.SpecialRights);
            }
            string UserId = null;
            if (AllSessionKeys.UserId != null)
                UserId = HttpContext.Session.GetString(AllSessionKeys.UserId);

            var usersRepository = _unitOfWorkDapper._UsersRepository;
            var listOfValuesRepository = _unitOfWorkDapper._ListOfValuesRepository;
            var projectRepository = _unitOfWorkDapper._ProjectsRepository;
            var projectTaskRepository = _unitOfWorkDapper._ProjectTasksRepository;
            var projectTasks = projectTaskRepository.GetAll();

            if(!isGetAllTask)
                projectTasks = projectTasks.Where(p => p.UserId == UserId);

            var listOfValues = listOfValuesRepository.GetAll();
            ProjectTaskMasterModel projectTaskMasterModel = new ProjectTaskMasterModel();
            List<ProjectTaskViewModel> projectTaskViewModels = new List<ProjectTaskViewModel>();
            foreach (Entities.ProjectTask.ProjectTasks projectTask in projectTasks)
            {
                ProjectTaskViewModel projectTaskViewModel = new ProjectTaskViewModel();
                projectTaskViewModel.Id = projectTask.Id.ToString();
                projectTaskViewModel.TaskCode = projectTask.TaskCode;
                projectTaskViewModel.Title = projectTask.Title;
                projectTaskViewModel.ProjectName = projectRepository.GetByPK(projectTask.ProjectId.ToString()).ProjectName;
                projectTaskViewModel.Status = projectTask.Status;
                projectTaskViewModel.StatusName = listOfValues.Where(p => p.Type == "TASK_STATUS" & p.Value == projectTask.Status.ToString()).FirstOrDefault().Label;
                projectTaskViewModel.ApprovalStatus = projectTask.ApprovalStatus;
                projectTaskViewModel.ApprovalStatusName = listOfValues.Where(p => p.Type == "APPROVAL_STATUS" & p.Value == projectTask.ApprovalStatus.ToString()).FirstOrDefault().Label;
                if (projectTask.UserId == null)
                {
                    projectTaskViewModel.UserId = null;
                    projectTaskViewModel.UserName = null;
                }
                else
                {
                    projectTaskViewModel.UserId = projectTask.UserId;
                    projectTaskViewModel.UserName = usersRepository.GetAll().Where(u => u.Id == projectTask.UserId).FirstOrDefault().UserName;
                }
                projectTaskViewModels.Add(projectTaskViewModel);
            }
            projectTaskMasterModel.Individuals = projectTaskViewModels;
            ViewBag.StatusNameType = listOfValues.Where(l => l.Type == "TASK_STATUS").Select(s => new { Id = s.Value, Name = s.Label }).ToList();
            ViewBag.ApprovalNameType = listOfValues.Where(l => l.Type == "APPROVAL_STATUS").Select(s => new { Id = s.Value, Name = s.Label }).ToList();
            return View(projectTaskMasterModel);
        }

        public JsonResult GetCurrentTaskStatus(string id, string statusName)
        {
            try
            {
                var listOfValuesRepository = _unitOfWorkDapper._ListOfValuesRepository;
                var projectTaskRepository = _unitOfWorkDapper._ProjectTasksRepository;
                var projectTaskHistoryRepository = _unitOfWorkDapper._ProjectTaskHistoryRepository;
                string UserId = null;
                if (AllSessionKeys.UserId != null)
                    UserId = HttpContext.Session.GetString(AllSessionKeys.UserId);
                var projectTasks = projectTaskRepository.GetByUserId(UserId);
                var listOfValues = listOfValuesRepository.GetAll().Where(l => l.Label == "In Progress").FirstOrDefault();
                var projectTask = projectTasks.Where(p => p.Status == Convert.ToInt32(listOfValues.Value)).FirstOrDefault();

                if (projectTask != null)
                    if(projectTask.Id != Guid.Parse(id))
                        return Json(new { Result = "failed", Message = "Cannot Progress" });

                projectTask = projectTasks.Where(p => p.Id == Guid.Parse(id)).FirstOrDefault();

                ProjectTaskHistory projectTaskHistory = new ProjectTaskHistory();
                projectTaskHistory.Id = projectTask.Id;
                projectTaskHistory.UserId = projectTask.UserId;
                projectTaskHistory.ProjectId = projectTask.ProjectId;
                projectTaskHistory.TaskCode = projectTask.TaskCode;
                projectTaskHistory.Title = projectTask.Title;
                projectTaskHistory.Description = projectTask.Description;
                projectTaskHistory.Status = projectTask.Status;
                projectTaskHistory.DaysToBeCompleted = projectTask.DaysToBeCompleted;
                projectTaskHistory.CreatedOn = projectTask.CreatedOn;
                projectTaskHistory.DateStarted = projectTask.DateStarted;
                projectTaskHistory.DateEnded = projectTask.DateEnded;
                projectTaskHistory.DateCompleted = projectTask.DateCompleted;
                projectTaskHistory.ApprovalStatus = projectTask.ApprovalStatus;
                projectTaskHistory.ApprovalId = projectTask.ApprovalId;

                projectTaskHistoryRepository.Insert(projectTaskHistory.ConvertToDataTable());

                listOfValues = listOfValuesRepository.GetAll().Where(l => l.Value == statusName).FirstOrDefault();
                projectTask.Status = Convert.ToInt32(listOfValues.Value);
                if (statusName == "2")
                {
                    projectTask.DateStarted = DateTime.Now;
                    projectTask.DateEnded = DateTime.Now.AddDays(projectTask.DaysToBeCompleted);
                }
                if(statusName == "5")
                {
                    projectTask.DateCompleted = DateTime.Now;
                }
                projectTaskRepository.Update(projectTask.ConvertToDataTable());

                _unitOfWorkDapper.Commit();
                return Json(new { Result = "success", Message = "Cannot Progress" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "failed", Message = "Cannot Progress" });
            }
        }

        public JsonResult GetApprovalStatus(string id, string statusName)
        {
            try
            {
                var listOfValuesRepository = _unitOfWorkDapper._ListOfValuesRepository;
                var projectTaskRepository = _unitOfWorkDapper._ProjectTasksRepository;
                var projectTaskHistoryRepository = _unitOfWorkDapper._ProjectTaskHistoryRepository;
                var projectTask = projectTaskRepository.GetByPK(id);

                ProjectTaskHistory projectTaskHistory = new ProjectTaskHistory();
                projectTaskHistory.Id = projectTask.Id;
                projectTaskHistory.UserId = projectTask.UserId;
                projectTaskHistory.ProjectId = projectTask.ProjectId;
                projectTaskHistory.TaskCode = projectTask.TaskCode;
                projectTaskHistory.Title = projectTask.Title;
                projectTaskHistory.Description = projectTask.Description;
                projectTaskHistory.Status = projectTask.Status;
                projectTaskHistory.DaysToBeCompleted = projectTask.DaysToBeCompleted;
                projectTaskHistory.CreatedOn = projectTask.CreatedOn;
                projectTaskHistory.DateStarted = projectTask.DateStarted;
                projectTaskHistory.DateEnded = projectTask.DateEnded;
                projectTaskHistory.DateCompleted = projectTask.DateCompleted;
                projectTaskHistory.ApprovalStatus = projectTask.ApprovalStatus;
                projectTaskHistory.ApprovalId = projectTask.ApprovalId;

                projectTaskHistoryRepository.Insert(projectTaskHistory.ConvertToDataTable());

                var listOfValues = listOfValuesRepository.GetAll().Where(l => l.Value == statusName).FirstOrDefault();
                projectTask.ApprovalStatus = Convert.ToInt32(listOfValues.Value);
                projectTaskRepository.Update(projectTask.ConvertToDataTable());

                _unitOfWorkDapper.Commit();
                return Json(new { Result = "success", Message = "Cannot Progress" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "failed", Message = "Cannot Progress" });
            }
        }

        public IActionResult TaskForm(string Id = null)
        {
            var usersRepository = _unitOfWorkDapper._UsersRepository;
            var users = usersRepository.GetAll();

            var projectsRepository = _unitOfWorkDapper._ProjectsRepository;
            var projects = projectsRepository.GetAll();

            ProjectTaskViewModel projectTaskViewModel = new ProjectTaskViewModel();
            projectTaskViewModel.TaskCode = "CTPL000T000";
            if (Id != null)
            {
                var projectTasksRepository = _unitOfWorkDapper._ProjectTasksRepository;
                ProjectTasks projectTask = projectTasksRepository.GetByPK(Id);
                if (projectTask != null)
                {
                    projectTaskViewModel = new ProjectTaskViewModel();
                    projectTaskViewModel.Id = projectTask.Id.ToString();
                    projectTaskViewModel.TaskCode = projectTask.TaskCode;
                    projectTaskViewModel.Title = projectTask.Title;
                    projectTaskViewModel.Description = projectTask.Description;

                    if (projectTask.ProjectId == null)
                        projectTaskViewModel.ProjectId = null;
                    else
                        projectTaskViewModel.ProjectId = projectTask.ProjectId.ToString();

                    if (projectTask.UserId == null)
                        projectTaskViewModel.UserId = null;
                    else
                        projectTaskViewModel.UserId = projectTask.UserId;

                    projectTaskViewModel.DaysToBeCompleted = projectTask.DaysToBeCompleted;
                    projectTaskViewModel.Status = projectTask.Status;
                    projectTaskViewModel.ApprovalStatus = projectTask.ApprovalStatus;
                }
            }
            var userNames = users.Select(s => new { Id = s.Id, Name = s.UserName }).ToList();
            ViewBag.UserNameType = userNames.OrderBy(p => p.Id);

            var projectNames = projects.Select(s => new { Id = s.Id, Name = s.ProjectName }).ToList();
            ViewBag.ProjectNameType = projectNames.OrderBy(p => p.Id);

            return View(projectTaskViewModel);
        }

        public IActionResult GetTaskCodeByProjectId(string ProjectId = null)
        {
            string taskCode = "CTPL000T000";
            if (ProjectId != null)
            {
                var projectsRepository = _unitOfWorkDapper._ProjectsRepository;
                var projectTasksRepository = _unitOfWorkDapper._ProjectTasksRepository;
                var project = projectsRepository.GetByPK(ProjectId);

                if (project != null)
                {
                    var projectTask = projectTasksRepository.GetAll().Where(p => p.ProjectId == Guid.Parse(ProjectId)).OrderByDescending(o => o.CreatedOn).FirstOrDefault();
                    if (projectTask == null)
                        taskCode = project.ProjectCode + "T001";
                    else
                    {
                        var lastDigits = projectTask.TaskCode.Substring(projectTask.TaskCode.Length - 3);
                        int lastNumber = Convert.ToInt32(lastDigits);
                        taskCode = project.ProjectCode + "T" + (lastNumber + 1).ToString("D3");
                    }

                    return Json(new { Result = taskCode, Message = "Cannot Progress" });
                }
            }
            return Json(new { Result = taskCode, Message = "Cannot Progress" });
        }

        [HttpPost]
        [Authorize]
        public ActionResult SaveProjectTask(ProjectTaskViewModel model)
        {
            try
            {
                var projectTaskRepository = _unitOfWorkDapper._ProjectTasksRepository;
                if (model != null)
                {
                    bool isInsert = false;
                    WorkTimesheet.Entities.ProjectTask.ProjectTasks projectTask = projectTaskRepository.GetByPK(model.Id);
                    if (projectTask == null)
                    {
                        isInsert = true;
                        projectTask = new WorkTimesheet.Entities.ProjectTask.ProjectTasks();
                        projectTask.Id = Guid.NewGuid();
                    }

                    projectTask.TaskCode = model.TaskCode;
                    projectTask.UserId = model.UserId;
                    projectTask.ProjectId = Guid.Parse(model.ProjectId);
                    projectTask.Title = model.Title;
                    projectTask.Description = model.Description;
                    projectTask.DaysToBeCompleted = model.DaysToBeCompleted;
                    projectTask.CreatedOn = DateTime.Now;
                    projectTask.Status = 1;                    
                    projectTask.ApprovalStatus = 0;
                    projectTask.UserAction = 1;// LoggedInUserID.Value;

                    if (isInsert)
                        projectTaskRepository.Insert(projectTask.ConvertToDataTable());
                    else
                        projectTaskRepository.Update(projectTask.ConvertToDataTable());

                    _unitOfWorkDapper.Commit();
                }
                return Json(model);
            }
            catch (Exception ex)
            {
                //LogError(ex, model);
                return Json(ex.Message);
            }
        }
    }
}
