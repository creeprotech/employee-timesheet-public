﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkTimesheet.Repositories;
using WorkTimesheet.Web.Data;
using WorkTimesheet.Web.Data.Filters;
using WorkTimesheet.Web.Data.Helpers;
using WorkTimesheet.Web.ViewModels;

namespace WorkTimesheet.Web.Controllers
{
    [AuthorizeSessionAttribute]
    public class TimesheetController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger<TimesheetController> _logger;
        private readonly ApplicationDbContext _context;
        private readonly IUnitOfWorkDapper _unitOfWorkDapper;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public TimesheetController(
                UserManager<IdentityUser> userManager,
                RoleManager<IdentityRole> roleManager,
                ApplicationDbContext context,
                ILogger<TimesheetController> logger,
                IUnitOfWorkDapper unitOfWorkDapper, 
                IHttpContextAccessor httpContextAccessor)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _context = context;
            _logger = logger;
            _unitOfWorkDapper = unitOfWorkDapper;
            _httpContextAccessor = httpContextAccessor;
        }

        public IActionResult WorkFromHomeView()
        {
            TimeTrackerViewModel timeTrackerViewModel = new TimeTrackerViewModel();

            var listOfValuesRepository = _unitOfWorkDapper._ListOfValuesRepository;
            var listOfValues = listOfValuesRepository.GetAll();

            var userNames = listOfValues.Where(l => l.Type == "NETWORK_STATUS").Select(s => new { Id = s.Value, Name = s.Label }).ToList();
            ViewBag.NetworkNameType = userNames.OrderBy(p => p.Id);

            var projectNames = listOfValues.Where(l => l.Type == "ENVIRONMENT_STATUS").Select(s => new { Id = s.Value, Name = s.Label }).ToList();
            ViewBag.EnvironmentNameType = projectNames.OrderBy(p => p.Id);

            return View(timeTrackerViewModel);
        }

        public IActionResult SaveTimeTracker(TimeTrackerViewModel model)
        {
            try
            {
                var timeTrackerRepository = _unitOfWorkDapper._TimeTrackerRepository;
                if (model != null)
                {
                    bool isInsert = false;
                    WorkTimesheet.Entities.Timesheet.TimeTracker timeTracker = timeTrackerRepository.GetByPK(model.Id);
                    if (timeTracker == null)
                    {
                        isInsert = true;
                        timeTracker = new WorkTimesheet.Entities.Timesheet.TimeTracker();
                        timeTracker.Id = Guid.NewGuid();
                    }

                    DateTime dtFrom = DateTime.Parse(model.DateStarted);
                    DateTime dtTo = DateTime.Parse(model.DateEnded);
                    int totalHours = dtTo.Subtract(dtFrom).Hours;

                    if(AllSessionKeys.UserId != null)
                        timeTracker.UserId = HttpContext.Session.GetString(AllSessionKeys.UserId);
                    timeTracker.DateStarted = dtFrom;
                    timeTracker.DateEnded = dtTo;
                    timeTracker.TotalHours = totalHours;
                    timeTracker.Status = 0;
                    timeTracker.CreatedOn = DateTime.Now;
                    timeTracker.Comments = model.Comments;
                    timeTracker.NetworkUsedBy = model.NetworkUsedBy;
                    timeTracker.EnvironmentUsedBy = model.EnvironmentUsedBy;
                    timeTracker.ApprovalStatus = 0;
                    timeTracker.UserAction = 1;// LoggedInUserID.Value;
                    var remoteIpAddress = Convert.ToString(_httpContextAccessor.HttpContext.Connection.RemoteIpAddress);
                    timeTracker.IPAddress = remoteIpAddress;
                    var userAgent = Request.Headers["User-Agent"].ToString();
                    timeTracker.UserAgent = userAgent;

                    if (isInsert)
                        timeTrackerRepository.Insert(timeTracker.ConvertToDataTable());
                    else
                        timeTrackerRepository.Update(timeTracker.ConvertToDataTable());

                    _unitOfWorkDapper.Commit();
                }
                return Json(model);
            }
            catch (Exception ex)
            {
                //LogError(ex, model);
                return Json(ex.Message);
            }
        }
    }
}
