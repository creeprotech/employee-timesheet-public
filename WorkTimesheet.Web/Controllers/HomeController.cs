﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using WorkTimesheet.Repositories;
using WorkTimesheet.Web.Data;
using WorkTimesheet.Web.Data.Filters;
using WorkTimesheet.Web.Data.Helpers;
using WorkTimesheet.Web.ViewModels;

namespace WorkTimesheet.Web.Controllers
{
    [AuthorizeSessionAttribute]
    public class HomeController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _context;
        private readonly IUnitOfWorkDapper _unitOfWorkDapper;

        public HomeController(
                UserManager<IdentityUser> userManager,
                RoleManager<IdentityRole> roleManager,
                ApplicationDbContext context,
                ILogger<HomeController> logger,
                IUnitOfWorkDapper unitOfWorkDapper)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _context = context;
            _logger = logger;
            _unitOfWorkDapper = unitOfWorkDapper;
        }

        public IActionResult Index()
        {
            var userRepository = _unitOfWorkDapper._UsersRepository;
            var projectRepository = _unitOfWorkDapper._ProjectsRepository;
            var projectTaskRepository = _unitOfWorkDapper._ProjectTasksRepository;
            HomeViewModel homeViewModel = new HomeViewModel();
            homeViewModel.TotalEmployee = userRepository.GetAll().Count().ToString();
            homeViewModel.TotalProject = projectRepository.GetAll().Count().ToString();
            homeViewModel.TotalProgressTask = projectTaskRepository.GetAll().Where(p => p.Status != 5).Count().ToString();
            homeViewModel.TotalCompletedTask = projectTaskRepository.GetAll().Where(p => p.Status == 5).Count().ToString();
            return View(homeViewModel);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public IActionResult Logout()
        {
            try
            {
                CookieOptions option = new CookieOptions();

                if (Request.Cookies[AllSessionKeys.AuthenticationToken] != null)
                {
                    option.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Append(AllSessionKeys.AuthenticationToken, "", option);
                }

                if (HttpContext.Session != null)
                    HttpContext.Session.Clear();

                return RedirectToPage("/Account/Login", new { area = "Identity" });
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
