﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkTimesheet.Repositories;
using WorkTimesheet.Web.Data;
using WorkTimesheet.Web.Data.Filters;
using WorkTimesheet.Web.Data.Models;
using WorkTimesheet.Web.ViewModels;

namespace WorkTimesheet.Web.Controllers
{
    [AuthorizeSessionAttribute]
    public class MastersController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger<MastersController> _logger;
        private readonly ApplicationDbContext _context;
        private readonly IUnitOfWorkDapper _unitOfWorkDapper;

        public MastersController(
                UserManager<IdentityUser> userManager,
                RoleManager<IdentityRole> roleManager,
                ApplicationDbContext context,
                ILogger<MastersController> logger,
                IUnitOfWorkDapper unitOfWorkDapper)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _context = context;
            _logger = logger;
            _unitOfWorkDapper = unitOfWorkDapper;
        }

        public IActionResult LOVView()
        {
            var listOfValuesRepository = _unitOfWorkDapper._ListOfValuesRepository;
            var listOfValues = listOfValuesRepository.GetAll();

            ListOfValuesModel listOfValuesModel = new ListOfValuesModel();
            List<LovViewModel> lovViewModels = new List<LovViewModel>();
            foreach (var e in listOfValues)
            {
                List<LovValue> lovValues = new List<LovValue>();
                var l = lovViewModels.Where(o => o.ProjectType == e.Type).Count();
                if (l == 0)
                {
                    var Child = listOfValuesRepository.GetAll().Where(o => o.Type == e.Type).ToList();

                    foreach (var s in Child)
                    {
                        LovValue lovValue = new LovValue();
                        lovValue.ProjectLabel = s.Label;
                        lovValue.ProjectValue = s.Value;
                        lovValue.Id = s.Id;
                        lovValue.Order = s.OrderValue;
                        lovValues.Add(lovValue);
                    }
                }
                if (l == 0)
                {
                    lovViewModels.Add(new LovViewModel { Id = e.Id, ProjectType = e.Type, ProjectDescription = e.Description, Values = lovValues.ToList() });
                }
            }
            if (lovViewModels.Count > 0)
                listOfValuesModel.Individuals = lovViewModels;

            return View(listOfValuesModel);
        }

        [HttpGet, Authorize]
        public ActionResult ListOfValuesApplication(string type = null)
        {
            var listOfValuesRepository = _unitOfWorkDapper._ListOfValuesRepository;
            List<LovValue> lovValues = new List<LovValue>();
            LovViewModel lovViewModel = new LovViewModel();
            if (!string.IsNullOrEmpty(type))
            {
                var lov = listOfValuesRepository.GetAll().Where(o => o.Type == type).ToList();

                foreach (var e in lov)
                {
                    LovValue lovValuev = new LovValue();
                    lovValuev.ProjectValue = e.Value;
                    lovValuev.Order = e.OrderValue;
                    lovValuev.ProjectLabel = e.Label;
                    lovValuev.Id = e.Id;
                    lovValues.Add(lovValuev);
                }
                lovViewModel.Id = lov[0].Id;
                lovViewModel.ProjectDescription = lov[0].Description;
                lovViewModel.ProjectType = lov[0].Type;
                lovViewModel.Values = lovValues;
            }
            else
            {
                lovViewModel.Id = 0;
                lovViewModel.ProjectDescription = "";
                lovViewModel.ProjectType = "";
                LovValue lovValuev = new LovValue();
                //lovValuev.ProjectValue = "";
                //lovValuev.Order = 0;
                //lovValuev.ProjectLabel = "";
                //lovValuev.Id = 0;
                lovValues.Add(lovValuev);
                lovViewModel.Values = lovValues;
            }

            return View("~/Views/Masters/ListOfValuesForm.cshtml", lovViewModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult SaveListOfValues(LovViewModel model)
        {
            //var validation = POCDevWebApps.Enterprise.Processes.Masters.ListOfValuesProcesses.Validate(model);

            //if (!validation.IsValid)
            //{
            //    return JsonValidationError();
            //}

            try
            {
                var listOfValuesRepository = _unitOfWorkDapper._ListOfValuesRepository;
                if (model.Values != null)
                {
                    foreach (LovValue lovValue in model.Values)
                    {
                        bool isInsert = false;
                        Entities.Masters.ListOfValues listOfValues = listOfValuesRepository.GetByPK(lovValue.Id);
                        if (listOfValues == null)
                        {
                            isInsert = true;
                            listOfValues = new Entities.Masters.ListOfValues();
                        }

                        listOfValues.Id = lovValue.Id;
                        listOfValues.Label = lovValue.ProjectLabel;
                        listOfValues.OrderValue = lovValue.Order;
                        listOfValues.Type = model.ProjectType;
                        listOfValues.UserAction = 1;// LoggedInUserID.Value;
                        listOfValues.Value = lovValue.ProjectValue;
                        listOfValues.Description = model.ProjectDescription;
                        listOfValues.FlagDeleted = lovValue.FlagDeleted;
                        //listOfValues.Status = "A";

                        if (isInsert)
                            listOfValuesRepository.Insert(listOfValues.ConvertToDataTable());
                        else
                        {
                            if (!listOfValues.FlagDeleted)
                                listOfValuesRepository.Update(listOfValues.ConvertToDataTable());
                            else
                                listOfValuesRepository.Delete(listOfValues.ConvertToDataTable());
                        }
                    }
                }
                _unitOfWorkDapper.Commit();
                return Json(model);
            }
            catch (Exception ex)
            {
                //LogError(ex, model);
                return Json(ex.Message);
            }
        }
    }
}
