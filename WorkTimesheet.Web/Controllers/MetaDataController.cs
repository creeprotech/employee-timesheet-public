﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkTimesheet.Entities.MetaData;
using WorkTimesheet.Repositories;
using WorkTimesheet.Web.Data;
using WorkTimesheet.Web.Data.Filters;
using WorkTimesheet.Web.ViewModels;

namespace WorkTimesheet.Web.Controllers
{
    [AuthorizeSessionAttribute]
    public class MetaDataController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger<MetaDataController> _logger;
        private readonly ApplicationDbContext _context;
        private readonly IUnitOfWorkDapper _unitOfWorkDapper;

        public MetaDataController(
                UserManager<IdentityUser> userManager,
                RoleManager<IdentityRole> roleManager,
                ApplicationDbContext context,
                ILogger<MetaDataController> logger,
                IUnitOfWorkDapper unitOfWorkDapper)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _context = context;
            _logger = logger;
            _unitOfWorkDapper = unitOfWorkDapper;
        }

        public IActionResult ModulesView()
        {
            var _modulesRepository = _unitOfWorkDapper._ModulesRepository;
            var modules = _modulesRepository.GetAll();

            List<ModuleViewModel> moduleViewModels = new List<ModuleViewModel>();

            foreach (Modules _module in modules)
            {
                ModuleViewModel moduleViewModel = new ModuleViewModel();
                moduleViewModel.Id = _module.Id.ToString();
                moduleViewModel.ModuleName = _module.ModuleName;
                moduleViewModel.Description = _module.Description;
                moduleViewModel.ModuleUUID = _module.Id.ToString();
                moduleViewModel.ModuleValue = _module.ModuleValue;
                moduleViewModel.PermissionValue = _module.PermissionValue;
                moduleViewModel.ControlValue = _module.ControlValue;

                moduleViewModel.CanView = ((_module.PermissionValue & 1) == 1);
                moduleViewModel.CanAdd = ((_module.PermissionValue & 2) == 2);
                moduleViewModel.CanEdit = ((_module.PermissionValue & 4) == 4);
                moduleViewModel.CanDelete = ((_module.PermissionValue & 8) == 8);
                moduleViewModel.CanSpecialRights = ((_module.PermissionValue & 16) == 16);
                moduleViewModel.CanExport = ((_module.PermissionValue & 32) == 32);
                moduleViewModels.Add(moduleViewModel);
            }
            ModuleMasterModel moduleMasterModel = new ModuleMasterModel();
            moduleMasterModel.Individuals = moduleViewModels;

            return View(moduleMasterModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult SaveModuleMaster(ModuleViewModel model)
        {
            try
            {
                var modulesRepository = _unitOfWorkDapper._ModulesRepository;
                if (model != null)
                {
                    bool isInsert = false;
                    WorkTimesheet.Entities.MetaData.Modules modules = modulesRepository.GetByPK(model.Id);
                    if (modules == null)
                    {
                        isInsert = true;
                        modules = new WorkTimesheet.Entities.MetaData.Modules();
                        modules.Id = Guid.NewGuid();
                    }

                    modules.ModuleName = model.ModuleName;
                    modules.Description = model.Description;
                    modules.ModuleValue = model.ModuleValue;
                    modules.ControlValue = model.ControlValue;
                    Data.Authorization.PermissionType permissionType = Data.Authorization.PermissionType.None;
                    if (model.CanView)
                        permissionType = permissionType | Data.Authorization.PermissionType.View;
                    if (model.CanAdd)
                        permissionType = permissionType | Data.Authorization.PermissionType.Add;
                    if (model.CanEdit)
                        permissionType = permissionType | Data.Authorization.PermissionType.Update;
                    if (model.CanDelete)
                        permissionType = permissionType | Data.Authorization.PermissionType.Delete;
                    if (model.CanSpecialRights)
                        permissionType = permissionType | Data.Authorization.PermissionType.SpecialRights;
                    if (model.CanExport)
                        permissionType = permissionType | Data.Authorization.PermissionType.Export;
                    modules.PermissionValue = (int)permissionType;
                    modules.UserAction = 1;// LoggedInUserID.Value;

                    if (isInsert)
                        modulesRepository.Insert(modules.ConvertToDataTable());
                    else
                        modulesRepository.Update(modules.ConvertToDataTable());

                    _unitOfWorkDapper.Commit();
                }
                return Json(model);
            }
            catch (Exception ex)
            {
                //LogError(ex, model);
                return Json(ex.Message);
            }
        }

        public IActionResult MenusView()
        {
            var _menusRepository = _unitOfWorkDapper._NavigationMenuRepository;
            var menus = _menusRepository.GetAll();

            List<MenuViewModel> menuViewModels = new List<MenuViewModel>();

            foreach (NavigationMenu _menu in menus)
            {
                MenuViewModel menuViewModel = new MenuViewModel();
                menuViewModel.Id = _menu.Id.ToString();
                menuViewModel.Title = _menu.Title;
                menuViewModel.Description = _menu.Description;
                menuViewModel.ParentId = _menu.ParentId.ToString();
                menuViewModel.ParentName = menus.Where(m => m.Id == _menu.ParentId).FirstOrDefault() == null ? null : menus.Where(m => m.Id == _menu.ParentId).FirstOrDefault().Title;
                menuViewModel.Controller = _menu.Controller;
                menuViewModel.Action = _menu.Action;
                menuViewModel.IconClass = _menu.IconClass;
                menuViewModel.MenuOrder = _menu.MenuOrder;
                menuViewModel.MenuUUID = _menu.MenuUUID.ToString();
                menuViewModel.SiteId = _menu.SiteId;
                menuViewModel.IsExternalLink = _menu.IsExternalLink;
                menuViewModel.URL = _menu.URL;
                menuViewModels.Add(menuViewModel);
            }
            MenuMasterModel menuMasterModel = new MenuMasterModel();
            menuMasterModel.Individuals = menuViewModels;

            return View(menuMasterModel);
        }

        public IActionResult MenuForm(string Id = null)
        {
            var menusRepository = _unitOfWorkDapper._NavigationMenuRepository;
            var menus = menusRepository.GetAll();

            var modulesRepository = _unitOfWorkDapper._ModulesRepository;
            var modules = modulesRepository.GetAll();

            MenuViewModel menuViewModel = new MenuViewModel();
            if(Id != null)
            {
                NavigationMenu menu = menusRepository.GetByPK(Id);
                if(menu != null)
                {
                    menuViewModel = new MenuViewModel();
                    menuViewModel.Id = menu.Id.ToString();
                    menuViewModel.Title = menu.Title;
                    menuViewModel.Description = menu.Description;
                    if (menu.ParentId == null)
                        menuViewModel.ParentId = null;
                    else
                        menuViewModel.ParentId = menu.ParentId.ToString();
                    menuViewModel.Controller = menu.Controller;
                    menuViewModel.Action = menu.Action;

                    menuViewModel.IconClass = menu.IconClass;
                    menuViewModel.MenuOrder = menu.MenuOrder;
                    if (menu.MenuUUID == null)
                        menuViewModel.MenuUUID = null;
                    else
                        menuViewModel.MenuUUID = menu.MenuUUID.ToString();
                    menuViewModel.SiteId = menu.SiteId;
                    menuViewModel.IsExternalLink = menu.IsExternalLink;
                    menuViewModel.URL = menu.URL;
                }
            }
            var parentNames = menus.Select(s => new { Id = s.Id, Name = s.Title }).ToList();
            parentNames.Add(new { Id = Guid.Empty, Name = "No Parent" });
            ViewBag.ParentNameType = parentNames.OrderBy(p => p.Id);

            var moduleNames = modules.Select(s => new { Id = s.Id, Name = s.ModuleName }).ToList();
            //moduleNames.Add(new { Id = Guid.Empty, Name = "No Parent" });
            ViewBag.ModuleNameType = moduleNames.OrderBy(p => p.Id);

            return View(menuViewModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult SaveMenuMaster(MenuViewModel model)
        {
            try
            {
                var menusRepository = _unitOfWorkDapper._NavigationMenuRepository;
                if (model != null)
                {
                    bool isInsert = false;
                    WorkTimesheet.Entities.MetaData.NavigationMenu menu = menusRepository.GetByPK(model.Id);
                    if (menu == null)
                    {
                        isInsert = true;
                        menu = new WorkTimesheet.Entities.MetaData.NavigationMenu();
                        menu.Id = Guid.NewGuid();
                    }

                    menu.Title = model.Title;
                    menu.Description = model.Description;
                    if (model.ParentId == Guid.Empty.ToString())
                        menu.ParentId = null;
                    else
                        menu.ParentId = Guid.Parse(model.ParentId);
                    menu.Controller = model.Controller;
                    menu.Action = model.Action;

                    menu.IconClass = model.IconClass;
                    menu.MenuOrder = model.MenuOrder;

                    menu.MenuUUID = Guid.Parse(model.MenuUUID);
                    menu.SiteId = 1;
                    menu.IsExternalLink = model.IsExternalLink;
                    menu.URL = model.URL;
                    menu.UserAction = 1;// LoggedInUserID.Value;

                    if (isInsert)
                        menusRepository.Insert(menu.ConvertToDataTable());
                    else
                        menusRepository.Update(menu.ConvertToDataTable());

                    _unitOfWorkDapper.Commit();
                }
                return Json(model);
            }
            catch (Exception ex)
            {
                //LogError(ex, model);
                return Json(ex.Message);
            }
        }
    }
}
