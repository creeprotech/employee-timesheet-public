﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkTimesheet.Web.Data.Filters;

namespace WorkTimesheet.Web.Controllers
{
    [AuthorizeSessionAttribute]
    public class SettingsController : Controller
    {
        public IActionResult NotificationView()
        {
            return View();
        }
    }
}
