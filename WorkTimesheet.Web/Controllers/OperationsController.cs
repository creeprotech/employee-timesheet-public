﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkTimesheet.Repositories;
using WorkTimesheet.Web.Data;
using WorkTimesheet.Web.Data.Filters;
using WorkTimesheet.Web.ViewModels;

namespace WorkTimesheet.Web.Controllers
{
    [AuthorizeSessionAttribute]
    public class OperationsController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger<OperationsController> _logger;
        private readonly ApplicationDbContext _context;
        private readonly IUnitOfWorkDapper _unitOfWorkDapper;

        public OperationsController(
                UserManager<IdentityUser> userManager,
                RoleManager<IdentityRole> roleManager,
                ApplicationDbContext context,
                ILogger<OperationsController> logger,
                IUnitOfWorkDapper unitOfWorkDapper)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _context = context;
            _logger = logger;
            _unitOfWorkDapper = unitOfWorkDapper;
        }

        public IActionResult ZoneView()
        {
            var zoneRepository = _unitOfWorkDapper._OrganizationUnitsRepository;
            var zones = zoneRepository.GetAll();
            ZoneMasterModel zoneMasterModel = new ZoneMasterModel();
            List<ZoneViewModel> zoneViewModels = new List<ZoneViewModel>();
            foreach (Entities.Operations.OrganizationUnits zone in zones)
            {
                ZoneViewModel zoneViewModel = new ZoneViewModel();
                zoneViewModel.Id = zone.Id.ToString();
                zoneViewModel.ZoneName = zone.OUName;
                zoneViewModel.Description = zone.Description;
                zoneViewModel.ParentZone = zone.ParentOU.ToString();
                zoneViewModels.Add(zoneViewModel);
            }
            zoneMasterModel.Individuals = zoneViewModels;
            var parentZones = zones.Select(s => new { Id = s.Id, Name = s.OUName }).ToList();
            //parentZones.Add(new { Id = 0, Name = "No Parent" });
            ViewBag.ParentZoneType = parentZones.OrderBy(p => p.Id);
            return View(zoneMasterModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult SaveZone(ZoneViewModel model)
        {
            try
            {
                var zoneRepository = _unitOfWorkDapper._OrganizationUnitsRepository;
                if (model != null)
                {
                    bool isInsert = false;
                    WorkTimesheet.Entities.Operations.OrganizationUnits zone = zoneRepository.GetByPK(model.Id);
                    if (zone == null)
                    {
                        isInsert = true;
                        zone = new WorkTimesheet.Entities.Operations.OrganizationUnits();
                        zone.Id = Guid.NewGuid();
                    }

                    zone.OUName = model.ZoneName;
                    zone.Description = model.Description;
                    zone.ParentOU = Guid.Parse(model.ParentZone);
                    zone.UserAction = 1;// LoggedInUserID.Value;

                    if (isInsert)
                        zone = zoneRepository.Insert(zone.ConvertToDataTable()).FirstOrDefault();
                    else
                        zoneRepository.Update(zone.ConvertToDataTable());

                    if (zone.Id != null && isInsert)
                    {
                        var groupRepository = _unitOfWorkDapper._GroupsRepository;
                        WorkTimesheet.Entities.Operations.Groups groups = new Entities.Operations.Groups();
                        groups.Id = Guid.NewGuid(); 
                        //groups.GroupUUID = Guid.NewGuid().ToString().ToLower();
                        groups.ParentGroupId = null;
                        groups.GroupType = 0;
                        groups.GroupName = zone.OUName + " Group";
                        groups.Description = zone.OUName + " Group Description";
                        groups.OrganizationUnitId = zone.Id;
                        groupRepository.Insert(groups.ConvertToDataTable());
                    }

                    _unitOfWorkDapper.Commit();
                }
                return Json(model);
            }
            catch (Exception ex)
            {
                //LogError(ex, model);
                return Json(ex.Message);
            }
        }

        public IActionResult GroupsView()
        {
            var groupRepository = _unitOfWorkDapper._GroupsRepository;
            var groups = groupRepository.GetAll();
            GroupMasterModel groupMasterModel = new GroupMasterModel();
            List<GroupViewModel> groupViewModels = new List<GroupViewModel>();

            foreach (Entities.Operations.Groups group in groups)
            {
                GroupViewModel groupViewModel = new GroupViewModel();
                groupViewModel.Id = group.Id.ToString();
                groupViewModel.GroupName = group.GroupName;
                groupViewModel.Description = group.Description;
                groupViewModel.ParentGroup = group.ParentGroupId.ToString();
                groupViewModels.Add(groupViewModel);
            }
            groupMasterModel.Individuals = groupViewModels;
            var parentGroups = groups.Select(s => new { Id = s.Id, Name = s.GroupName }).ToList();
            //parentZones.Add(new { Id = 0, Name = "No Parent" });
            ViewBag.ParentGroupType = parentGroups.OrderBy(p => p.Id);
            return View(groupMasterModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult SaveGroup(GroupViewModel model)
        {
            try
            {
                var groupRepository = _unitOfWorkDapper._GroupsRepository;
                var zoneRepository = _unitOfWorkDapper._OrganizationUnitsRepository;
                if (model != null)
                {
                    bool isInsert = false;
                    WorkTimesheet.Entities.Operations.Groups group = groupRepository.GetByPK(model.Id);
                    if (group == null)
                    {
                        isInsert = true;
                        group = new WorkTimesheet.Entities.Operations.Groups();
                        group.Id = Guid.NewGuid();
                    }

                    group.GroupName = model.GroupName;
                    group.Description = model.Description;
                    group.GroupType = 1;
                    group.ParentGroupId = Guid.Parse(model.ParentGroup);
                    group.OrganizationUnitId = groupRepository.GetAll().Where(g => g.Id == Guid.Parse(model.ParentGroup)).FirstOrDefault().OrganizationUnitId;
                    group.UserAction = 1;// LoggedInUserID.Value;

                    if (isInsert)
                        groupRepository.Insert(group.ConvertToDataTable());
                    else
                        groupRepository.Update(group.ConvertToDataTable());

                    _unitOfWorkDapper.Commit();
                }
                return Json(model);
            }
            catch (Exception ex)
            {
                //LogError(ex, model);
                return Json(ex.Message);
            }
        }

        public IActionResult OrganizationView()
        {
            return View();
        }

        public IActionResult RolesView()
        {
            var roles = (from _roles in _context.Roles
                         select _roles)
                              .Select(r => new RoleViewModel()
                              {
                                  Id = r.Id,
                                  RoleName = r.Name,
                                  Description = r.Name,
                              }).Distinct().OrderBy(a => a.RoleName).ToListAsync();

            RoleMasterModel roleMasterModel = new RoleMasterModel();
            roleMasterModel.Individuals = roles.Result.ToList();

            return View(roleMasterModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult SaveRoleMaster(RoleViewModel model)
        {
            try
            {
                var roleRepository = _unitOfWorkDapper._RolesRepository;
                if (model != null)
                {
                    bool isInsert = false;
                    WorkTimesheet.Entities.Operations.Roles role = roleRepository.GetByPK(model.Id);
                    if (role == null)
                    {
                        isInsert = true;
                        role = new WorkTimesheet.Entities.Operations.Roles();
                        role.Id = Guid.NewGuid().ToString().ToUpper();
                        role.ConcurrencyStamp = Guid.NewGuid().ToString().ToLower();
                    }

                    role.Name = model.RoleName;
                    role.NormalizedName = model.RoleName.ToUpper();
                    role.UserAction = 1;// LoggedInUserID.Value;

                    if (isInsert)
                        roleRepository.Insert(role.ConvertToDataTable());
                    else
                        roleRepository.Update(role.ConvertToDataTable());

                    _unitOfWorkDapper.Commit();
                }
                return Json(model);
            }
            catch (Exception ex)
            {
                //LogError(ex, model);
                return Json(ex.Message);
            }
        }

        public IActionResult RolePermission(string Id = null)
        {
            var roles = (from _role in _context.Roles where _role.Id == Id select _role);
            var role = roles.FirstOrDefault();

            var moduleRules = (from _modules in _context.Modules
                                   //where _modules.ControlValue != 0
                               select _modules)
                              .Select(m => new RuleViewModel()
                              {
                                  Id = 0,
                                  RuleId = m.Id.ToString(),
                                  RuleName = m.ModuleName,
                                  RuleUUID = m.Id.ToString(),
                                  RuleValue = 0,
                                  ModuleValue = m.ModuleValue,
                                  ControlValue = m.ControlValue,
                                  PermissionValue = m.PermissionValue,
                              }).Distinct().OrderBy(a => a.ModuleValue).ToListAsync().Result;

            var rules = (from _rules in _context.RoleMenuPermission
                         where _rules.RoleId == role.Id //&& _rules.Modules.ControlValue != 0
                         select _rules)
                              .Select(m => new RuleViewModel()
                              {
                                  Id = m.Id,
                                  RuleId = m.PermissionId.ToString(),
                                  RuleName = m.Modules.ModuleName,
                                  RuleUUID = m.PermissionId.ToString(),
                                  RuleValue = m.PermissionValue,
                                  ModuleValue = m.Modules.ModuleValue,
                                  ControlValue = m.Modules.ControlValue,
                                  PermissionValue = m.Modules.PermissionValue,
                              }).Distinct().OrderBy(a => a.RuleId).ToListAsync().Result;

            var q = (from c in moduleRules
                     join o in rules on c.RuleUUID equals o.RuleUUID
                     into leftRules
                     from o in leftRules.DefaultIfEmpty()
                     select new
                     {
                         Id = (o == null) ? c.Id : o.Id,
                         RuleId = (o == null) ? c.RuleId : o.RuleId,
                         RuleName = c.RuleName,
                         RuleUUID = c.RuleUUID,
                         RuleValue = (o == null) ? 0 : o.RuleValue,
                         ModuleValue = c.ModuleValue,
                         ControlValue = c.ControlValue,
                         PermissionValue = c.PermissionValue
                     })
                    .Select(m => new RuleViewModel()
                    {
                        Id = m.Id,
                        RuleId = m.RuleId,
                        RuleName = m.RuleName,
                        RuleUUID = m.RuleUUID,
                        RuleValue = m.RuleValue,
                        ModuleValue = m.ModuleValue,
                        ControlValue = m.ControlValue,
                        PermissionValue = m.PermissionValue,
                    }).Distinct().OrderBy(a => a.ModuleValue).ThenBy(b => b.ControlValue).ToList();

            #region 

            //var q = from c in moduleRules
            //         join o in rules on c.RuleUUID equals o.RuleUUID
            //         into leftRules
            //         from o in leftRules.DefaultIfEmpty()
            //         select new {
            //             RuleId = (o == null) ? 0 : o.RuleId,
            //             RuleName = c.RuleName,
            //             RuleUUID = c.RuleUUID,
            //             RuleValue = (o == null) ? 0 : o.RuleValue,
            //             ModuleValue = c.ModuleValue,
            //             ControlValue = c.ControlValue,
            //             PermissionValue = c.PermissionValue
            //         };

            //var q = (from c in moduleRules
            //         join o in rules on c.RuleUUID equals o.RuleUUID
            //         into leftRules
            //         from o in leftRules.DefaultIfEmpty()
            //         select o)
            //        .Select(m => new RuleViewModel()
            //        {
            //            RuleId = m.RuleId,
            //            RuleName = m.RuleName,
            //            RuleUUID = m.RuleName,
            //            RuleValue = m.RuleValue,
            //            ModuleValue = m.ModuleValue,
            //            ControlValue = m.ControlValue,
            //            PermissionValue = m.PermissionValue,
            //        }).Distinct().OrderBy(a => a.RuleId).ToList();

            //var mapped = moduleRules.Join(rules, d => d.RuleUUID, s => s.RuleUUID, (d, s) =>
            //{
            //    d.RuleId = s.RuleId;
            //    d.RuleName = s.RuleName;
            //    d.RuleUUID = s.RuleUUID;
            //    d.RuleValue = s.RuleValue;
            //    d.ModuleValue = s.ModuleValue;
            //    d.ControlValue = s.ControlValue;
            //    d.PermissionValue = s.PermissionValue;
            //    return d;
            //}).ToList();

            //var result = from source in moduleRules.Result
            //             join destination in rules.Result on source.RuleUUID equals destination.RuleUUID
            //             into matchRules;

            #endregion

            RolePermissionMasterModel rolePermissionMasterModel = new RolePermissionMasterModel();
            rolePermissionMasterModel.Id = role.Id;
            rolePermissionMasterModel.RoleName = role.Name;
            rolePermissionMasterModel.Description = role.NormalizedName;

            List<RolePermissionViewModel> rolePermissionViewModels = new List<RolePermissionViewModel>();
            foreach (RuleViewModel rulePermission in q)
            {
                RolePermissionViewModel rolePermissionViewModel = new RolePermissionViewModel();
                rolePermissionViewModel.Id = rulePermission.Id;
                rolePermissionViewModel.PermissionId = rulePermission.RuleId;
                rolePermissionViewModel.PermissionUuid = rulePermission.RuleUUID;
                rolePermissionViewModel.ModuleValue = rulePermission.ModuleValue;
                rolePermissionViewModel.ControlValue = rulePermission.ControlValue;
                rolePermissionViewModel.FeatureName = rulePermission.RuleName;
                rolePermissionViewModel.CanView = ((rulePermission.RuleValue & 1) == 1);
                rolePermissionViewModel.CanAdd = ((rulePermission.RuleValue & 2) == 2);
                rolePermissionViewModel.CanEdit = ((rulePermission.RuleValue & 4) == 4);
                rolePermissionViewModel.CanDelete = ((rulePermission.RuleValue & 8) == 8);
                rolePermissionViewModel.CanSpecialRights = ((rulePermission.RuleValue & 16) == 16);
                rolePermissionViewModel.CanExport = ((rulePermission.RuleValue & 32) == 32);
                rolePermissionViewModel.PermissionValue = rulePermission.PermissionValue;
                rolePermissionViewModels.Add(rolePermissionViewModel);
            }
            if (rolePermissionViewModels.Count > 0)
                rolePermissionMasterModel.RolePermissions = rolePermissionViewModels;

            return View(rolePermissionMasterModel);
        }

        [HttpPost]
        public ActionResult SaveRolePermissions(RolePermissionMasterModel model)
        {
            var rulesRepository = _unitOfWorkDapper._RoleMenuPermissionRepository;
            var roleClaimsRepository = _unitOfWorkDapper._RolesClaimsRepository;

            List<Data.Authorization.RulePermission> rulePermissions = new List<Data.Authorization.RulePermission>();
            Data.Authorization.RulePermission rulePermission = new Data.Authorization.RulePermission();

            foreach (RolePermissionViewModel rolePermissionViewModel in model.RolePermissions)
            {
                bool isInsert = false;
                WorkTimesheet.Entities.Operations.RoleMenuPermission rules = null;

                rules = rulesRepository.GetByPK(rolePermissionViewModel.Id);

                if (rules == null)
                {
                    isInsert = true;
                    rules = new WorkTimesheet.Entities.Operations.RoleMenuPermission();
                    rules.PermissionId = Guid.Parse(rolePermissionViewModel.PermissionId);
                }
                rules.RoleId = model.Id;

                Data.Authorization.PermissionType permissionType = Data.Authorization.PermissionType.None;
                if (rolePermissionViewModel.CanView)
                    permissionType = permissionType | Data.Authorization.PermissionType.View;
                if (rolePermissionViewModel.CanAdd)
                    permissionType = permissionType | Data.Authorization.PermissionType.Add;
                if (rolePermissionViewModel.CanEdit)
                    permissionType = permissionType | Data.Authorization.PermissionType.Update;
                if (rolePermissionViewModel.CanDelete)
                    permissionType = permissionType | Data.Authorization.PermissionType.Delete;
                if (rolePermissionViewModel.CanSpecialRights)
                    permissionType = permissionType | Data.Authorization.PermissionType.SpecialRights;
                if (rolePermissionViewModel.CanExport)
                    permissionType = permissionType | Data.Authorization.PermissionType.Export;

                if (rolePermissionViewModel.ControlValue == 0)
                {
                    permissionType = Data.Authorization.PermissionType.None;
                    var _rolePermissions = model.RolePermissions.Where(r => r.ModuleValue == rolePermissionViewModel.ModuleValue && r.ControlValue != 0).ToList();
                    if (_rolePermissions != null)
                    {
                        RolePermissionViewModel _rolePermissionViewModel = _rolePermissions.Find(r => r.CanAdd == true || r.CanDelete == true || r.CanEdit == true || r.CanExport == true || r.CanSpecialRights == true || r.CanView == true);
                        if (_rolePermissionViewModel == null)
                            permissionType = Data.Authorization.PermissionType.None;
                        else
                            permissionType = Data.Authorization.PermissionType.View;
                    }
                }

                rules.PermissionValue = (int)permissionType;
                rules.UserAction = 1;

                if (isInsert)
                    rulesRepository.Insert(rules.ConvertToDataTable());
                else
                    rulesRepository.Update(rules.ConvertToDataTable());

                rulePermission = new Data.Authorization.RulePermission();
                rulePermission.ruleUUID = rolePermissionViewModel.PermissionUuid;
                rulePermission.moduleValue = rolePermissionViewModel.ModuleValue;
                rulePermission.controlValue = rolePermissionViewModel.ControlValue;
                rulePermission.permissionValue = permissionType;
                rulePermissions.Add(rulePermission);
            }

            List<Entities.Operations.RoleClaims> aspNetRoleClaims = roleClaimsRepository.GetAll().ToList();
            if (aspNetRoleClaims != null)
            {
                Data.Authorization.RuleClaims ruleClaims = new Data.Authorization.RuleClaims();
                ruleClaims.rulePermissions = rulePermissions.ToArray();
                string output = JsonConvert.SerializeObject(ruleClaims);

                Entities.Operations.RoleClaims aspNetRoleClaim = aspNetRoleClaims.Where(a => a.RoleId == model.Id && a.ClaimType == "Permission").FirstOrDefault();
                if (aspNetRoleClaim == null)
                {
                    aspNetRoleClaim = new Entities.Operations.RoleClaims();
                    aspNetRoleClaim.ClaimType = "Permission";
                    aspNetRoleClaim.ClaimValue = output;
                    aspNetRoleClaim.RoleId = model.Id;
                    aspNetRoleClaim.UserAction = 1;
                    roleClaimsRepository.Insert(aspNetRoleClaim.ConvertToDataTable());
                }
                else
                {
                    aspNetRoleClaim.ClaimValue = output;
                    aspNetRoleClaim.UserAction = 1;
                    roleClaimsRepository.Update(aspNetRoleClaim.ConvertToDataTable());
                }
            }
            _unitOfWorkDapper.Commit();
            return Json("success");
        }

        public IActionResult UsersView()
        {
            var userRepository = _unitOfWorkDapper._UsersRepository;
            var userRolesRepository = _unitOfWorkDapper._UserRolesRepository;
            var groupRepository = _unitOfWorkDapper._GroupsRepository;
            var userGroupsRepository = _unitOfWorkDapper._UserGroupsRepository;
            var roleRepository = _unitOfWorkDapper._RolesRepository;
            var users = userRepository.GetAll();
            var roles = roleRepository.GetAll();
            var groups = groupRepository.GetAll();

            var parentGroups = groups.Select(s => new { Id = s.Id, Name = s.GroupName }).ToList();
            ViewBag.GroupType = parentGroups.OrderBy(p => p.Name);
            var parentroles = roles.Select(s => new { Id = s.Id, Name = s.Name }).ToList();
            ViewBag.RoleType = parentroles.OrderBy(p => p.Name);

            UserMasterModel userMasterModel = new UserMasterModel();
            List<UserViewModel> userViewModels = new List<UserViewModel>();
            foreach (WorkTimesheet.Entities.Operations.Users aspNetUsers in users)
            {
                UserViewModel userViewModel = new UserViewModel();
                userViewModel.Id = aspNetUsers.Id;
                //userViewModel.UserId = aspNetUsers.UserId;
                userViewModel.UserName = aspNetUsers.UserName;
                userViewModel.PhoneNumber = aspNetUsers.PhoneNumber;
                userViewModel.RoleId = userRolesRepository.GetAll().Where(u => u.UserId == aspNetUsers.Id).FirstOrDefault().RoleId;
                userViewModel.GroupId = userGroupsRepository.GetAll().Where(u => u.UserId == aspNetUsers.Id).FirstOrDefault().GroupId;
                userViewModels.Add(userViewModel);
            }

            userMasterModel.Individuals = userViewModels;

            return View(userMasterModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult SaveUser(UserViewModel model)
        {
            try
            {
                var userRepository = _unitOfWorkDapper._UsersRepository;
                if (model != null)
                {
                    bool isInsert = false;
                    WorkTimesheet.Entities.Operations.Users user = userRepository.GetByPK(model.Id);
                    if (user == null)
                    {
                        isInsert = true;
                        user = new WorkTimesheet.Entities.Operations.Users();
                        user.Id = Guid.NewGuid().ToString().ToLower();
                        user.ConcurrencyStamp = Guid.NewGuid().ToString().ToLower();
                        PasswordHasher<string> passwordHasher = new PasswordHasher<string>();
                        user.PasswordHash = passwordHasher.HashPassword(model.UserName, "PassW@rd!1");
                        user.EmailConfirmed = false;
                        user.PhoneNumberConfirmed = false;
                        user.TwoFactorEnabled = false;
                        user.LockoutEnd = null;
                        user.LockoutEnabled = true;
                        user.AccessFailedCount = 0;
                        user.LastLogOn = null;
                        user.PasswordModifiedTime = null;
                        var guid = Guid.NewGuid();
                        user.SecurityStamp = String.Concat(Array.ConvertAll(guid.ToByteArray(), b => b.ToString("X2")));
                        user.UserAction = 1;// LoggedInUserID.Value;
                    }
                    user.UserName = user.Email = model.UserName;
                    user.NormalizedUserName = user.NormalizedEmail = model.UserName.ToUpper();
                    user.PhoneNumber = model.PhoneNumber;

                    if (isInsert)
                        user = userRepository.Insert(user.ConvertToDataTable()).FirstOrDefault();
                    else
                        userRepository.Update(user.ConvertToDataTable());

                    //_unitOfWorkDapper.Commit();

                    var userRolesRepository = _unitOfWorkDapper._UserRolesRepository;

                    WorkTimesheet.Entities.Operations.UserRoles aspNetUserRoles = new WorkTimesheet.Entities.Operations.UserRoles();
                    aspNetUserRoles.RoleId = model.RoleId;
                    aspNetUserRoles.UserId = user.Id;
                    aspNetUserRoles.UserAction = 1;// LoggedInUserID.Value;

                    if (isInsert)
                        userRolesRepository.Insert(aspNetUserRoles.ConvertToDataTable());
                    else
                        userRolesRepository.Update(aspNetUserRoles.ConvertToDataTable());

                    //_unitOfWorkDapper.Commit();

                    var userGroupsRepository = _unitOfWorkDapper._UserGroupsRepository;

                    WorkTimesheet.Entities.Operations.UserGroups aspNetUserGroups = new WorkTimesheet.Entities.Operations.UserGroups();
                    aspNetUserGroups.GroupId = model.GroupId;
                    aspNetUserGroups.UserId = user.Id;
                    aspNetUserGroups.UserAction = 1;// LoggedInUserID.Value;

                    if (isInsert)
                        userGroupsRepository.Insert(aspNetUserGroups.ConvertToDataTable());
                    else
                        userGroupsRepository.Update(aspNetUserGroups.ConvertToDataTable());

                    _unitOfWorkDapper.Commit();
                }
                return Json(model);
            }
            catch (Exception ex)
            {
                //LogError(ex, model);
                return Json(ex.Message);
            }
        }
    }
}
