﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WorkTimesheet.Web.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetModules",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ModuleName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    ModuleValue = table.Column<int>(nullable: false),
                    ControlValue = table.Column<int>(nullable: false),
                    PermissionValue = table.Column<int>(nullable: false),
                    FlagDeleted = table.Column<bool>(nullable: false),
                    UserCreated = table.Column<int>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserAmended = table.Column<int>(nullable: true),
                    DateAmended = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetModules", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetOrganizationUnits",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    OUName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    ParentOU = table.Column<Guid>(nullable: true),
                    FlagDeleted = table.Column<bool>(nullable: false),
                    UserCreated = table.Column<int>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserAmended = table.Column<int>(nullable: true),
                    DateAmended = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetOrganizationUnits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetOrganizationUnits_AspNetOrganizationUnits_ParentOU",
                        column: x => x.ParentOU,
                        principalTable: "AspNetOrganizationUnits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    FlagDeleted = table.Column<bool>(nullable: true),
                    UserCreated = table.Column<int>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserAmended = table.Column<int>(nullable: true),
                    DateAmended = table.Column<DateTime>(nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserGroups",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(nullable: true),
                    GroupId = table.Column<string>(nullable: true),
                    FlagDeleted = table.Column<bool>(nullable: false),
                    UserCreated = table.Column<int>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserAmended = table.Column<int>(nullable: true),
                    DateAmended = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserGroups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    LastLogOn = table.Column<DateTime>(nullable: true),
                    PasswordModifiedTime = table.Column<DateTime>(nullable: true),
                    FlagDeleted = table.Column<bool>(nullable: true),
                    UserCreated = table.Column<int>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserAmended = table.Column<int>(nullable: true),
                    DateAmended = table.Column<DateTime>(nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ListOfValues",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Label = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true),
                    OrderValue = table.Column<int>(nullable: false),
                    SiteId = table.Column<int>(nullable: false),
                    FlagDeleted = table.Column<bool>(nullable: false),
                    UserCreated = table.Column<int>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserAmended = table.Column<int>(nullable: true),
                    DateAmended = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ListOfValues", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetNavigationMenu",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    MenuUUID = table.Column<Guid>(nullable: true),
                    ParentId = table.Column<Guid>(nullable: true),
                    Controller = table.Column<string>(nullable: true),
                    Action = table.Column<string>(nullable: true),
                    IconClass = table.Column<string>(nullable: true),
                    MenuOrder = table.Column<int>(nullable: false),
                    SiteId = table.Column<int>(nullable: false),
                    IsExternalLink = table.Column<bool>(nullable: false),
                    URL = table.Column<string>(nullable: true),
                    FlagDeleted = table.Column<bool>(nullable: false),
                    UserCreated = table.Column<int>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserAmended = table.Column<int>(nullable: true),
                    DateAmended = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetNavigationMenu", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetNavigationMenu_AspNetModules_MenuUUID",
                        column: x => x.MenuUUID,
                        principalTable: "AspNetModules",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AspNetNavigationMenu_AspNetNavigationMenu_ParentId",
                        column: x => x.ParentId,
                        principalTable: "AspNetNavigationMenu",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleMenuPermission",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PermissionId = table.Column<Guid>(nullable: true),
                    PermissionValue = table.Column<int>(nullable: false),
                    RoleId = table.Column<string>(nullable: true),
                    FlagDeleted = table.Column<bool>(nullable: false),
                    UserCreated = table.Column<int>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserAmended = table.Column<int>(nullable: true),
                    DateAmended = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleMenuPermission", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleMenuPermission_AspNetModules_PermissionId",
                        column: x => x.PermissionId,
                        principalTable: "AspNetModules",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetGroups",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    GroupName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    ParentGroupId = table.Column<Guid>(nullable: true),
                    GroupType = table.Column<int>(nullable: true),
                    OrganizationUnitId = table.Column<Guid>(nullable: true),
                    FlagDeleted = table.Column<bool>(nullable: false),
                    UserCreated = table.Column<int>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserAmended = table.Column<int>(nullable: true),
                    DateAmended = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetGroups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetGroups_AspNetOrganizationUnits_OrganizationUnitId",
                        column: x => x.OrganizationUnitId,
                        principalTable: "AspNetOrganizationUnits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AspNetGroups_AspNetGroups_ParentGroupId",
                        column: x => x.ParentGroupId,
                        principalTable: "AspNetGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    FlagDeleted = table.Column<bool>(nullable: true),
                    UserCreated = table.Column<int>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserAmended = table.Column<int>(nullable: true),
                    DateAmended = table.Column<DateTime>(nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    FlagDeleted = table.Column<bool>(nullable: true),
                    UserCreated = table.Column<int>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserAmended = table.Column<int>(nullable: true),
                    DateAmended = table.Column<DateTime>(nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderKey = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false),
                    FlagDeleted = table.Column<bool>(nullable: true),
                    UserCreated = table.Column<int>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserAmended = table.Column<int>(nullable: true),
                    DateAmended = table.Column<DateTime>(nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false),
                    FlagDeleted = table.Column<bool>(nullable: true),
                    UserCreated = table.Column<int>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserAmended = table.Column<int>(nullable: true),
                    DateAmended = table.Column<DateTime>(nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    Name = table.Column<string>(maxLength: 128, nullable: false),
                    Value = table.Column<string>(nullable: true),
                    FlagDeleted = table.Column<bool>(nullable: true),
                    UserCreated = table.Column<int>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    UserAmended = table.Column<int>(nullable: true),
                    DateAmended = table.Column<DateTime>(nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetGroups_OrganizationUnitId",
                table: "AspNetGroups",
                column: "OrganizationUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetGroups_ParentGroupId",
                table: "AspNetGroups",
                column: "ParentGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetNavigationMenu_MenuUUID",
                table: "AspNetNavigationMenu",
                column: "MenuUUID");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetNavigationMenu_ParentId",
                table: "AspNetNavigationMenu",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetOrganizationUnits_ParentOU",
                table: "AspNetOrganizationUnits",
                column: "ParentOU");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleMenuPermission_PermissionId",
                table: "AspNetRoleMenuPermission",
                column: "PermissionId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetGroups");

            migrationBuilder.DropTable(
                name: "AspNetNavigationMenu");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetRoleMenuPermission");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserGroups");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "ListOfValues");

            migrationBuilder.DropTable(
                name: "AspNetOrganizationUnits");

            migrationBuilder.DropTable(
                name: "AspNetModules");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");
        }
    }
}
