﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.Data
{
    public static class DbInitializer
    {
        public static void Initialize(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
                context.Database.EnsureCreated();

                var _userManager =
                         serviceScope.ServiceProvider.GetService<UserManager<IdentityUser>>();
                var _roleManager =
                         serviceScope.ServiceProvider.GetService<RoleManager<IdentityRole>>();

                #region Users
                if (!context.Users.Any(usr => usr.UserName == "ceo@creepro.com"))
                {
                    var user = new IdentityUser()
                    {
                        UserName = "ceo@creepro.com",
                        Email = "ceo@creepro.com",
                        EmailConfirmed = true,
                    };

                    var userResult = _userManager.CreateAsync(user, "Creepro@1").Result;
                }

                if (!context.Users.Any(usr => usr.UserName == "admin@creepro.com"))
                {
                    var user = new IdentityUser()
                    {
                        UserName = "admin@creepro.com",
                        Email = "admin@creepro.com",
                        EmailConfirmed = true,
                    };

                    var userResult = _userManager.CreateAsync(user, "Creepro@1").Result;
                }


                if (!context.Users.Any(usr => usr.UserName == "vps@creepro.com"))
                {
                    var user = new IdentityUser()
                    {
                        UserName = "vps@creepro.com",
                        Email = "vps@creepro.com",
                        EmailConfirmed = true,
                    };

                    var userResult = _userManager.CreateAsync(user, "Creepro@1").Result;
                }
                #endregion

                #region Roles
                //Creating Role
                if (!_roleManager.RoleExistsAsync("SystemAdmin").Result)
                {
                    var role = _roleManager.CreateAsync
                               (new IdentityRole { Name = "SystemAdmin" }).Result;
                }

                if (!_roleManager.RoleExistsAsync("Administrator").Result)
                {
                    var role = _roleManager.CreateAsync
                               (new IdentityRole { Name = "Administrator" }).Result;
                }

                if (!_roleManager.RoleExistsAsync("Employee").Result)
                {
                    var role = _roleManager.CreateAsync
                               (new IdentityRole { Name = "Employee" }).Result;
                }
                #endregion

                #region UserRoles
                var _user = _userManager.FindByNameAsync("ceo@creepro.com").Result;
                var _role = _userManager.AddToRolesAsync
                               (_user, new string[] { "SystemAdmin" }).Result;


                _user = _userManager.FindByNameAsync("admin@creepro.com").Result;
                _role = _userManager.AddToRolesAsync
                               (_user, new string[] { "Administrator" }).Result;


                _user = _userManager.FindByNameAsync("vps@creepro.com").Result;
                _role = _userManager.AddToRolesAsync
                               (_user, new string[] { "Employee" }).Result;
                #endregion

                #region OrganizationUnits & Groups

                var _organizationUnits = context.OrganizationUnits
                                  .Where(x => x.OUName == "Default OU").FirstOrDefault();

                if (_organizationUnits == null)
                {
                    _organizationUnits = new Models.AspNetOrganizationUnits()
                    {
                        OUName = "Default OU",
                        Description = "Default OU Description",
                        ParentOU = null,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.OrganizationUnits.Add(_organizationUnits);
                    context.SaveChanges();
                }

                var _groups = context.Groups
                                  .Where(x => x.GroupName == "Default OU Group").FirstOrDefault();

                if (_groups == null)
                {
                    _groups = new Models.AspNetGroups()
                    {
                        GroupName = "Default OU Group",
                        Description = "Default OU Group Description",
                        ParentGroupId = null,
                        GroupType = 0,
                        OrganizationUnitId = context.OrganizationUnits.Where(x => x.OUName == "Default OU").FirstOrDefault().Id,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Groups.Add(_groups);
                    context.SaveChanges();
                }

                _organizationUnits = context.OrganizationUnits
                                  .Where(x => x.OUName == "India").FirstOrDefault();
                if (_organizationUnits == null)
                {
                    _organizationUnits = new Models.AspNetOrganizationUnits()
                    {
                        OUName = "India",
                        Description = "India Description",
                        ParentOU = context.OrganizationUnits.Where(x => x.OUName == "Default OU").FirstOrDefault().Id,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.OrganizationUnits.Add(_organizationUnits);
                    context.SaveChanges();
                }

                _groups = context.Groups
                                  .Where(x => x.GroupName == "India Group").FirstOrDefault();

                if (_groups == null)
                {
                    _groups = new Models.AspNetGroups()
                    {
                        GroupName = "India Group",
                        Description = "India Group Description",
                        ParentGroupId = null,
                        GroupType = 0,
                        OrganizationUnitId = context.OrganizationUnits.Where(x => x.OUName == "India").FirstOrDefault().Id,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Groups.Add(_groups);
                    context.SaveChanges();
                }
                #endregion

                #region UserGroups
                _user = _userManager.FindByNameAsync("ceo@creepro.com").Result;
                _groups = context.Groups
                                  .Where(x => x.GroupName == "Default OU Group").FirstOrDefault();

                var _usergroups = context.UserGroups
                                  .Where(x => x.UserId == _user.Id && x.GroupId == _groups.Id.ToString()).FirstOrDefault();

                if (_usergroups == null)
                {
                    _usergroups = new Models.AspNetUserGroups()
                    {
                        UserId = _user.Id,
                        GroupId = _groups.Id.ToString(),
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.UserGroups.Add(_usergroups);
                    context.SaveChanges();
                }

                _user = _userManager.FindByNameAsync("admin@creepro.com").Result;
                _groups = context.Groups
                                  .Where(x => x.GroupName == "India Group").FirstOrDefault();

                _usergroups = context.UserGroups
                                  .Where(x => x.UserId == _user.Id && x.GroupId == _groups.Id.ToString()).FirstOrDefault();

                if (_usergroups == null)
                {
                    _usergroups = new Models.AspNetUserGroups()
                    {
                        UserId = _user.Id,
                        GroupId = _groups.Id.ToString(),
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.UserGroups.Add(_usergroups);
                    context.SaveChanges();
                }

                _user = _userManager.FindByNameAsync("vps@creepro.com").Result;
                _groups = context.Groups
                                  .Where(x => x.GroupName == "India Group").FirstOrDefault();

                _usergroups = context.UserGroups
                                  .Where(x => x.UserId == _user.Id && x.GroupId == _groups.Id.ToString()).FirstOrDefault();

                if (_usergroups == null)
                {
                    _usergroups = new Models.AspNetUserGroups()
                    {
                        UserId = _user.Id,
                        GroupId = _groups.Id.ToString(),
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.UserGroups.Add(_usergroups);
                    context.SaveChanges();
                }
                #endregion

                #region Modules
                var _roleSystemAdminId = _roleManager.Roles.Where(r => r.Name == "SystemAdmin").FirstOrDefault().Id;

                #region Master Modules
                var _modules = context.Modules
                                  .Where(x => x.ModuleName == "Masters").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "Masters",
                        Description = "Masters Description",
                        ModuleValue = 1,
                        ControlValue = 0,
                        PermissionValue = 0,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }

                var _navigationMenu = context.NavigationMenu
                                 .Where(x => x.Title == "Masters").FirstOrDefault();

                if (_navigationMenu == null)
                {
                    _navigationMenu = new Models.AspNetNavigationMenu()
                    {
                        Title = "Masters",
                        Description = "Masters Description",
                        MenuUUID = context.Modules.Where(x => x.ModuleName == "Masters").FirstOrDefault().Id,
                        ParentId = null,
                        Controller = null,
                        Action = null,
                        IconClass = "fa fa-superpowers",
                        MenuOrder = 1,
                        SiteId = 1,
                        IsExternalLink = false,
                        URL = null,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.NavigationMenu.Add(_navigationMenu);
                    context.SaveChanges();
                }

                var _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 1,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }

                _modules = context.Modules
                                  .Where(x => x.ModuleName == "List Of Values (LOV)").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "List Of Values (LOV)",
                        Description = "List Of Values (LOV) Description",
                        ModuleValue = 1,
                        ControlValue = 1,
                        PermissionValue = 15,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }

                _navigationMenu = context.NavigationMenu
                                 .Where(x => x.Title == "List Of Values (LOV)").FirstOrDefault();

                if (_navigationMenu == null)
                {
                    _navigationMenu = new Models.AspNetNavigationMenu()
                    {
                        Title = "List Of Values (LOV)",
                        Description = "List Of Values (LOV) Description",
                        MenuUUID = context.Modules.Where(x => x.ModuleName == "List Of Values (LOV)").FirstOrDefault().Id,
                        ParentId = context.NavigationMenu.Where(x => x.Title == "Masters").FirstOrDefault().Id,
                        Controller = "Masters",
                        Action = "LOVView",
                        IconClass = "fa fa-briefcase",
                        MenuOrder = 1,
                        SiteId = 1,
                        IsExternalLink = false,
                        URL = null,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.NavigationMenu.Add(_navigationMenu);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 15,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }
                #endregion

                #region Operations Modules
                _modules = context.Modules
                                  .Where(x => x.ModuleName == "Operations").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "Operations",
                        Description = "Operations Description",
                        ModuleValue = 2,
                        ControlValue = 0,
                        PermissionValue = 0,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }

                _navigationMenu = context.NavigationMenu
                                 .Where(x => x.Title == "Operations").FirstOrDefault();

                if (_navigationMenu == null)
                {
                    _navigationMenu = new Models.AspNetNavigationMenu()
                    {
                        Title = "Operations",
                        Description = "Operations Description",
                        MenuUUID = context.Modules.Where(x => x.ModuleName == "Operations").FirstOrDefault().Id,
                        ParentId = null,
                        Controller = null,
                        Action = null,
                        IconClass = "fa fa-user-circle-o",
                        MenuOrder = 2,
                        SiteId = 1,
                        IsExternalLink = false,
                        URL = null,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.NavigationMenu.Add(_navigationMenu);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 1,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }

                #region Zone
                //Zone
                _modules = context.Modules
                                  .Where(x => x.ModuleName == "Zone").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "Zone",
                        Description = "Zone Description",
                        ModuleValue = 2,
                        ControlValue = 1,
                        PermissionValue = 15,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }

                _navigationMenu = context.NavigationMenu
                                 .Where(x => x.Title == "Zone").FirstOrDefault();

                if (_navigationMenu == null)
                {
                    _navigationMenu = new Models.AspNetNavigationMenu()
                    {
                        Title = "Zone",
                        Description = "Zone Description",
                        MenuUUID = context.Modules.Where(x => x.ModuleName == "Zone").FirstOrDefault().Id,
                        ParentId = context.NavigationMenu.Where(x => x.Title == "Operations").FirstOrDefault().Id,
                        Controller = "Operations",
                        Action = "ZoneView",
                        IconClass = "fa fa-briefcase",
                        MenuOrder = 1,
                        SiteId = 1,
                        IsExternalLink = false,
                        URL = null,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.NavigationMenu.Add(_navigationMenu);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 15,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }
                #endregion

                #region Groups
                //Groups
                _modules = context.Modules
                                  .Where(x => x.ModuleName == "Groups").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "Groups",
                        Description = "Groups Description",
                        ModuleValue = 2,
                        ControlValue = 2,
                        PermissionValue = 15,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }

                _navigationMenu = context.NavigationMenu
                                 .Where(x => x.Title == "Groups").FirstOrDefault();

                if (_navigationMenu == null)
                {
                    _navigationMenu = new Models.AspNetNavigationMenu()
                    {
                        Title = "Groups",
                        Description = "Groups Description",
                        MenuUUID = context.Modules.Where(x => x.ModuleName == "Groups").FirstOrDefault().Id,
                        ParentId = context.NavigationMenu.Where(x => x.Title == "Operations").FirstOrDefault().Id,
                        Controller = "Operations",
                        Action = "GroupsView",
                        IconClass = "fa fa-briefcase",
                        MenuOrder = 2,
                        SiteId = 1,
                        IsExternalLink = false,
                        URL = null,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.NavigationMenu.Add(_navigationMenu);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 15,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }
                #endregion

                #region Organization
                //Organization
                _modules = context.Modules
                                  .Where(x => x.ModuleName == "Organization").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "Organization",
                        Description = "Organization Description",
                        ModuleValue = 2,
                        ControlValue = 4,
                        PermissionValue = 1,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }

                _navigationMenu = context.NavigationMenu
                                 .Where(x => x.Title == "Organization").FirstOrDefault();

                if (_navigationMenu == null)
                {
                    _navigationMenu = new Models.AspNetNavigationMenu()
                    {
                        Title = "Organization",
                        Description = "Organization Description",
                        MenuUUID = context.Modules.Where(x => x.ModuleName == "Organization").FirstOrDefault().Id,
                        ParentId = context.NavigationMenu.Where(x => x.Title == "Operations").FirstOrDefault().Id,
                        Controller = "Operations",
                        Action = "OrganizationView",
                        IconClass = "fa fa-briefcase",
                        MenuOrder = 3,
                        SiteId = 1,
                        IsExternalLink = false,
                        URL = null,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.NavigationMenu.Add(_navigationMenu);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 1,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }
                #endregion

                #region Roles
                //Roles
                _modules = context.Modules
                                  .Where(x => x.ModuleName == "Roles").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "Roles",
                        Description = "Roles Description",
                        ModuleValue = 2,
                        ControlValue = 8,
                        PermissionValue = 15,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }

                _navigationMenu = context.NavigationMenu
                                 .Where(x => x.Title == "Roles").FirstOrDefault();

                if (_navigationMenu == null)
                {
                    _navigationMenu = new Models.AspNetNavigationMenu()
                    {
                        Title = "Roles",
                        Description = "Roles Description",
                        MenuUUID = context.Modules.Where(x => x.ModuleName == "Roles").FirstOrDefault().Id,
                        ParentId = context.NavigationMenu.Where(x => x.Title == "Operations").FirstOrDefault().Id,
                        Controller = "Operations",
                        Action = "RolesView",
                        IconClass = "fa fa-briefcase",
                        MenuOrder = 4,
                        SiteId = 1,
                        IsExternalLink = false,
                        URL = null,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.NavigationMenu.Add(_navigationMenu);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 15,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }
                #endregion

                #region Users
                //Users
                _modules = context.Modules
                                  .Where(x => x.ModuleName == "Users").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "Users",
                        Description = "Users Description",
                        ModuleValue = 2,
                        ControlValue = 16,
                        PermissionValue = 15,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }

                _navigationMenu = context.NavigationMenu
                                 .Where(x => x.Title == "Users").FirstOrDefault();

                if (_navigationMenu == null)
                {
                    _navigationMenu = new Models.AspNetNavigationMenu()
                    {
                        Title = "Users",
                        Description = "Users Description",
                        MenuUUID = context.Modules.Where(x => x.ModuleName == "Users").FirstOrDefault().Id,
                        ParentId = context.NavigationMenu.Where(x => x.Title == "Operations").FirstOrDefault().Id,
                        Controller = "Operations",
                        Action = "UsersView",
                        IconClass = "fa fa-briefcase",
                        MenuOrder = 5,
                        SiteId = 1,
                        IsExternalLink = false,
                        URL = null,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.NavigationMenu.Add(_navigationMenu);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 15,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }
                #endregion


                #region RolePermissions
                //Users
                _modules = context.Modules
                                  .Where(x => x.ModuleName == "RolePermissions").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "RolePermissions",
                        Description = "RolePermissions Description",
                        ModuleValue = 2,
                        ControlValue = 32,
                        PermissionValue = 16,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }

                //NO NAVIGATION MENU

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 16,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }
                #endregion

                #endregion

                #region MetaData Modules
                _modules = context.Modules
                                  .Where(x => x.ModuleName == "MetaData").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "MetaData",
                        Description = "MetaData Description",
                        ModuleValue = 4,
                        ControlValue = 0,
                        PermissionValue = 0,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }
                _navigationMenu = context.NavigationMenu
                                 .Where(x => x.Title == "MetaData").FirstOrDefault();

                if (_navigationMenu == null)
                {
                    _navigationMenu = new Models.AspNetNavigationMenu()
                    {
                        Title = "MetaData",
                        Description = "MetaData Description",
                        MenuUUID = context.Modules.Where(x => x.ModuleName == "MetaData").FirstOrDefault().Id,
                        ParentId = null,
                        Controller = null,
                        Action = null,
                        IconClass = "fa fa-user-o",
                        MenuOrder = 3,
                        SiteId = 1,
                        IsExternalLink = false,
                        URL = null,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.NavigationMenu.Add(_navigationMenu);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 1,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }

                _modules = context.Modules
                                 .Where(x => x.ModuleName == "Modules").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "Modules",
                        Description = "Modules Description",
                        ModuleValue = 4,
                        ControlValue = 1,
                        PermissionValue = 15,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }
                _navigationMenu = context.NavigationMenu
                                 .Where(x => x.Title == "Modules").FirstOrDefault();

                if (_navigationMenu == null)
                {
                    _navigationMenu = new Models.AspNetNavigationMenu()
                    {
                        Title = "Modules",
                        Description = "Modules Description",
                        MenuUUID = context.Modules.Where(x => x.ModuleName == "Modules").FirstOrDefault().Id,
                        ParentId = context.NavigationMenu.Where(x => x.Title == "MetaData").FirstOrDefault().Id,
                        Controller = "MetaData",
                        Action = "ModulesView",
                        IconClass = "fa fa-briefcase",
                        MenuOrder = 1,
                        SiteId = 1,
                        IsExternalLink = false,
                        URL = null,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.NavigationMenu.Add(_navigationMenu);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 15,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }

                _modules = context.Modules
                                 .Where(x => x.ModuleName == "Menus").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "Menus",
                        Description = "Menus Description",
                        ModuleValue = 4,
                        ControlValue = 2,
                        PermissionValue = 15,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }

                _navigationMenu = context.NavigationMenu
                                 .Where(x => x.Title == "Menus").FirstOrDefault();

                if (_navigationMenu == null)
                {
                    _navigationMenu = new Models.AspNetNavigationMenu()
                    {
                        Title = "Menus",
                        Description = "Menus Description",
                        MenuUUID = context.Modules.Where(x => x.ModuleName == "Menus").FirstOrDefault().Id,
                        ParentId = context.NavigationMenu.Where(x => x.Title == "MetaData").FirstOrDefault().Id,
                        Controller = "MetaData",
                        Action = "MenusView",
                        IconClass = "fa fa-briefcase",
                        MenuOrder = 2,
                        SiteId = 1,
                        IsExternalLink = false,
                        URL = null,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.NavigationMenu.Add(_navigationMenu);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 15,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }
                #endregion

                #region ProjectTask Modules
                _modules = context.Modules
                                  .Where(x => x.ModuleName == "ProjectTask").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "ProjectTask",
                        Description = "ProjectTask Description",
                        ModuleValue = 8,
                        ControlValue = 0,
                        PermissionValue = 0,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }
                _navigationMenu = context.NavigationMenu
                                 .Where(x => x.Title == "Project Task").FirstOrDefault();

                if (_navigationMenu == null)
                {
                    _navigationMenu = new Models.AspNetNavigationMenu()
                    {
                        Title = "Project Task",
                        Description = "ProjectTask Description",
                        MenuUUID = context.Modules.Where(x => x.ModuleName == "ProjectTask").FirstOrDefault().Id,
                        ParentId = null,
                        Controller = null,
                        Action = null,
                        IconClass = "fa fa-user-o",
                        MenuOrder = 4,
                        SiteId = 1,
                        IsExternalLink = false,
                        URL = null,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.NavigationMenu.Add(_navigationMenu);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 1,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }

                _modules = context.Modules
                                 .Where(x => x.ModuleName == "CurrentTask").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "CurrentTask",
                        Description = "CurrentTask Description",
                        ModuleValue = 8,
                        ControlValue = 1,
                        PermissionValue = 15,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }
                _navigationMenu = context.NavigationMenu
                                 .Where(x => x.Title == "Current Task").FirstOrDefault();

                if (_navigationMenu == null)
                {
                    _navigationMenu = new Models.AspNetNavigationMenu()
                    {
                        Title = "Current Task",
                        Description = "CurrentTask Description",
                        MenuUUID = context.Modules.Where(x => x.ModuleName == "CurrentTask").FirstOrDefault().Id,
                        ParentId = context.NavigationMenu.Where(x => x.Title == "Project Task").FirstOrDefault().Id,
                        Controller = "ProjectTask",
                        Action = "CurrentTaskView",
                        IconClass = "fa fa-briefcase",
                        MenuOrder = 1,
                        SiteId = 1,
                        IsExternalLink = false,
                        URL = null,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.NavigationMenu.Add(_navigationMenu);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 15,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }

                _modules = context.Modules
                                 .Where(x => x.ModuleName == "ProjectList").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "ProjectList",
                        Description = "ProjectList Description",
                        ModuleValue = 8,
                        ControlValue = 2,
                        PermissionValue = 15,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }

                _navigationMenu = context.NavigationMenu
                                 .Where(x => x.Title == "ProjectList").FirstOrDefault();

                if (_navigationMenu == null)
                {
                    _navigationMenu = new Models.AspNetNavigationMenu()
                    {
                        Title = "ProjectList",
                        Description = "ProjectList Description",
                        MenuUUID = context.Modules.Where(x => x.ModuleName == "ProjectList").FirstOrDefault().Id,
                        ParentId = context.NavigationMenu.Where(x => x.Title == "ProjectTask").FirstOrDefault().Id,
                        Controller = "MetaData",
                        Action = "ProjectListView",
                        IconClass = "fa fa-briefcase",
                        MenuOrder = 2,
                        SiteId = 1,
                        IsExternalLink = false,
                        URL = null,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.NavigationMenu.Add(_navigationMenu);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 15,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }

                _modules = context.Modules
                                 .Where(x => x.ModuleName == "ChangeTaskStatus").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "ChangeTaskStatus",
                        Description = "ChangeTaskStatus Description",
                        ModuleValue = 8,
                        ControlValue = 4,
                        PermissionValue = 16,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 16,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }

                _modules = context.Modules
                                 .Where(x => x.ModuleName == "GetAllTask").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "GetAllTask",
                        Description = "GetAllTask Description",
                        ModuleValue = 8,
                        ControlValue = 8,
                        PermissionValue = 16,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 16,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }
                #endregion

                #region Timesheet Modules
                _modules = context.Modules
                                  .Where(x => x.ModuleName == "Timesheet").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "Timesheet",
                        Description = "Timesheet Description",
                        ModuleValue = 16,
                        ControlValue = 0,
                        PermissionValue = 0,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }
                _navigationMenu = context.NavigationMenu
                                 .Where(x => x.Title == "Timesheet").FirstOrDefault();

                if (_navigationMenu == null)
                {
                    _navigationMenu = new Models.AspNetNavigationMenu()
                    {
                        Title = "Timesheet",
                        Description = "Timesheet Description",
                        MenuUUID = context.Modules.Where(x => x.ModuleName == "Timesheet").FirstOrDefault().Id,
                        ParentId = null,
                        Controller = null,
                        Action = null,
                        IconClass = "fa fa-user-o",
                        MenuOrder = 5,
                        SiteId = 1,
                        IsExternalLink = false,
                        URL = null,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.NavigationMenu.Add(_navigationMenu);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 1,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }

                _modules = context.Modules
                                 .Where(x => x.ModuleName == "WorkFromHome").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "WorkFromHome",
                        Description = "WorkFromHome Description",
                        ModuleValue = 16,
                        ControlValue = 1,
                        PermissionValue = 15,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }
                _navigationMenu = context.NavigationMenu
                                 .Where(x => x.Title == "Work From Home").FirstOrDefault();

                if (_navigationMenu == null)
                {
                    _navigationMenu = new Models.AspNetNavigationMenu()
                    {
                        Title = "Work From Home",
                        Description = "WorkFromHome Description",
                        MenuUUID = context.Modules.Where(x => x.ModuleName == "WorkFromHome").FirstOrDefault().Id,
                        ParentId = context.NavigationMenu.Where(x => x.Title == "Timesheet").FirstOrDefault().Id,
                        Controller = "Timesheet",
                        Action = "WorkFromHomeView",
                        IconClass = "fa fa-briefcase",
                        MenuOrder = 1,
                        SiteId = 1,
                        IsExternalLink = false,
                        URL = null,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.NavigationMenu.Add(_navigationMenu);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 15,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }
                #endregion

                #region Reports Modules
                _modules = context.Modules
                                  .Where(x => x.ModuleName == "Reports").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "Reports",
                        Description = "Reports Description",
                        ModuleValue = 32,
                        ControlValue = 0,
                        PermissionValue = 0,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }
                _navigationMenu = context.NavigationMenu
                                 .Where(x => x.Title == "Reports").FirstOrDefault();

                if (_navigationMenu == null)
                {
                    _navigationMenu = new Models.AspNetNavigationMenu()
                    {
                        Title = "Reports",
                        Description = "Reports Description",
                        MenuUUID = context.Modules.Where(x => x.ModuleName == "Reports").FirstOrDefault().Id,
                        ParentId = null,
                        Controller = null,
                        Action = null,
                        IconClass = "fa fa-user-o",
                        MenuOrder = 6,
                        SiteId = 1,
                        IsExternalLink = false,
                        URL = null,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.NavigationMenu.Add(_navigationMenu);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 1,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }

                _modules = context.Modules
                                 .Where(x => x.ModuleName == "TimesheetReports").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "TimesheetReports",
                        Description = "TimesheetReports Description",
                        ModuleValue = 32,
                        ControlValue = 1,
                        PermissionValue = 1,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }
                _navigationMenu = context.NavigationMenu
                                 .Where(x => x.Title == "Timesheet Reports").FirstOrDefault();

                if (_navigationMenu == null)
                {
                    _navigationMenu = new Models.AspNetNavigationMenu()
                    {
                        Title = "Timesheet Reports",
                        Description = "TimesheetReports Description",
                        MenuUUID = context.Modules.Where(x => x.ModuleName == "TimesheetReports").FirstOrDefault().Id,
                        ParentId = context.NavigationMenu.Where(x => x.Title == "Reports").FirstOrDefault().Id,
                        Controller = "Reports",
                        Action = "TimesheetReportView",
                        IconClass = "fa fa-briefcase",
                        MenuOrder = 1,
                        SiteId = 1,
                        IsExternalLink = false,
                        URL = null,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.NavigationMenu.Add(_navigationMenu);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 1,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }

                _modules = context.Modules
                                 .Where(x => x.ModuleName == "TaskReports").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "TaskReports",
                        Description = "TaskReports Description",
                        ModuleValue = 32,
                        ControlValue = 2,
                        PermissionValue = 1,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }

                _navigationMenu = context.NavigationMenu
                                 .Where(x => x.Title == "Task Reports").FirstOrDefault();

                if (_navigationMenu == null)
                {
                    _navigationMenu = new Models.AspNetNavigationMenu()
                    {
                        Title = "Task Reports",
                        Description = "TaskReports Description",
                        MenuUUID = context.Modules.Where(x => x.ModuleName == "TaskReports").FirstOrDefault().Id,
                        ParentId = context.NavigationMenu.Where(x => x.Title == "Reports").FirstOrDefault().Id,
                        Controller = "Reports",
                        Action = "TaskReportView",
                        IconClass = "fa fa-briefcase",
                        MenuOrder = 2,
                        SiteId = 1,
                        IsExternalLink = false,
                        URL = null,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.NavigationMenu.Add(_navigationMenu);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 1,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }

                _modules = context.Modules
                                 .Where(x => x.ModuleName == "GetAllTimesheetReports").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "GetAllTimesheetReports",
                        Description = "GetAllTimesheetReports Description",
                        ModuleValue = 32,
                        ControlValue = 4,
                        PermissionValue = 16,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 16,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }
                #endregion

                #region Settings Modules
                _modules = context.Modules
                                  .Where(x => x.ModuleName == "Settings").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "Settings",
                        Description = "Settings Description",
                        ModuleValue = 64,
                        ControlValue = 0,
                        PermissionValue = 0,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }
                _navigationMenu = context.NavigationMenu
                                 .Where(x => x.Title == "Settings").FirstOrDefault();

                if (_navigationMenu == null)
                {
                    _navigationMenu = new Models.AspNetNavigationMenu()
                    {
                        Title = "Settings",
                        Description = "Settings Description",
                        MenuUUID = context.Modules.Where(x => x.ModuleName == "Settings").FirstOrDefault().Id,
                        ParentId = null,
                        Controller = null,
                        Action = null,
                        IconClass = "fa fa-user-o",
                        MenuOrder = 7,
                        SiteId = 1,
                        IsExternalLink = false,
                        URL = null,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.NavigationMenu.Add(_navigationMenu);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 1,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }

                _modules = context.Modules
                                 .Where(x => x.ModuleName == "Notification").FirstOrDefault();

                if (_modules == null)
                {
                    _modules = new Models.AspNetModules()
                    {
                        ModuleName = "Notification",
                        Description = "Notification Description",
                        ModuleValue = 64,
                        ControlValue = 1,
                        PermissionValue = 15,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.Modules.Add(_modules);
                    context.SaveChanges();
                }
                _navigationMenu = context.NavigationMenu
                                 .Where(x => x.Title == "Notification").FirstOrDefault();

                if (_navigationMenu == null)
                {
                    _navigationMenu = new Models.AspNetNavigationMenu()
                    {
                        Title = "Notification",
                        Description = "Notification Description",
                        MenuUUID = context.Modules.Where(x => x.ModuleName == "Notification").FirstOrDefault().Id,
                        ParentId = context.NavigationMenu.Where(x => x.Title == "Settings").FirstOrDefault().Id,
                        Controller = "Settings",
                        Action = "NotificationView",
                        IconClass = "fa fa-briefcase",
                        MenuOrder = 1,
                        SiteId = 1,
                        IsExternalLink = false,
                        URL = null,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.NavigationMenu.Add(_navigationMenu);
                    context.SaveChanges();
                }

                _roleMenuPermission = context.RoleMenuPermission
                                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                if (_roleMenuPermission == null)
                {
                    _roleMenuPermission = new Models.AspNetRoleMenuPermission()
                    {
                        PermissionId = _modules.Id,
                        RoleId = _roleSystemAdminId,
                        PermissionValue = 15,
                        FlagDeleted = false,
                        UserCreated = 0,
                        DateCreated = DateTime.Now,
                        UserAmended = 0,
                        DateAmended = DateTime.Now,
                    };

                    context.RoleMenuPermission.Add(_roleMenuPermission);
                    context.SaveChanges();
                }
                #endregion

                #region Comments
                #region Customers Modules
                //_modules = context.Modules
                //                  .Where(x => x.ModuleName == "Customers").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "Customers",
                //        Description = "Customers Description",
                //        ModuleValue = 4,
                //        ControlValue = 0,
                //        PermissionValue = 0,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                 .Where(x => x.Title == "Customers").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "Customers",
                //        Description = "Customers Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "Customers").FirstOrDefault().Id,
                //        ParentId = null,
                //        Controller = null,
                //        Action = null,
                //        IconClass = "fa fa-user-o",
                //        MenuOrder = 1,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 0,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}

                //_modules = context.Modules
                //                 .Where(x => x.ModuleName == "Expatriate").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "Expatriate",
                //        Description = "Expatriate Description",
                //        ModuleValue = 4,
                //        ControlValue = 1,
                //        PermissionValue = 15,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                 .Where(x => x.Title == "Expatriate").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "Expatriate",
                //        Description = "Expatriate Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "Expatriate").FirstOrDefault().Id,
                //        ParentId = context.NavigationMenu.Where(x => x.Title == "Customers").FirstOrDefault().Id,
                //        Controller = "Customers",
                //        Action = "Expatriate",
                //        IconClass = "fa fa-briefcase",
                //        MenuOrder = 1,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 15,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}

                //_modules = context.Modules
                //                 .Where(x => x.ModuleName == "Assigned Expatriate").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "Assigned Expatriate",
                //        Description = "Assigned Expatriate Description",
                //        ModuleValue = 4,
                //        ControlValue = 2,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}

                //_navigationMenu = context.NavigationMenu
                //                 .Where(x => x.Title == "Assigned Expatriate").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "Assigned Expatriate",
                //        Description = "Assigned Expatriate Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "Assigned Expatriate").FirstOrDefault().Id,
                //        ParentId = context.NavigationMenu.Where(x => x.Title == "Customers").FirstOrDefault().Id,
                //        Controller = "Customers",
                //        Action = "AssignedExpatriate",
                //        IconClass = "fa fa-briefcase",
                //        MenuOrder = 2,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}
                #endregion

                #region Medical Modules
                //_modules = context.Modules
                //                  .Where(x => x.ModuleName == "Medical").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "Medical",
                //        Description = "Medical Description",
                //        ModuleValue = 8,
                //        ControlValue = 0,
                //        PermissionValue = 0,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}

                //_navigationMenu = context.NavigationMenu
                //                 .Where(x => x.Title == "Medical").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "Medical",
                //        Description = "Medical Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "Medical").FirstOrDefault().Id,
                //        ParentId = null,
                //        Controller = null,
                //        Action = null,
                //        IconClass = "fa fa-medkit",
                //        MenuOrder = 4,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 0,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}

                //_modules = context.Modules
                //                 .Where(x => x.ModuleName == "Examined").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "Examined",
                //        Description = "Examined Description",
                //        ModuleValue = 8,
                //        ControlValue = 1,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                 .Where(x => x.Title == "Examined").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "Examined",
                //        Description = "Examined Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "Examined").FirstOrDefault().Id,
                //        ParentId = context.NavigationMenu.Where(x => x.Title == "Medical").FirstOrDefault().Id,
                //        Controller = "Medical",
                //        Action = "Examined",
                //        IconClass = "fa fa-briefcase",
                //        MenuOrder = 1,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}

                //_modules = context.Modules
                //                 .Where(x => x.ModuleName == "Search").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "Search",
                //        Description = "Search Description",
                //        ModuleValue = 8,
                //        ControlValue = 2,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                 .Where(x => x.Title == "Search").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "Search",
                //        Description = "Search Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "Search").FirstOrDefault().Id,
                //        ParentId = context.NavigationMenu.Where(x => x.Title == "Medical").FirstOrDefault().Id,
                //        Controller = "Medical",
                //        Action = "Search",
                //        IconClass = "fa fa-briefcase",
                //        MenuOrder = 2,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}
                #endregion

                #region Certification Modules
                //_modules = context.Modules
                //                  .Where(x => x.ModuleName == "Certification").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "Certification",
                //        Description = "Certification Description",
                //        ModuleValue = 16,
                //        ControlValue = 0,
                //        PermissionValue = 0,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                 .Where(x => x.Title == "Certification").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "Certification",
                //        Description = "Certification Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "Certification").FirstOrDefault().Id,
                //        ParentId = null,
                //        Controller = null,
                //        Action = null,
                //        IconClass = "fa fa-certificate",
                //        MenuOrder = 5,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 0,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}

                //_modules = context.Modules
                //                 .Where(x => x.ModuleName == "Medical Exam").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "Medical Exam",
                //        Description = "Medical Exam Description",
                //        ModuleValue = 16,
                //        ControlValue = 1,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                 .Where(x => x.Title == "Medical Exam").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "Medical Exam",
                //        Description = "Medical Exam Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "Medical Exam").FirstOrDefault().Id,
                //        ParentId = context.NavigationMenu.Where(x => x.Title == "Certification").FirstOrDefault().Id,
                //        Controller = "Certification",
                //        Action = "MedicalExam",
                //        IconClass = "fa fa-briefcase",
                //        MenuOrder = 1,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}

                //_modules = context.Modules
                //                 .Where(x => x.ModuleName == "Previous Certification").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "Previous Certification",
                //        Description = "Previous Certification Description",
                //        ModuleValue = 16,
                //        ControlValue = 2,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                 .Where(x => x.Title == "Previous Certification").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "Previous Certification",
                //        Description = "Previous Certification Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "Previous Certification").FirstOrDefault().Id,
                //        ParentId = context.NavigationMenu.Where(x => x.Title == "Certification").FirstOrDefault().Id,
                //        Controller = "Certification",
                //        Action = "PreviousCertification",
                //        IconClass = "fa fa-briefcase",
                //        MenuOrder = 2,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}

                //_modules = context.Modules
                //                 .Where(x => x.ModuleName == "Pending Certification").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "Pending Certification",
                //        Description = "Pending Certification Description",
                //        ModuleValue = 16,
                //        ControlValue = 4,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                 .Where(x => x.Title == "Pending Certification").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "Pending Certification",
                //        Description = "Pending Certification Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "Pending Certification").FirstOrDefault().Id,
                //        ParentId = context.NavigationMenu.Where(x => x.Title == "Certification").FirstOrDefault().Id,
                //        Controller = "Certification",
                //        Action = "PendingCertification",
                //        IconClass = "fa fa-briefcase",
                //        MenuOrder = 3,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}

                //_modules = context.Modules
                //                 .Where(x => x.ModuleName == "Delayed Certification").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "Delayed Certification",
                //        Description = "Delayed Certification Description",
                //        ModuleValue = 16,
                //        ControlValue = 8,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                 .Where(x => x.Title == "Delayed Certification").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "Delayed Certification",
                //        Description = "Delayed Certification Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "Delayed Certification").FirstOrDefault().Id,
                //        ParentId = context.NavigationMenu.Where(x => x.Title == "Certification").FirstOrDefault().Id,
                //        Controller = "Certification",
                //        Action = "DelayedCertification",
                //        IconClass = "fa fa-briefcase",
                //        MenuOrder = 4,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}

                //_modules = context.Modules
                //                 .Where(x => x.ModuleName == "Search Certification").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "Search Certification",
                //        Description = "Search Certification Description",
                //        ModuleValue = 16,
                //        ControlValue = 16,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                 .Where(x => x.Title == "Search Certification").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "Search Certification",
                //        Description = "Search Certification Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "Search Certification").FirstOrDefault().Id,
                //        ParentId = context.NavigationMenu.Where(x => x.Title == "Certification").FirstOrDefault().Id,
                //        Controller = "Certification",
                //        Action = "SearchCertification",
                //        IconClass = "fa fa-briefcase",
                //        MenuOrder = 5,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}
                #endregion

                #region Appeal Modules
                //_modules = context.Modules
                //                  .Where(x => x.ModuleName == "Appeal").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "Appeal",
                //        Description = "Appeal Description",
                //        ModuleValue = 32,
                //        ControlValue = 0,
                //        PermissionValue = 0,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                 .Where(x => x.Title == "Appeal").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "Appeal",
                //        Description = "Appeal Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "Appeal").FirstOrDefault().Id,
                //        ParentId = null,
                //        Controller = null,
                //        Action = null,
                //        IconClass = "fa fa-stethoscope",
                //        MenuOrder = 6,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 0,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}

                //_modules = context.Modules
                //                 .Where(x => x.ModuleName == "Review").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "Review",
                //        Description = "Review Appeal Description",
                //        ModuleValue = 32,
                //        ControlValue = 1,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                .Where(x => x.Title == "Review").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "Review",
                //        Description = "Review Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "Review").FirstOrDefault().Id,
                //        ParentId = context.NavigationMenu.Where(x => x.Title == "Appeal").FirstOrDefault().Id,
                //        Controller = "Appeal",
                //        Action = "Review",
                //        IconClass = "fa fa-briefcase",
                //        MenuOrder = 1,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}
                #endregion

                #region X-Ray Modules
                //_modules = context.Modules
                //                  .Where(x => x.ModuleName == "X-Ray").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "X-Ray",
                //        Description = "X-Ray Description",
                //        ModuleValue = 64,
                //        ControlValue = 0,
                //        PermissionValue = 0,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                 .Where(x => x.Title == "X-Ray").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "X-Ray",
                //        Description = "X-Ray Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "X-Ray").FirstOrDefault().Id,
                //        ParentId = null,
                //        Controller = null,
                //        Action = null,
                //        IconClass = "fa fa-eraser",
                //        MenuOrder = 7,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 0,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}

                //_modules = context.Modules
                //                 .Where(x => x.ModuleName == "X-Ray Review").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "X-Ray Review",
                //        Description = "Review X-Ray Description",
                //        ModuleValue = 64,
                //        ControlValue = 1,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                .Where(x => x.Title == "X-Ray Review").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "X-Ray Review",
                //        Description = "X-Ray Review Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "X-Ray Review").FirstOrDefault().Id,
                //        ParentId = context.NavigationMenu.Where(x => x.Title == "X-Ray").FirstOrDefault().Id,
                //        Controller = "XRay ",
                //        Action = "XRayReview",
                //        IconClass = "fa fa-briefcase",
                //        MenuOrder = 1,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}
                #endregion

                #region Assignment Modules
                //_modules = context.Modules
                //                  .Where(x => x.ModuleName == "Assignment").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "Assignment",
                //        Description = "Assignment Description",
                //        ModuleValue = 128,
                //        ControlValue = 0,
                //        PermissionValue = 0,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                 .Where(x => x.Title == "Assignment").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "Assignment",
                //        Description = "Assignment Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "Assignment").FirstOrDefault().Id,
                //        ParentId = null,
                //        Controller = null,
                //        Action = null,
                //        IconClass = "fa fa-hospital-o",
                //        MenuOrder = 8,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 0,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}

                //_modules = context.Modules
                //                 .Where(x => x.ModuleName == "UnassignedList").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "UnassignedList",
                //        Description = "UnassignedList Description",
                //        ModuleValue = 128,
                //        ControlValue = 1,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                .Where(x => x.Title == "UnassignedList").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "UnassignedList",
                //        Description = "UnassignedList Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "UnassignedList").FirstOrDefault().Id,
                //        ParentId = context.NavigationMenu.Where(x => x.Title == "Assignment").FirstOrDefault().Id,
                //        Controller = "Assignment ",
                //        Action = "UnassignedList",
                //        IconClass = "fa fa-briefcase",
                //        MenuOrder = 1,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}

                //_modules = context.Modules
                //                 .Where(x => x.ModuleName == "AssignedList").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "AssignedList",
                //        Description = "AssignedList Description",
                //        ModuleValue = 128,
                //        ControlValue = 2,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                .Where(x => x.Title == "AssignedList").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "AssignedList",
                //        Description = "AssignedList Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "AssignedList").FirstOrDefault().Id,
                //        ParentId = context.NavigationMenu.Where(x => x.Title == "Assignment").FirstOrDefault().Id,
                //        Controller = "Assignment ",
                //        Action = "AssignedList",
                //        IconClass = "fa fa-briefcase",
                //        MenuOrder = 2,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}
                //_modules = context.Modules
                //                 .Where(x => x.ModuleName == "ReportedList").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "ReportedList",
                //        Description = "ReportedList Description",
                //        ModuleValue = 128,
                //        ControlValue = 4,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                .Where(x => x.Title == "ReportedList").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "ReportedList",
                //        Description = "ReportedList Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "ReportedList").FirstOrDefault().Id,
                //        ParentId = context.NavigationMenu.Where(x => x.Title == "Assignment").FirstOrDefault().Id,
                //        Controller = "Assignment ",
                //        Action = "ReportedList",
                //        IconClass = "fa fa-briefcase",
                //        MenuOrder = 3,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}
                //_modules = context.Modules
                //                 .Where(x => x.ModuleName == "Submitted Case").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "Submitted Case",
                //        Description = "Submitted Case Description",
                //        ModuleValue = 128,
                //        ControlValue = 8,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                .Where(x => x.Title == "Submitted Case").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "Submitted Case",
                //        Description = "Submitted Case Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "Submitted Case").FirstOrDefault().Id,
                //        ParentId = context.NavigationMenu.Where(x => x.Title == "Assignment").FirstOrDefault().Id,
                //        Controller = "Assignment ",
                //        Action = "SubmittedCase",
                //        IconClass = "fa fa-briefcase",
                //        MenuOrder = 4,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}
                #endregion

                #region Reports Modules
                //_modules = context.Modules
                //                  .Where(x => x.ModuleName == "Reports").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "Reports",
                //        Description = "Reports Description",
                //        ModuleValue = 256,
                //        ControlValue = 0,
                //        PermissionValue = 0,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                 .Where(x => x.Title == "Reports").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "Reports",
                //        Description = "Reports Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "Reports").FirstOrDefault().Id,
                //        ParentId = null,
                //        Controller = null,
                //        Action = null,
                //        IconClass = "fa fa-bar-chart",
                //        MenuOrder = 9,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 0,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}

                //_modules = context.Modules
                //                 .Where(x => x.ModuleName == "Summary Report").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "Summary Report",
                //        Description = "Summary Report Description",
                //        ModuleValue = 256,
                //        ControlValue = 1,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                .Where(x => x.Title == "Summary Report").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "Summary Report",
                //        Description = "Summary Report Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "Summary Report").FirstOrDefault().Id,
                //        ParentId = context.NavigationMenu.Where(x => x.Title == "Reports").FirstOrDefault().Id,
                //        Controller = "Reports ",
                //        Action = "SummaryReport",
                //        IconClass = "fa fa-briefcase",
                //        MenuOrder = 1,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}
                //_modules = context.Modules
                //                 .Where(x => x.ModuleName == "Reported Cases").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "Reported Cases",
                //        Description = "Reported Cases Description",
                //        ModuleValue = 256,
                //        ControlValue = 2,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                .Where(x => x.Title == "Reported Cases").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "Reported Cases",
                //        Description = "Reported Cases Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "Reported Cases").FirstOrDefault().Id,
                //        ParentId = context.NavigationMenu.Where(x => x.Title == "Reports").FirstOrDefault().Id,
                //        Controller = "Reports ",
                //        Action = "ReportedCases",
                //        IconClass = "fa fa-briefcase",
                //        MenuOrder = 2,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}
                #endregion

                #region Help Modules
                //_modules = context.Modules
                //                  .Where(x => x.ModuleName == "Help").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "Help",
                //        Description = "Help Description",
                //        ModuleValue = 512,
                //        ControlValue = 0,
                //        PermissionValue = 0,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                 .Where(x => x.Title == "Help").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "Help",
                //        Description = "Help Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "Help").FirstOrDefault().Id,
                //        ParentId = null,
                //        Controller = null,
                //        Action = null,
                //        IconClass = "fa fa-bar-chart",
                //        MenuOrder = 10,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 0,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}

                //_modules = context.Modules
                //                 .Where(x => x.ModuleName == "Quick Help").FirstOrDefault();

                //if (_modules == null)
                //{
                //    _modules = new Models.Modules()
                //    {
                //        ModuleName = "Quick Help",
                //        Description = "Quick Help Description",
                //        ModuleValue = 512,
                //        ControlValue = 1,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.Modules.Add(_modules);
                //    context.SaveChanges();
                //}
                //_navigationMenu = context.NavigationMenu
                //                .Where(x => x.Title == "Quick Help").FirstOrDefault();

                //if (_navigationMenu == null)
                //{
                //    _navigationMenu = new Models.NavigationMenu()
                //    {
                //        Title = "Quick Help",
                //        Description = "Quick Help Description",
                //        MenuUUID = context.Modules.Where(x => x.ModuleName == "Quick Help").FirstOrDefault().Id,
                //        ParentId = context.NavigationMenu.Where(x => x.Title == "Help").FirstOrDefault().Id,
                //        Controller = "Help ",
                //        Action = "QuickHelp",
                //        IconClass = "fa fa-briefcase",
                //        MenuOrder = 1,
                //        SiteId = 1,
                //        IsExternalLink = false,
                //        URL = null,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.NavigationMenu.Add(_navigationMenu);
                //    context.SaveChanges();
                //}

                //_roleMenuPermission = context.RoleMenuPermission
                //                 .Where(x => x.PermissionId == _modules.Id && x.RoleId == _roleSystemAdminId).FirstOrDefault();
                //if (_roleMenuPermission == null)
                //{
                //    _roleMenuPermission = new Models.RoleMenuPermission()
                //    {
                //        PermissionId = _modules.Id,
                //        RoleId = _roleSystemAdminId,
                //        PermissionValue = 1,
                //        FlagDeleted = false,
                //        UserCreated = 0,
                //        DateCreated = DateTime.Now,
                //        UserAmended = 0,
                //        DateAmended = DateTime.Now,
                //    };

                //    context.RoleMenuPermission.Add(_roleMenuPermission);
                //    context.SaveChanges();
                //}
                #endregion
                #endregion

                #endregion

                context.SaveChanges();
            }
        }
    }
}
