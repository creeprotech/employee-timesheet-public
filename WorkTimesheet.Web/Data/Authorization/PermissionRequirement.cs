﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.Data.Authorization
{
    public class PermissionRequirement : IAuthorizationRequirement
    {
        public int ModuleValue { get; set; }
        public int ControlValue { get; set; }
        public PermissionType PermissionValue { get; set; }
        public PermissionRequirement(int moduleValue, int controlValue, PermissionType permissionValue)
        {
            ModuleValue = moduleValue;
            ControlValue = controlValue;
            PermissionValue = permissionValue;
        }
    }
}
