﻿using WorkTimesheet.Entities.ValueDataType;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.Data.Authorization
{
    public class PermissionHandler : AuthorizationHandler<PermissionRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionRequirement requirement)
        {
            if (context.User == null)
            {
                return Task.CompletedTask;
            }

            //ModuleValueType moduleValue = (ModuleValueType)Enum.Parse(typeof(ModuleValueType), requirement.ModuleValue.ToString());
            if(!Enum.IsDefined(typeof(ModuleValueType),requirement.ModuleValue))
            {
                return Task.CompletedTask;
            }

            var permissions = context.User.Claims.Where(x => x.Type == "Permission").FirstOrDefault();
            if (permissions == null)
            {
                return Task.CompletedTask;
            }

            RuleClaims ruleClaims = JsonConvert.DeserializeObject<RuleClaims>(permissions.Value);
            if (ruleClaims == null)
            {
                return Task.CompletedTask;
            }
            RulePermission rulePermission = ruleClaims.rulePermissions.Where(r => r.moduleValue == requirement.ModuleValue && r.controlValue == requirement.ControlValue).FirstOrDefault();
            
            if (rulePermission == null)
            {
                return Task.CompletedTask;
            }
            //TODO: verification code if the user has rights to perform operations 
            //if (requirement.PermissionValue.Any())
            //if (((requirement.PermissionValue & rulePermission.permissionValue) == rulePermission.permissionValue))
            if(rulePermission.permissionValue.HasFlag(requirement.PermissionValue))
            {
                context.Succeed(requirement);
            }
            return Task.CompletedTask;
        }
    }
}
