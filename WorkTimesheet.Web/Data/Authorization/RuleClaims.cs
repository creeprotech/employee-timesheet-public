﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.Data.Authorization
{
    public class RuleClaims
    {
        public RulePermission[] rulePermissions { get; set; }
    }

    public class RulePermission
    {
        public string ruleUUID { get; set; }
        public int moduleValue { get; set; }
        public int controlValue { get; set; }
        public PermissionType permissionValue { get; set; }
    }
}
