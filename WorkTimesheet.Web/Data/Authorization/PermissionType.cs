﻿using Microsoft.AspNetCore.Authorization.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.Data.Authorization
{
    [Flags]
    public enum PermissionType : int
    {
        None = 0,
        View = 1,
        Add = 2,
        Update = 4,
        Delete = 8,
        SpecialRights = 16,
        Export = 32
    }
}
