﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.Data.Helpers
{
    public static class BootstrapHelpers
    {
        public static IHtmlContent BootstrapLabelFor<TModel, TProp>(
                this IHtmlHelper<TModel> helper,
                Expression<Func<TModel, TProp>> property,
                int size = 4)
        {
            return helper.LabelFor(property, new
            {
                //@class = "col-sm-" + size + " control-label"
                @class = "control-label"
            });
        }

        //public static IHtmlContent BootstrapLabel(
        //        this IHtmlHelper helper,
        //        string propertyName)
        //{
        //    return helper.Label(propertyName, new
        //    {
        //        @class = "col-sm-4 control-label"
        //    });
        //}
    }
}
