﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.Data.Helpers
{
    public class Period
    {
        [ScaffoldColumn(false)]
        public int PeriodID { get; set; }
        [DisplayName("Period Start"), FAIcon("calendar")]
        public DateTime PeriodStart { get; set; }
        [DisplayName("- Period End"), FAIcon("calendar")]
        public DateTime PeriodEnd { get; set; }
    }
}
