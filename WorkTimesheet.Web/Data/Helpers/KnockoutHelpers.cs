﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.Data.Helpers
{
    public static class KnockoutHelpers
    {
        public static IHtmlContent BeginKnockoutModalForm(this IHtmlHelper helper, string title, string showVariable)
        {
            #region Output
            /*
            <div class="modal fade" tabindex="-1" role="dialog" data-bind="modal:showFinancialInterestDialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">title</h4>
                    </div>
                    <div class="modal-body"> 
            */
            #endregion

            System.Text.StringBuilder html = new System.Text.StringBuilder();
            html.AppendLine("<div class=\"modal fade\" tabindex=\" - 1\" role=\"dialog\" data-bind=\"modal: " + showVariable + "\">");
            html.AppendLine("<div class=\"modal-dialog\" role=\"document\">");
            html.AppendLine("<div class=\"modal-content\">");
            html.AppendLine("<div class=\"modal-header\">");
            html.AppendLine("<h5 class=\"modal-title\">" + title + "</h5>");
            html.AppendLine("<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">&times;</button>");
            html.AppendLine("</div>");
            html.AppendLine("<div class=\"modal-body\">");

            return new HtmlString(html.ToString());
        }

        public static IHtmlContent EndKnockoutModalCRUDForm(this IHtmlHelper helper, string groupName, bool canDelete = true)
        {
            #region Output
            /*
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-bind="click : AddFinancialInterestName">Add Name</button>
                        </div>
                    </div>
                </div>
            </div>
             */
            #endregion

            System.Text.StringBuilder html = new System.Text.StringBuilder();
            html.AppendLine("</div>");
            html.AppendLine("<div class=\"modal-footer\">");

            html.AppendLine("<button type=\"button\" class=\"btn btn-default\" data-bind=\"click: Cancel" + groupName + "\">Cancel</button>");
            if (canDelete)
                html.AppendLine("<button type=\"button\" class=\"btn btn-danger\" data-bind=\"click: Delete" + groupName + ", visible: " + groupName + "ID() != -1\">Delete</button>");

            html.AppendLine("<button type=\"button\" class=\"btn btn-primary\" data-bind=\"click: Update" + groupName + ", visible: " + groupName + "ID() != -1\">Update</button>");
            html.AppendLine("<button type=\"button\" class=\"btn btn-primary\" data-bind=\"click: Add" + groupName + ", visible: " + groupName + "ID() == -1\">Add</button>");
            html.AppendLine("</div>");
            html.AppendLine("</div>");
            html.AppendLine("</div>");
            html.AppendLine("</div>");

            return new HtmlString(html.ToString());
        }

        public static IHtmlContent KnockoutTableBody(this IHtmlHelper helper, Type model, string groupName, bool allowSelect = false, bool includeLineNumbers = false)
        {
            #region Output
            /*
                <tbody data-bind="foreach: DataFamilyMember">
                    <tr>
                        <td data-bind="text: TypeFamilyMemberID"></td>
                        <td data-bind="text: FirstName"></td>
                        <td data-bind="text: MiddleName"></td>
                        <td data-bind="text: MaidenName"></td>
                        <td data-bind="text: LastName"></td>
                        <td data-bind="text: NationalityID"></td>
                        <td data-bind="text: Address"></td>
                        <td data-bind="text: IDNumber"></td>
                        <td data-bind="text: DateOfBirth"></td>
                        <td data-bind="text: Occupation"></td>
                        <td data-bind="text: BusinessName"></td>
                        <td data-bind="text: BusinessAddress"></td>
                        <td data-bind="text: TypeSpouseUnionID"></td>
                    </tr>
                </tbody>
             */
            #endregion
            var typeProperties = model.GetProperties();

            System.Text.StringBuilder html = new System.Text.StringBuilder();
            html.AppendLine("<tbody data-bind=\"foreach: " + groupName + "\">");
            if (allowSelect)
                html.AppendLine("\t<tr data-bind=\"if: GetBooleanFromPossibleKnockout(FlagDeleted) == false, click: $root.Select" + groupName + "\" class='rowSelectable'>");
            else
                html.AppendLine("\t<tr data-bind=\"if: GetBooleanFromPossibleKnockout(FlagDeleted) == false\">");

            if (includeLineNumbers)
            {
                html.AppendLine("\t\t<td data-bind='text: $index() + 1 '></td>");
                // html.AppendLine("\t\t<td data-bind='text: RowIndex($index)'></td>");

            }

            string permissionValue = null;
            for (int i = 0; i < typeProperties.Length; i++)
            {
                var property = typeProperties[i];
                var isScaffold = Attribute.IsDefined(property, typeof(System.ComponentModel.DataAnnotations.ScaffoldColumnAttribute));
                var isKOSelect = Attribute.IsDefined(property, typeof(KnockoutSelectBinding));
                var isToggleFlip = Attribute.IsDefined(property, typeof(ToggleFlipAttribute));
                var isAnimatedCheckbox = Attribute.IsDefined(property, typeof(AnimatedCheckboxAttribute));
                var isAnimatedCheckboxValue = Attribute.IsDefined(property, typeof(AnimatedCheckboxAttributeValue));

                if (!isScaffold)
                {
                    if (isKOSelect)
                    {
                        KnockoutSelectBinding koSelect = (KnockoutSelectBinding)Attribute.GetCustomAttribute(property, typeof(KnockoutSelectBinding));
                        html.AppendLine("\t\t<td data-bind=\"text: $root.ArraySearch('" + koSelect.Source + "','" + koSelect.ValueField + "','" + koSelect.TextField + "'," + property.Name + ")\"></td>");
                    }
                    else if (property.PropertyType == typeof(bool))
                    {
                        if (isToggleFlip)
                        {
                            //<div class="toggle-flip">
                            //    <label>
                            //        <input type="checkbox" name="LaboratoryLaboratoryTest" data-bind="checked: LaboratoryLaboratoryTest"><span class="flip-indicator" data-toggle-on="YES" data-toggle-off="NO"></span>
                            //    </label>
                            //</div>
                            html.AppendLine("\t\t<td><div class=\"toggle-flip\"><label><input type=\"checkbox\" name=\"" + property.Name + "\" data-bind=\"checked: " + property.Name + "\"><span class=\"flip-indicator\" data-toggle-on=\"YES\" data-toggle-off=\"NO\"></span></label></div></td>");
                        }
                        else if (isAnimatedCheckbox)
                        {
                            AnimatedCheckboxAttribute permissionAttribute = (AnimatedCheckboxAttribute)Attribute.GetCustomAttribute(property, typeof(AnimatedCheckboxAttribute));
                            html.AppendLine("\t\t<td><div class=\"animated-checkbox\"><label><input type=\"checkbox\" name=\"" + property.Name + "\" data-bind=\"checked: " + property.Name + ", attrIf: { disabled: true , _if: (((GetValueFromPossibleKnockout(PermissionValue) & " + permissionAttribute.PermissionType + ") == " + permissionAttribute.PermissionType + ") ? false : true)}\"><span class=\"label-text\"></span></label></div></td>");
                        }
                        else
                            html.AppendLine("\t\t<td data-bind=\"YesNo: " + property.Name + "\"></td>");
                    }
                    else if (property.PropertyType == typeof(DateTime))
                    {
                        html.AppendLine("\t\t<td data-bind=\"text: " + property.Name + "\"></td>");
                    }
                    else if (property.PropertyType == typeof(Int32) ||
                             property.PropertyType == typeof(Int16) ||
                             property.PropertyType == typeof(float) ||
                             property.PropertyType == typeof(decimal))
                    {
                        if (!isAnimatedCheckboxValue)
                            html.AppendLine("\t\t<td class='text-right' data-bind=\"text: " + property.Name + "\"></td>");
                        else
                        {
                            permissionValue = property.Name;
                        }
                    }
                    else
                    {
                        html.AppendLine("\t\t<td data-bind=\"text: " + property.Name + "\"></td>");
                    }
                }
            }

            html.AppendLine("\t</tr>");
            html.AppendLine("</tbody>");

            return new HtmlString(html.ToString());
        }

        public static IHtmlContent KnockoutTableHead(this IHtmlHelper helper, Type model, bool includeLineNumbers = false)
        {
            #region Output
            /*
                <thead>
                    <tr>
                        <th>Relationship Type</th>
                        <th>First Name</th>
                        <th>Middle Name</th>
                        <th>Maiden name(if applicable)</th>
                        <th>Surname</th>
                        <th>Nationality</th>
                        <th>Address</th>
                        <th>Id Number</th>
                        <th>Date of Birth</th>
                        <th>Place of Birth</th>
                        <th>Occupation</th>
                        <th>Business Name</th>
                        <th>Business Address</th>
                        <th>Union Type</th>
                    </tr>
                </thead>
             */
            #endregion
            var typeProperties = model.GetProperties();

            System.Text.StringBuilder html = new System.Text.StringBuilder();
            html.AppendLine("<thead>");
            html.AppendLine("\t<tr>");
            if (includeLineNumbers)
            {
                html.AppendLine("\t\t<th>#</th>");
            }
            for (int i = 0; i < typeProperties.Length; i++)
            {
                var property = typeProperties[i];
                var isScaffold = Attribute.IsDefined(property, typeof(System.ComponentModel.DataAnnotations.ScaffoldColumnAttribute));
                var isKOSelect = Attribute.IsDefined(property, typeof(KnockoutSelectBinding));
                var hasDisplayName = Attribute.IsDefined(property, typeof(System.ComponentModel.DisplayNameAttribute));
                string displayName = property.Name;
                var isAnimatedCheckboxValue = Attribute.IsDefined(property, typeof(AnimatedCheckboxAttributeValue));

                if (hasDisplayName)
                {
                    System.ComponentModel.DisplayNameAttribute attr = (System.ComponentModel.DisplayNameAttribute)Attribute.GetCustomAttribute(property, typeof(System.ComponentModel.DisplayNameAttribute));
                    displayName = attr.DisplayName;
                }
                if (!isScaffold)
                {
                    if (isKOSelect)
                    {
                        html.AppendLine("\t\t<th>" + displayName + "</th>");
                    }
                    else if (property.PropertyType == typeof(Int32) ||
                        property.PropertyType == typeof(Int16) ||
                        property.PropertyType == typeof(float) ||
                        property.PropertyType == typeof(decimal))
                    {
                        if (!isAnimatedCheckboxValue)
                            html.AppendLine("\t\t<th class='text-right'>" + displayName + "</th>");
                    }
                    else
                    {
                        html.AppendLine("\t\t<th>" + displayName + "</th>");
                    }
                }
            }

            html.AppendLine("\t</tr>");
            html.AppendLine("</thead>");

            return new HtmlString(html.ToString());
        }

        public static IHtmlContent KnockoutCRUDModalLogic(this IHtmlHelper helper, Type model, string groupName)
        {
            var typeProperties = model.GetProperties();

            System.Text.StringBuilder html = new System.Text.StringBuilder();

            html.AppendLine("this." + groupName + "ID = ko.observable(-1);");
            html.AppendLine("this.show" + groupName + "Dialog = ko.observable(false);");
            html.AppendLine("self.ErrorMessage = ko.observable(\"\");");

            #region Observables
            for (int i = 0; i < typeProperties.Length; i++)
            {
                var property = typeProperties[i];
                var isScaffold = Attribute.IsDefined(property, typeof(System.ComponentModel.DataAnnotations.ScaffoldColumnAttribute));
                if (isScaffold)
                    continue;

                //if (property.PropertyType == typeof(Address))
                //{
                //    html.AppendLine("self." + groupName + property.Name + " = ko.mapping.fromJS({ Address1: '', Address2: '', SuburbID: '', NameSuburb: '', CityID: '', NameCity: '', ProvinceID: '', NameProvince: '', PostalCode: '' });");
                //    html.AppendLine("self." + groupName + property.Name + ".SuburbID.subscribe(function (suburbID) {self.UpdateSuburbInfo(self." + groupName + property.Name + ");}); ");
                //}
                //else 
                if (Attribute.IsDefined(property, typeof(DefaultValueAttribute)))
                {
                    var attribute = (DefaultValueAttribute)Attribute.GetCustomAttribute(property, typeof(DefaultValueAttribute));
                    html.AppendLine("self." + groupName + property.Name + " = ko.observable(\"" + attribute.Value + "\")");
                    html.AppendLine(";");
                }
                else
                {
                    html.AppendLine("self." + groupName + property.Name + " = ko.observable(\"\")");
                    html.AppendLine(";");
                }
            }
            #endregion

            #region Cancel
            html.AppendLine("this.Cancel" + groupName + " = function () {");
            for (int i = 0; i < typeProperties.Length; i++)
            {
                var property = typeProperties[i];
                var isScaffold = Attribute.IsDefined(property, typeof(System.ComponentModel.DataAnnotations.ScaffoldColumnAttribute));
                if (!isScaffold)
                {
                    if (Attribute.IsDefined(property, typeof(ComputedAttribute)))
                        continue;

                    //if (property.PropertyType == typeof(Address))
                    //{
                    //    html.AppendLine("\t\tself." + groupName + property.Name + ".Address1('');");
                    //    html.AppendLine("\t\tself." + groupName + property.Name + ".Address2('');");
                    //    html.AppendLine("\t\tself." + groupName + property.Name + ".SuburbID('');");
                    //    html.AppendLine("\t\tself." + groupName + property.Name + ".NameSuburb('');");
                    //    html.AppendLine("\t\tself." + groupName + property.Name + ".CityID('');");
                    //    html.AppendLine("\t\tself." + groupName + property.Name + ".NameCity('');");
                    //    html.AppendLine("\t\tself." + groupName + property.Name + ".ProvinceID('');");
                    //    html.AppendLine("\t\tself." + groupName + property.Name + ".NameProvince('');");
                    //    html.AppendLine("\t\tself." + groupName + property.Name + ".PostalCode('');");
                    //}
                    //else 
                    if (Attribute.IsDefined(property, typeof(DefaultValueAttribute)))
                    {
                        var attribute = (DefaultValueAttribute)Attribute.GetCustomAttribute(property, typeof(DefaultValueAttribute));
                        html.AppendLine("\t\tself." + groupName + property.Name + "('" + attribute.Value + "');");
                    }
                    else
                    {
                        html.AppendLine("\t\tself." + groupName + property.Name + "('');");
                    }
                }
            }

            html.AppendLine("\tself.ErrorMessage('');");
            html.AppendLine("\tself." + groupName + "ID(-1);");
            html.AppendLine("\tself.selected" + groupName + " = null;");
            html.AppendLine("\tself.show" + groupName + "Dialog(false);");
            html.AppendLine("};");
            #endregion            

            #region Select
            html.AppendLine("this.Select" + groupName + " = function (item) {");
            html.AppendLine("\tself." + groupName + "ID(GetValueFromPossibleKnockout(item.ID));");

            for (int i = 0; i < typeProperties.Length; i++)
            {
                var property = typeProperties[i];
                var isScaffold = Attribute.IsDefined(property, typeof(System.ComponentModel.DataAnnotations.ScaffoldColumnAttribute));
                if (!isScaffold)
                {
                    if (Attribute.IsDefined(property, typeof(ComputedAttribute)) == true)
                    {
                        continue; // Don't overwrite computations
                    }
                    //else if (property.PropertyType == typeof(Address))
                    //{
                    //    //html.AppendLine("\t\tconsole.log(item." + property.Name + ");");
                    //    //s2id_LA2PastEmploymentsAddressEmployer_SuburbID
                    //    //$('#LA2ResidencesAddress_SuburbID').select2('data', {text: 'Glen Hurd'});
                    //    html.AppendLine("\t\t$('#" + groupName + property.Name + "_SuburbID').select2('data', {text: item." + property.Name + ".NameSuburb() + ', ' + item." + property.Name + ".NameCity() + ', ' + item." + property.Name + ".NameProvince()});");
                    //    html.AppendLine("\t\tconsole.log($('#s2id_" + groupName + property.Name + "_SuburbID > span.select2-chosen'));");
                    //    html.AppendLine("\t\tself." + groupName + property.Name + ".Address1(GetValueFromPossibleKnockout(item." + property.Name + ".Address1));");
                    //    html.AppendLine("\t\tself." + groupName + property.Name + ".Address2(GetValueFromPossibleKnockout(item." + property.Name + ".Address2));");
                    //    html.AppendLine("\t\tself." + groupName + property.Name + ".SuburbID(GetValueFromPossibleKnockout(item." + property.Name + ".SuburbID));");
                    //    html.AppendLine("\t\tself." + groupName + property.Name + ".NameSuburb(GetValueFromPossibleKnockout(item." + property.Name + ".NameSuburb));");
                    //    html.AppendLine("\t\tself." + groupName + property.Name + ".CityID(GetValueFromPossibleKnockout(item." + property.Name + ".CityID));");
                    //    html.AppendLine("\t\tself." + groupName + property.Name + ".NameCity(GetValueFromPossibleKnockout(item." + property.Name + ".NameCity));");
                    //    html.AppendLine("\t\tself." + groupName + property.Name + ".ProvinceID(GetValueFromPossibleKnockout(item." + property.Name + ".ProvinceID));");
                    //    html.AppendLine("\t\tself." + groupName + property.Name + ".NameProvince(GetValueFromPossibleKnockout(item." + property.Name + ".NameProvince));");
                    //    html.AppendLine("\t\tself." + groupName + property.Name + ".PostalCode(GetValueFromPossibleKnockout(item." + property.Name + ".PostalCode));");
                    //}
                    else
                    {
                        html.AppendLine("\t\tself." + groupName + property.Name + "(GetValueFromPossibleKnockout(item." + property.Name + "));");
                    }
                }
            }

            html.AppendLine("\tself.selected" + groupName + " = item;");
            html.AppendLine("\tself.show" + groupName + "Dialog(true);");
            html.AppendLine("};");
            #endregion

            #region Update
            html.AppendLine("this.Update" + groupName + " = function () {");

            for (int i = 0; i < typeProperties.Length; i++)
            {
                var property = typeProperties[i];
                var isScaffold = Attribute.IsDefined(property, typeof(System.ComponentModel.DataAnnotations.ScaffoldColumnAttribute));
                if (!isScaffold)
                {
                    //if (property.PropertyType == typeof(Address))
                    //{
                    //    html.AppendLine("\t\t\tself.selected" + groupName + "." + property.Name + ".Address1(self." + groupName + property.Name + ".Address1())");
                    //    html.AppendLine("\t\t\tself.selected" + groupName + "." + property.Name + ".Address2(self." + groupName + property.Name + ".Address2())");
                    //    html.AppendLine("\t\t\tself.selected" + groupName + "." + property.Name + ".SuburbID(self." + groupName + property.Name + ".SuburbID())");
                    //    html.AppendLine("\t\t\tself.selected" + groupName + "." + property.Name + ".NameSuburb(self." + groupName + property.Name + ".NameSuburb())");
                    //    html.AppendLine("\t\t\tself.selected" + groupName + "." + property.Name + ".CityID(self." + groupName + property.Name + ".CityID())");
                    //    html.AppendLine("\t\t\tself.selected" + groupName + "." + property.Name + ".NameCity(self." + groupName + property.Name + ".NameCity())");
                    //    html.AppendLine("\t\t\tself.selected" + groupName + "." + property.Name + ".ProvinceID(self." + groupName + property.Name + ".ProvinceID())");
                    //    html.AppendLine("\t\t\tself.selected" + groupName + "." + property.Name + ".NameProvince(self." + groupName + property.Name + ".NameProvince())");
                    //    html.AppendLine("\t\t\tself.selected" + groupName + "." + property.Name + ".PostalCode(self." + groupName + property.Name + ".PostalCode())");
                    //}
                    //else
                    {
                        html.AppendLine("\t\t\tself.selected" + groupName + "." + property.Name + "(self." + groupName + property.Name + "())");
                    }
                }
            }
            html.AppendLine("\tself." + groupName + "ID(-1);");
            html.AppendLine("\tself.Cancel" + groupName + "();");
            html.AppendLine("};");
            #endregion

            #region Delete
            html.AppendLine("this.Delete" + groupName + " = function () {");

            html.AppendLine("\t\t\tself.selected" + groupName + ".FlagDeleted(true);");
            html.AppendLine("\tself." + groupName + "ID(-1);");
            html.AppendLine("\tself.Cancel" + groupName + "();");
            html.AppendLine("};");
            #endregion

            #region Add
            html.AppendLine("this.Add" + groupName + " = function () {");
            html.AppendLine("\tif (self." + groupName + "Errors != null) { if (self." + groupName + "Errors().length > 0) { self." + groupName + "Errors.showAllMessages(); $(\"span.validationMessage:visible\").parent().find(\"input, textarea\").addClass(\"is-invalid\");return;}}");
            html.AppendLine("self.ErrorMessage(' ');");

            //html.AppendLine("var self = this;");
            html.AppendLine("\tself." + groupName + ".push({");
            for (int i = 0; i < typeProperties.Length; i++)
            {
                var property = typeProperties[i];
                var isScaffold = Attribute.IsDefined(property, typeof(System.ComponentModel.DataAnnotations.ScaffoldColumnAttribute));
                if (!isScaffold)
                {
                    //if (property.PropertyType == typeof(Address))
                    //{
                    //    html.AppendLine("\t\t" + property.Name + ": ko.mapping.fromJS({");
                    //    html.AppendLine("\t\t\tAddress1 : new ko.observable(self." + groupName + property.Name + ".Address1()),");
                    //    html.AppendLine("\t\t\tAddress2 : new ko.observable(self." + groupName + property.Name + ".Address2()),");
                    //    html.AppendLine("\t\t\tSuburbID : new ko.observable(self." + groupName + property.Name + ".SuburbID()),");
                    //    html.AppendLine("\t\t\tNameSuburb : new ko.observable(self." + groupName + property.Name + ".NameSuburb()),");
                    //    html.AppendLine("\t\t\tCityID : new ko.observable(self." + groupName + property.Name + ".CityID()),");
                    //    html.AppendLine("\t\t\tNameCity : new ko.observable(self." + groupName + property.Name + ".NameCity()),");
                    //    html.AppendLine("\t\t\tProvinceID : new ko.observable(self." + groupName + property.Name + ".ProvinceID()),");
                    //    html.AppendLine("\t\t\tNameProvince : new ko.observable(self." + groupName + property.Name + ".NameProvince()),");
                    //    html.AppendLine("\t\t\tPostalCode : new ko.observable(self." + groupName + property.Name + ".PostalCode())");
                    //    html.AppendLine("\t\t}),");
                    //}
                    //else
                    {
                        html.AppendLine("\t\t" + property.Name + ": new ko.observable(self." + groupName + property.Name + "()),");
                    }
                }
            }
            html.AppendLine("\t\t" + "ID: 0,");
            html.AppendLine("\t\t" + "FlagDeleted: new ko.observable(false),");
            html.AppendLine("\t});");


            html.AppendLine("\tself." + groupName + "ID(-1);");
            html.AppendLine("\tself.Cancel" + groupName + "();");
            html.AppendLine("}");
            #endregion

            for (int i = 0; i < typeProperties.Length; i++)
            {
                var property = typeProperties[i];
                //if (property.PropertyType == typeof(Address))
                //{
                //    html.AppendLine("self.show" + groupName + property.Name + "Edit = ko.observable(false);");
                //}
            }

            return new HtmlString(html.ToString());
        }

        public static IHtmlContent KnockoutBasicModalView(this IHtmlHelper helper, Type model, string groupName)
        {
            #region Output
            /*
                <fieldset class="form-horizontal">
                <div class="form-group">
                    <label class="col-md-2 control-label" for="ResidenceDateResidence">Date</label>
                    <div class="col-md-10 input-icon-left">
                        <i class="fa fa-calendar"></i><input class="form-control datepicker" id="ResidenceDateResidence" name="ResidenceDateResidence" placeholder="" type="text" value="" data-bind='value: ResidenceDateResidence, valueUpdate: "afterkeydown"'>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="ResidenceAddress">Address</label>
                    <div class="col-md-10 input-icon-left">
                        <i class="fa fa-building"></i><input class="form-control" id="ResidenceAddress" name="ResidenceAddress" placeholder="" type="text" value="" data-bind='value: ResidenceAddress, valueUpdate: "afterkeydown"'>
                    </div>
                </div>
            </fieldset>
             */
            #endregion
            var typeProperties = model.GetProperties();

            System.Text.StringBuilder html = new System.Text.StringBuilder();

            html.AppendLine("<fieldset class=\"form-horizontal\">");

            for (int i = 0; i < typeProperties.Length; i++)
            {
                var property = typeProperties[i];
                html.AppendLine(KnockoutFieldFor(helper, groupName, property).ToString());
            }

            html.AppendLine("</fieldset>");

            return new HtmlString(html.ToString());
        }

        public static IHtmlContent KnockoutFieldFor(IHtmlHelper helper, string groupName, System.Reflection.PropertyInfo property)
        {
            System.Text.StringBuilder html = new System.Text.StringBuilder();
            var isScaffold = Attribute.IsDefined(property, typeof(System.ComponentModel.DataAnnotations.ScaffoldColumnAttribute));

            var hasDisplayName = Attribute.IsDefined(property, typeof(System.ComponentModel.DisplayNameAttribute));
            string displayName = property.Name;
            if (hasDisplayName)
            {
                System.ComponentModel.DisplayNameAttribute attr = (System.ComponentModel.DisplayNameAttribute)Attribute.GetCustomAttribute(property, typeof(System.ComponentModel.DisplayNameAttribute));
                displayName = attr.DisplayName;
            }

            if (!isScaffold)
            {
                string formgroupBindings = string.Empty;
                if (Attribute.IsDefined(property, typeof(KnockoutVisibleCondition)))
                {
                    KnockoutVisibleCondition attr = (KnockoutVisibleCondition)Attribute.GetCustomAttribute(property, typeof(KnockoutVisibleCondition));
                    formgroupBindings = "data-bind=\"visible: " + attr.Condition + "\"";
                }
                formgroupBindings = formgroupBindings.Replace("[GroupName]", groupName);

                html.AppendLine("\t<div class=\"form-group\"" + formgroupBindings + ">");
                html.AppendLine("\t\t<label class=\"col-sm-4 control-label\" for=\"" + groupName + property.Name + "\">" + displayName + "</label>");

                html.AppendLine("\t\t<div class=\"col-sm-7 input-icon-left\">");
                html.AppendLine(KnockoutControl(helper, property, groupName).ToString());

                html.AppendLine("\t\t</div>");

                html.AppendLine("\t</div>");
            }
            return new HtmlString(html.ToString());
        }

        public static IHtmlContent KnockoutModalConfig(this IHtmlHelper helper)
        {
            return new HtmlString(@"ko.bindingHandlers.modal = {
                init: function (element, valueAccessor) {
                    $(element).modal({
                        show: false
                    });

                    var value = valueAccessor();
                    if (ko.isObservable(value)) {
                        $(element).on('hidden.bs.modal', function () {
                            value(false);
                        });
                    }
                },
                update: function (element, valueAccessor) {
                    var value = valueAccessor();
                    if (ko.utils.unwrapObservable(value)) {
                        $(element).modal('show');
                    } else {
                        $(element).modal('hide');
                    }
                }
            }");
        }

        public static IHtmlContent KnockoutModelAdjustments(this IHtmlHelper helper, Type model)
        {
            var typeProperties = model.GetProperties();
            //var metaData = ModelMetadataProviders
            //.Current
            //.GetMetadataForType(null, model);
            System.Text.StringBuilder html = new System.Text.StringBuilder();

            for (int i = 0; i < typeProperties.Length; i++)
            {
                var property = typeProperties[i];
                if (property.PropertyType == typeof(DateTime) || property.PropertyType == typeof(Nullable<DateTime>))
                {
                    //html.AppendLine("self." + property.Name + "(new Date(parseInt(self." + property.Name + "().substr(6))).toISOString().substring(0, 10));");

                    html.AppendLine("if (self." + property.Name + "() != '' && self." + property.Name + "() != null){");
                    html.AppendLine("\tvar date" + property.Name + " = new Date(parseInt(self." + property.Name + "().substr(6)));");
                    html.AppendLine("\tself." + property.Name + "(date" + property.Name + ".getFullYear() + '/' + ('00' + (date" + property.Name + ".getMonth() + 1)).slice(-2) + '/' + ('00' + date" + property.Name + ".getDate()).slice(-2));");
                    html.AppendLine("}");
                    html.AppendLine("if (self." + property.Name + "() == '1/01/01') {");
                    html.AppendLine("\tself." + property.Name + "('');");
                    html.AppendLine("}");
                }
                //else if (property.PropertyType == typeof(TextArea))
                //{
                //    html.AppendLine("self." + property.Name + " = new ko.observable(self." + property.Name + ".Value());");
                //}
                //else if (property.PropertyType == typeof(Address))
                //{
                //    html.AppendLine("self." + property.Name + ".SuburbID.subscribe(function (suburbID) {self.UpdateSuburbInfo(self." + property.Name + ");}); ");
                //}
                else if (property.PropertyType == typeof(bool))
                {   // force a string
                    html.AppendLine("self." + property.Name + " = ko.observable('' + self." + property.Name + "());");
                }

                bool isCollection = typeof(System.Collections.IEnumerable).IsAssignableFrom(property.PropertyType);
                if (isCollection && property.PropertyType != typeof(String))
                {
                    if (property.PropertyType.GetGenericArguments().Length == 0)
                        continue;

                    var tProperties = property.PropertyType.GetGenericArguments()[0].GetProperties();
                    bool needsAdjustments = false;
                    for (int j = 0; j < tProperties.Length; j++)
                    {
                        var tProperty = tProperties[j];
                        if (tProperty.PropertyType == typeof(DateTime))
                        {
                            needsAdjustments = true;
                        }
                    }

                    if (needsAdjustments)
                    {
                        html.AppendLine("self." + property.Name + "().forEach(function (item) {");
                        for (int j = 0; j < tProperties.Length; j++)
                        {
                            var tProperty = tProperties[j];
                            if (tProperty.PropertyType == typeof(DateTime))
                            {
                                html.AppendLine("\tif (item." + tProperty.Name + "() != '' && item." + tProperty.Name + "() != null){");
                                html.AppendLine("\t\tvar date" + tProperty.Name + " = new Date(parseInt(item." + tProperty.Name + "().substr(6)));");
                                html.AppendLine("\t\titem." + tProperty.Name + "(date" + tProperty.Name + ".getFullYear() + '/' + ('00' + (date" + tProperty.Name + ".getMonth() + 1)).slice(-2) + '/' + ('00' + date" + tProperty.Name + ".getDate()).slice(-2));");
                                html.AppendLine("\t}");
                                html.AppendLine("\tif (item." + tProperty.Name + "() == '1/01/01') {");
                                html.AppendLine("\t\titem." + tProperty.Name + "('');");
                                html.AppendLine("\t}");
                            }
                        }
                        html.AppendLine("});");
                    }
                }
            }

            return new HtmlString(html.ToString());
        }

        public static IHtmlContent KnockoutModelValidations(this IHtmlHelper helper, Type model, bool childModel = false, string groupname = "")
        {
            var typeProperties = model.GetProperties();

            System.Text.StringBuilder html = new System.Text.StringBuilder();
            if (!childModel)
            {
                //RSA ID Number validator
                html.AppendLine("ko.validation.rules['RSAID'] = {validator: function(val) {if ( val != null && val.length != 0) {return ValidateRSAID(val);} else { return true; }},message: 'This is not a valid RSA ID number.'}; ");

                //RSA ID Number validator
                html.AppendLine("ko.validation.rules['PassportOrRSAID'] = {validator: function(val) { if ( val != null && val.length == 13 && isNumber(val)) { return ValidateRSAID(val); } else { return true; }},message: 'This is not a valid RSA ID number.'}; ");

                // Decimal places validator/limiter
                //console.log('decimalLimit(val: ' + val + ', limit: ' + limit + ', count:' + countDecimals(val) + ')');
                html.AppendLine("ko.validation.rules['decimalLimit'] = {validator: function(val, limit) { return countDecimals(val) <= limit;},message: 'Value must be within {0} decimal places.'}; ");

                // Integer validator, includes negative numbers and nullable (Required tag should allow for non-null)
                html.AppendLine("ko.validation.rules['integer'] = {validator: function(val, validate) {return val === null || typeof val === 'undefined' || val === '' || (validate && /^-?\\d*$/.test(val.toString()));},message: 'Please provide a valid number.'}; ");

                // Register validation extenders
                html.AppendLine("ko.validation.registerExtenders();");
            }

            System.Text.StringBuilder groupItems = new System.Text.StringBuilder();
            for (int i = 0; i < typeProperties.Length; i++)
            {
                var property = typeProperties[i];

                if (Attribute.IsDefined(property, typeof(System.ComponentModel.DataAnnotations.ScaffoldColumnAttribute)))
                {
                    continue;
                }

                //if (property.PropertyType == typeof(FileModel))
                //{
                //    continue;
                //}
                //if (property.PropertyType == typeof(KOFileModel))
                //{
                //    continue;
                //}
                //if (property.PropertyType == typeof(DropzoneContents))
                //{
                //    continue;
                //}
                //if (property.PropertyType == typeof(Address))
                //{
                //    if (Attribute.IsDefined(property, typeof(RequiredAttribute)))
                //    {
                //        html.Append("self." + groupname + property.Name + ".Address1.extend({ ");
                //        html.Append("required: true, ");
                //        html.AppendLine("trackChange: false })");

                //        html.Append("self." + groupname + property.Name + ".SuburbID.extend({ ");
                //        html.Append("required: true, ");
                //        html.AppendLine("trackChange: false })");
                //    }

                //    groupItems.Append("self." + groupname + property.Name + ".Address1,");
                //    groupItems.Append("self." + groupname + property.Name + ".SuburbID,");
                //    continue;
                //}

                //if (property.PropertyType == typeof(KOFileUploadModel))
                //{
                //    continue;
                //}

                var isEmail = Attribute.IsDefined(property, typeof(EmailAddressAttribute));

                //self.SectionA.extend({ required: true, trackChange: true })
                html.Append("self." + groupname + property.Name + ".extend({ ");
                if (Attribute.IsDefined(property, typeof(RequiredAttribute)))
                {
                    html.Append("required: true, ");
                    //  html.Append("required: { message: '"+ property.Name + "required.' }, ");
                }
                if (Attribute.IsDefined(property, typeof(MinLengthAttribute)))
                {
                    MinLengthAttribute attribute = (MinLengthAttribute)Attribute.GetCustomAttribute(property, typeof(MinLengthAttribute));
                    html.Append("minLength: " + attribute.Length + ", ");
                }
                if (Attribute.IsDefined(property, typeof(MaxLengthAttribute)))
                {
                    MaxLengthAttribute attribute = (MaxLengthAttribute)Attribute.GetCustomAttribute(property, typeof(MaxLengthAttribute));
                    html.Append("maxLength: " + attribute.Length + ", ");
                }
                if (Attribute.IsDefined(property, typeof(EmailAddressAttribute)))
                {
                    MaxLengthAttribute attribute = (MaxLengthAttribute)Attribute.GetCustomAttribute(property, typeof(MaxLengthAttribute));
                    html.Append("email: true, ");
                }

                if (Attribute.IsDefined(property, typeof(ReadOnlyAttribute)))
                {
                    ReadOnlyAttribute attribute = (ReadOnlyAttribute)Attribute.GetCustomAttribute(property, typeof(ReadOnlyAttribute));
                    html.Append("enable: false, ");
                }

                if (Attribute.IsDefined(property, typeof(IDNumberAttribute)))
                {
                    IDNumberAttribute attribute = (IDNumberAttribute)Attribute.GetCustomAttribute(property, typeof(IDNumberAttribute));
                    html.Append("RSAID: true, digit: true, ");
                }

                if (Attribute.IsDefined(property, typeof(AlphaNoNumbersAttribute)))
                {
                    html.Append("pattern: {message: 'Numbers are not allowed',params: '^([^0-9]*)$'}, ");
                }

                if (property.PropertyType == typeof(DateTime))
                {
                    html.Append("dateISO: true, ");
                }

                if (Attribute.IsDefined(property, typeof(PhoneNumberAttribute)))
                {
                    PhoneNumberAttribute attribute = (PhoneNumberAttribute)Attribute.GetCustomAttribute(property, typeof(PhoneNumberAttribute));
                    html.Append("maxLength: 11, minLength: 10, digit: true, ");
                }

                if (property.PropertyType == typeof(int) || property.PropertyType == typeof(short) || Attribute.IsDefined(property, typeof(NumbersOnlyAttribute)))
                {
                    html.Append("integer: true, ");
                }

                if (property.PropertyType == typeof(decimal) || property.PropertyType == typeof(decimal?))
                {
                    html.Append("number: true, ");

                    if (Attribute.IsDefined(property, typeof(DecimalLimitAttribute)))
                    {
                        DecimalLimitAttribute attribute = (DecimalLimitAttribute)Attribute.GetCustomAttribute(property, typeof(DecimalLimitAttribute));
                        html.Append("decimalLimit: " + attribute.NumberOfPlaces.ToString() + ", ");
                    }
                }

                if (Attribute.IsDefined(property, typeof(PassportNoAttribute)))
                {
                    PassportNoAttribute attribute = (PassportNoAttribute)Attribute.GetCustomAttribute(property, typeof(PassportNoAttribute));
                    html.Append("PassportNo: true, ");
                }

                if (Attribute.IsDefined(property, typeof(PassportOrIDNumberAttribute)))
                {
                    html.Append("PassportOrRSAID: true, minLength: 7, maxLength: 13, ");
                }

                html.AppendLine("trackChange: true })");
                //Place after track change -- field types
                if (property.PropertyType == typeof(decimal))
                {
                    html.Append(@".extend({ validation: [{
                    validator: function (value) {
                        return /^-?\d*(?:\.\d*)?$/.test(value.toString());
                    },
                    message: function () {
                        return 'Must be a valid Decimal';
                    },
                }] 
                })");
                }
                if (property.PropertyType == typeof(bool?))
                {
                    html.Append(@".extend({ validation: [{
                    validator: function (value) {
                        return value == true || value == false;
                    },
                    message: function () {
                        return 'Must be boolean';
                    },
                    }]})"
                );
                }

                //if(property.PropertyType == typeof(IdentityNumber))
                //{

                //}
                html.AppendLine(";");

                groupItems.Append("self." + groupname + property.Name + ",");
            }

            //html.AppendLine("self.errors = ko.validation.group(self); ");
            groupItems.Remove(groupItems.Length - 1, 1);
            if (!childModel)
                html.AppendLine("self.errors = ko.validation.group([" + groupItems.ToString() + "], {deep: true,observable: false}); ");
            else
                html.AppendLine("self." + groupname + "Errors = ko.validation.group([" + groupItems.ToString() + "], {deep: true,observable: false}); ");

            return new HtmlString(html.ToString());
        }

        public static IHtmlContent KnockoutControlFor<TModel, TProp>(
                this IHtmlHelper<TModel> helper,
                Expression<Func<TModel, TProp>> property,
                string groupName = "")
        {
            MemberExpression member = property.Body as MemberExpression;
            if (member == null)
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a method, not a property.",
                    property.ToString()));

            System.Reflection.PropertyInfo propInfo = member.Member as System.Reflection.PropertyInfo;
            return KnockoutControl(helper, propInfo, groupName);
        }

        public static IHtmlContent KnockoutControl(IHtmlHelper helper, System.Reflection.PropertyInfo property, string groupName)
        {
            System.Text.StringBuilder html = new System.Text.StringBuilder();
            var isToggleFlip = Attribute.IsDefined(property, typeof(ToggleFlipAttribute));
            var isAnimatedCheckbox = Attribute.IsDefined(property, typeof(AnimatedCheckboxAttribute));
            var isKOSelect = Attribute.IsDefined(property, typeof(KnockoutSelectBinding));
            var isKOMultiSelect = Attribute.IsDefined(property, typeof(KnockoutMultiSelectBinding));
            var isKOCheckBoxList = Attribute.IsDefined(property, typeof(KnockoutCheckBoxListBinding));
            var hasIcon = Attribute.IsDefined(property, typeof(FAIconAttribute));
            var isTextArea = Attribute.IsDefined(property, typeof(TextAreaField));
            var isPeriod = property.PropertyType == typeof(Period);
            var isReadOnly = Attribute.IsDefined(property, typeof(ReadOnly));
            var isIDNumber = Attribute.IsDefined(property, typeof(IDNumberAttribute));
            var isPassportOrIDNumber = Attribute.IsDefined(property, typeof(PassportOrIDNumberAttribute));
            var isPhoneNumber = Attribute.IsDefined(property, typeof(PhoneNumberAttribute));
            var isPassportNo = Attribute.IsDefined(property, typeof(PassportNoAttribute));
            var isDateTime = false;

            var extraClasses = "";
            if (property.PropertyType == typeof(DateTime) || property.PropertyType == typeof(DateTime?))
            {
                isDateTime = true;
                //extraClasses += " datepicker";
            }
            if (hasIcon && !isTextArea)
            {
                FAIconAttribute faIcon = (FAIconAttribute)Attribute.GetCustomAttribute(property, typeof(FAIconAttribute));
                html.AppendLine("\t\t<i class=\"fa fa-" + faIcon.FAIcon + "\"></i>");
            }
            else if (isKOSelect)
            {
                //html.AppendLine("\t\t<i class=\"fa fa-list-ul\" ></i>");
            }
            string extraAttributes = "";
            if (Attribute.IsDefined(property, typeof(MaxLengthAttribute)))
            {
                MaxLengthAttribute attribute = (MaxLengthAttribute)Attribute.GetCustomAttribute(property, typeof(MaxLengthAttribute));
                extraAttributes += " maxlength=\"" + attribute.Length + "\"";
            }

            if (isReadOnly)
            {
                if (property.PropertyType == typeof(bool))
                {
                    html.AppendLine(KnockoutBoolRadioButtonFor(helper, property, groupName, true).ToString());
                }
                else
                {
                    html.AppendLine("\t\t<label class=\"form-control readonly" + extraClasses + "\" data-bind=\"text : " + groupName + property.Name + "\"></label>");
                }
            }
            else
            {
                if (isKOSelect)
                {
                    KnockoutSelectBinding koSelect = (KnockoutSelectBinding)Attribute.GetCustomAttribute(property, typeof(KnockoutSelectBinding));
                    string optionCaption = string.Empty;

                    if (koSelect.IncludeDefault)
                    {
                        optionCaption = ", optionsCaption: '" + koSelect.DefaultOptionCaption + "'";
                    }

                    html.AppendLine("\t\t<select name=" + groupName + property.Name + " data-bind=\"options: " + koSelect.Source + optionCaption + ", optionsText: '" + koSelect.TextField + "', optionsValue: '" + koSelect.ValueField + "', value: " + groupName + property.Name + "\" class=\"form-control\"></select>");
                }
                else if (isKOCheckBoxList)
                {
                    KnockoutCheckBoxListBinding koCheckBoxList = (KnockoutCheckBoxListBinding)Attribute.GetCustomAttribute(property, typeof(KnockoutCheckBoxListBinding));
                    html.AppendLine("\t\t<div data-bind='foreach: " + koCheckBoxList.Source + ", validationOptions: {insertMessages: false}'>");
                    //html.AppendLine("\t\t\t<label class=\"radio radio-inline\">");
                    html.AppendLine("\t\t\t\t<input type=\"radio\" class=\"\" name=\"" + groupName + property.Name + "\" for=\"" + groupName + property.Name + "\" data-bind='checked: $parent." + groupName + property.Name + ", value: " + koCheckBoxList.ValueField + "'><span data-bind=\"text: " + koCheckBoxList.TextField + "\">");
                    //html.AppendLine("\t\t\t</label>");
                    html.AppendLine("\t\t</div>");

                    /*
                    <td data-bind="foreach: optionValues">
                        <label><input type="radio" data-bind="checked: $root.radioSelectedOptionValue, value: Value" /><span data-bind="text: Text"></span></label>
                    </td>
                     */

                    html.Replace("<label", "<label data-bind=\"validationOptions: { insertMessages: false}\"");
                    html.AppendLine("<span class=\"validationMessage\" data-bind=\"validationMessage: " + groupName + property.Name + "\" style=\"display:none;\">Test</span>");
                    //html.AppendLine("\t\t<select name=" + groupName + property.Name + " data-bind=\"options: " + koSelect.Source + optionCaption + ", optionsText: '" + koSelect.TextField + "', optionsValue: '" + koSelect.ValueField + "', value: " + groupName + property.Name + "\" class=\"form-control\"></select>");
                }
                else if (isKOMultiSelect)
                {
                    KnockoutMultiSelectBinding koMultiSelect = (KnockoutMultiSelectBinding)Attribute.GetCustomAttribute(property, typeof(KnockoutMultiSelectBinding));
                    string optionCaption = string.Empty;

                    if (koMultiSelect.IncludeDefault)
                    {
                        optionCaption = ", optionsCaption: '" + koMultiSelect.DefaultOptionCaption + "'";
                    }

                    html.AppendLine("\t\t<select name=" + groupName + property.Name + " data-bind=\"selectedOptions: " + groupName + property.Name + ", options: " + koMultiSelect.Source + ", valueAllowUnset: true, optionsText: '" + koMultiSelect.TextField + "', optionsValue: '" + koMultiSelect.ValueField + "', multiselect2: { placeholder: '', allowClear: true, multiple: 'multiple' } \" class=\"form-control\" multiple></select>");
                }
                else if (property.PropertyType == typeof(bool))
                {
                    if (isAnimatedCheckbox)
                    {
                        html.AppendLine("\t\t<td><div class=\"animated-checkbox\"><label><input type=\"checkbox\" name=\"" + groupName + property.Name + "\" data-bind=\"checked: " + groupName + property.Name + "\"><span class=\"label-text\"></span></label></div></td>");
                    }
                    else
                    {
                        html.AppendLine(KnockoutBoolRadioButtonFor(helper, property, groupName).ToString());
                        html.Replace("<label", "<label data-bind=\"validationOptions: { insertMessages: false}\"");
                        html.AppendLine("<span class=\"validationMessage\" data-bind=\"validationMessage: " + groupName + property.Name + "\" style=\"display:none;\">Test</span>");
                    }
                }
                else if (isTextArea)
                {
                    // fetch row count from Attribute
                    //var row_count = ((Infrastructure.TextArea)Attribute.GetCustomAttribute(property, typeof(Infrastructure.TextArea))).NumberOfRows;
                    var row_count = 4;
                    html.AppendLine("\t\t<textarea rows='" + row_count + "' class=\"form-control" + extraClasses + "\" id=\"" + groupName + property.Name + "\" name=\"" + groupName + property.Name + "\" type=\"text\" value=\"\" data-bind='value: " + groupName + property.Name + ", valueUpdate: \"afterkeydown\"' " + extraAttributes + "></textarea>");
                }
                else if (isPeriod)
                {
                    html.AppendLine(helper.Editor("edit" + property.Name, "Period").ToString());
                }
                else if (isIDNumber)
                {
                    html.AppendLine("\t\t<input class=\"form-control" + extraClasses + "\" id=\"" + groupName + property.Name + "\" name=\"" + groupName + property.Name + "\" type=\"text\" value=\"\" data-bind='value: " + groupName + property.Name + ", valueUpdate: \"afterkeydown\"' maxlength='13'>");
                }
                else if (isPassportOrIDNumber)
                {
                    html.AppendLine("\t\t<input class=\"form-control" + extraClasses + "\" id=\"" + groupName + property.Name + "\" name=\"" + groupName + property.Name + "\" type=\"text\" value=\"\" data-bind='value: " + groupName + property.Name + ", valueUpdate: \"afterkeydown\"' maxlength='13'>");
                }
                else if (isPhoneNumber)
                {
                    html.AppendLine("\t\t<input class=\"form-control" + extraClasses + "\" id=\"" + groupName + property.Name + "\" name=\"" + groupName + property.Name + "\" maxlength=\"10\" type=\"text\" value=\"\" data-bind='value: " + groupName + property.Name + ", valueUpdate: \"afterkeydown\"' maxlength='11'>");
                }
                else if (isPassportNo)
                {
                    html.AppendLine("\t\t<input class=\"form-control" + extraClasses + "\" id=\"" + groupName + property.Name + "\" name=\"" + groupName + property.Name + "\" type=\"text\" value=\"\" data-bind='value: " + groupName + property.Name + ", valueUpdate: \"afterkeydown\"' maxlength='20'>");
                }
                else if (isDateTime)
                {
                    html.AppendLine("\t\t<input class=\"form-control" + extraClasses + "\" id=\"" + groupName + property.Name + "\" name=\"" + groupName + property.Name + "\" type=\"text\" value=\"\" data-bind='bootstrapDP: " + groupName + property.Name + ", value: " + groupName + property.Name + ", datepickerOptions: { format: \"yyyy/mm/dd\", autoclose: true} , valueUpdate: \"afterkeydown\"' " + extraAttributes + ">");
                }
                else if (isToggleFlip)
                {
                    //<div class="toggle-flip">
                    //    <label>
                    //        <input type="checkbox" name="LaboratoryLaboratoryTest" data-bind="checked: LaboratoryLaboratoryTest"><span class="flip-indicator" data-toggle-on="YES" data-toggle-off="NO"></span>
                    //    </label>
                    //</div>
                    html.AppendLine("\t\t<td><div class=\"toggle-flip\"><label><input type=\"checkbox\" name=\"" + property.Name + "\" data-bind=\"bscomponent: " + property.Name + "\"><span class=\"flip-indicator\" data-toggle-on=\"YES\" data-toggle-off=\"NO\"></span></label></div></td>");
                }
                else
                {
                    html.AppendLine("\t\t<input class=\"form-control" + extraClasses + "\" id=\"" + groupName + property.Name + "\" name=\"" + groupName + property.Name + "\" type=\"text\" value=\"\" data-bind='value: " + groupName + property.Name + ", valueUpdate: \"afterkeydown\"' " + extraAttributes + ">");
                }
            }

            return new HtmlString(html.ToString());
        }

        public static IHtmlContent KnockoutBoolRadioButtonFor<TModel, TProp>(
                this IHtmlHelper<TModel> helper,
                Expression<Func<TModel, TProp>> property,
                string groupName = "")
        {
            return KnockoutBoolRadioButtonFor(helper, property, groupName);
        }

        public static IHtmlContent KnockoutBoolRadioButtonFor(
                this IHtmlHelper helper,
                System.Reflection.PropertyInfo property,
                string groupName = "",
                bool disabled = false)
        {
            string elementProperties = string.Empty;
            if (disabled)
                elementProperties += " disabled='disabled'";

            //System.Text.StringBuilder html = new System.Text.StringBuilder();
            ////html.AppendLine("\t\t<label class=\"radio radio-inline\">");
            //html.AppendLine("\t\t\t<input type=\"radio\"  name=\"" + groupName + property.Name + "\"  value=\"true\" for=\"" + groupName + property.Name + "\" data-bind='checked: " + groupName + property.Name + "' " + elementProperties + "><span>Yes</span>");
            ////html.AppendLine("\t\t</label>");
            ////html.AppendLine("\t\t<label class=\"radio radio-inline\">");
            //html.AppendLine("\t\t\t<input type=\"radio\"  name=\"" + groupName + property.Name + "\"  value=\"false\" for=\"" + groupName + property.Name + "\" data-bind='checked: " + groupName + property.Name + "' " + elementProperties + "><span>No</span>");
            ////html.AppendLine("\t\t</label>");

            System.Text.StringBuilder html = new System.Text.StringBuilder();
            html.AppendLine("<div class=\"form-check\">");
            html.AppendLine("<label class=\"form-check-label\">");
            html.AppendLine("\t<input type=\"radio\" class=\"form-check-input\" name=\"" + groupName + property.Name + "\"  value=\"true\" for=\"" + groupName + property.Name + "\" data-bind='checked: " + groupName + property.Name + "' " + elementProperties + "><span>Yes</span>");
            html.AppendLine("</label>");
            html.AppendLine("</div>");
            html.AppendLine("<div class=\"form-check\">");
            html.AppendLine("<label class=\"form-check-label\">");
            html.AppendLine("\t<input type=\"radio\" class=\"form-check-input\" name=\"" + groupName + property.Name + "\"  value=\"false\" for=\"" + groupName + property.Name + "\" data-bind='checked: " + groupName + property.Name + "' " + elementProperties + "><span>No</span>");
            html.AppendLine("</label>");
            html.AppendLine("</div>");

            return new HtmlString(html.ToString());
        }

        public static IHtmlContent KnockoutBooleanToYesNo(this IHtmlHelper helper)
        {
            return new HtmlString(@"ko.bindingHandlers.YesNo = {
                update: function (element, valueAccessor) {
                    // defaults to false
                    var val = '' + ko.utils.unwrapObservable(valueAccessor()) || false;
                    if (val == 'true')
                        $(element).text('Yes');
                    else
                        $(element).text('No');
                    }
                }");
        }
    }
}
