﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.Data.Helpers
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class FAIconAttribute : Attribute
    {
        public string FAIcon { get; set; }
        public FAIconAttribute(string FAIcon)
        {
            this.FAIcon = FAIcon;
        }
    }

    public class KnockoutSelectBinding : Attribute
    {
        public string Source { get; set; }
        public string TextField { get; set; }
        public string ValueField { get; set; }
        public bool IncludeDefault { get; set; }
        public string DefaultOptionCaption { get; set; }

        public KnockoutSelectBinding(string Source, string TextField, string ValueField, bool includeDefault = false, string defaultOptionCaption = "Please choose an option")
        {
            this.Source = Source;
            this.TextField = TextField;
            this.ValueField = ValueField;
            this.IncludeDefault = includeDefault;
            this.DefaultOptionCaption = defaultOptionCaption;
        }
    }

    public class KnockoutMultiSelectBinding : Attribute
    {
        public string Source { get; set; }
        public string TextField { get; set; }
        public string ValueField { get; set; }
        public bool IncludeDefault { get; set; }
        public string DefaultOptionCaption { get; set; }

        public KnockoutMultiSelectBinding(string Source, string TextField, string ValueField, bool includeDefault = false, string defaultOptionCaption = "Please choose an option")
        {
            this.Source = Source;
            this.TextField = TextField;
            this.ValueField = ValueField;
            this.IncludeDefault = includeDefault;
            this.DefaultOptionCaption = defaultOptionCaption;
        }
    }

    public class KnockoutCheckBoxListBinding : Attribute
    {
        public string Source { get; set; }
        public string TextField { get; set; }
        public string ValueField { get; set; }

        public KnockoutCheckBoxListBinding(string Source, string TextField, string ValueField)
        {
            this.Source = Source;
            this.TextField = TextField;
            this.ValueField = ValueField;
        }
    }

    public class TextAreaField : Attribute
    {
        // Specifies whether a field is textarea or not
    }
    public class ReadOnly : Attribute
    {
        // Specifies Whether a field must not be editable
    }
    public class IDNumberAttribute : Attribute
    {
        // Specifies an IDNumber, so should be a number with length of 13, among other validations
    }

    public class PhoneNumberAttribute : Attribute
    {
        // Specifies an IDNumber, so should be a number with length of 13, among other validations
    }

    public class NumbersOnlyAttribute : Attribute
    {
        // For when a string field requires acceptance of only numbers, eg. and account number that might contain leading zeroes
    }

    public class AlphaNoNumbersAttribute : Attribute
    {
        // Specifies only alpha characters allowed, no numbers
    }

    public class DecimalLimitAttribute : Attribute
    {
        public byte NumberOfPlaces { get; set; }

        public DecimalLimitAttribute(byte numberOfPlaces)
        {
            this.NumberOfPlaces = numberOfPlaces;
        }
    }

    public class PassportNoAttribute : Attribute
    {
        // Specifies an PassportNo, so should be a number with length of 20, among other validations
    }

    public class PassportOrIDNumberAttribute : Attribute
    {
        // Specifies a hybrid validation combining the PassportNo and IDNumber validations
    }

    public class ComputedAttribute : Attribute
    {
        // Specifies a computed column, as sometimes they need to be ignored
    }

    /// <summary>
    /// Currently only for modal controls. Use [GroupName] to indicate where group name must be injected.
    /// </summary>
    public class KnockoutVisibleCondition : Attribute
    {
        public string Condition { get; set; }

        public KnockoutVisibleCondition(string condition)
        {
            this.Condition = condition;
        }
    }

    public class ToggleFlipAttribute : Attribute
    {
        // Specifies a toggle flip, as sometimes they need to be ignored
    }

    public class AnimatedCheckboxAttribute : Attribute
    {
        // Specifies a toggle flip, as sometimes they need to be ignored
        public int PermissionType { get; set; }

        public AnimatedCheckboxAttribute(int permissionType)
        {
            this.PermissionType = permissionType;
        }
    }

    public class AnimatedCheckboxAttributeValue : Attribute
    {
        // Specifies a toggle flip, as sometimes they need to be ignored
    }
}
