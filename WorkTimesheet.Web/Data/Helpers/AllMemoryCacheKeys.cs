﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.Data.Helpers
{
    public static class AllMemoryCacheKeys
    {
        public const string NavigationMenuKey = "Timesheet.NavigationMenuKey";
    }
}
