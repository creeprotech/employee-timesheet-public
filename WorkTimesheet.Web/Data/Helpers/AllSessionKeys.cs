﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.Data.Helpers
{
    public static class AllSessionKeys
    {
        public const string OUId = "Timesheet.OUId";
        public const string OUName = "Timesheet.OUName";
        public const string GroupId = "Timesheet.GroupId";
        public const string GroupName = "Timesheet.GroupName";
        public const string UserId = "Timesheet.UserId";
        public const string UserName = "Timesheet.UserName";
        public const string RoleId = "Timesheet.RoleId";
        public const string RoleName = "Timesheet.RoleName";
        public const string AuthenticationToken = "Timesheet.AuthenticationToken";
    }
}
