﻿using WorkTimesheet.Web.Data.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace WorkTimesheet.Web.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<AspNetOrganizationUnits> OrganizationUnits { get; set; }
        public DbSet<AspNetGroups> Groups { get; set; }
        public DbSet<AspNetUserGroups> UserGroups { get; set; }
        public DbSet<AspNetModules> Modules { get; set; }
        public DbSet<AspNetNavigationMenu> NavigationMenu { get; set; }
        public DbSet<AspNetRoleMenuPermission> RoleMenuPermission { get; set; }
        public DbSet<ListOfValues> ListOfValues { get; set; }
    }
}
