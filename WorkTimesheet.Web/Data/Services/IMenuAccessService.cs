﻿using WorkTimesheet.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.Data.Services
{
    public interface IMenuAccessService
    { 
        Task<List<MenuViewModel>> GetMenuItemsAsync(ClaimsPrincipal principal);
    }
}
