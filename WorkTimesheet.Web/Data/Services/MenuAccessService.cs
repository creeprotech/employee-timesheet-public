﻿using Microsoft.EntityFrameworkCore;
using WorkTimesheet.Web.ViewModels;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WorkTimesheet.Web.Data;
using WorkTimesheet.Web.Data.Helpers;

namespace WorkTimesheet.Web.Data.Services
{
    public class MenuAccessService : IMenuAccessService
    {
        private readonly IMemoryCache _cache;
        private readonly ApplicationDbContext _context;

        public MenuAccessService(ApplicationDbContext context, IMemoryCache cache)
        {
            _cache = cache;
            _context = context;
        }

        public async Task<List<MenuViewModel>> GetMenuItemsAsync(ClaimsPrincipal principal)
        {
            var isAuthenticated = principal.Identity.IsAuthenticated;
            if (!isAuthenticated)
                return new List<MenuViewModel>();

            var roleIds = await GetUserRoleIds(principal);
            var rules = await (from _rules in _context.RoleMenuPermission
                               where roleIds.Contains(_rules.RoleId) //&& _rules.RuleValue > 0
                               select _rules)
                              .Select(m => new RuleViewModel()
                              {
                                  RuleId = m.PermissionId.ToString(),
                                  RuleName = m.Modules.ModuleName,
                                  RuleUUID = m.Modules.Id.ToString(),
                                  RuleValue = m.PermissionValue,
                                  ModuleValue = m.Modules.ModuleValue,
                                  ControlValue = m.Modules.ControlValue,
                                  PermissionValue = m.Modules.PermissionValue,
                              }).Distinct().OrderBy(a => a.ModuleValue).ThenBy(b => b.ControlValue).ToListAsync();

            Dictionary<int, int> dicModules = new Dictionary<int, int>();
            if (rules.Count > 0)
            {
                int allowFeatures = 0;
                int allowedInteractions = 0;
                foreach (RuleViewModel rule in rules)
                {
                    if (rule.ControlValue == 0)
                    {
                        if (!((allowFeatures & rule.ModuleValue) == rule.ModuleValue))
                        {
                            allowFeatures = allowFeatures | rule.ModuleValue;
                            dicModules.Add(rule.ModuleValue, 0);
                        }
                    }
                    else
                    {
                        if (dicModules.ContainsKey(rule.ModuleValue))
                        {
                            allowedInteractions = dicModules[rule.ModuleValue];
                            if (!((allowedInteractions & rule.ControlValue) == rule.ControlValue))
                            {
                                allowedInteractions = allowedInteractions | rule.ControlValue;
                                if (dicModules.ContainsKey(rule.ModuleValue))
                                {
                                    dicModules[rule.ModuleValue] = allowedInteractions;
                                }
                            }
                        }
                    }
                }
            }

            if (_cache.Get(AllMemoryCacheKeys.NavigationMenuKey) == null)
            {
                var result = (from menu in _context.NavigationMenu.AsNoTracking()
                              where menu.FlagDeleted == false
                              select menu).ToList();

                MemoryCacheEntryOptions cacheExpirationOptions = new MemoryCacheEntryOptions
                {
                    AbsoluteExpiration = DateTime.Now.AddDays(7),
                    Priority = CacheItemPriority.Normal
                };

                _cache.Set<List<Models.AspNetNavigationMenu>>(AllMemoryCacheKeys.NavigationMenuKey, result, cacheExpirationOptions);
            }

            var _navigationMenu = _cache.Get(AllMemoryCacheKeys.NavigationMenuKey) as List<Models.AspNetNavigationMenu>;

            List<string> ruleIds = rules.Where(r => r.RuleValue > 0).Select(x => x.RuleUUID).OfType<string>().ToList();
            var menus = await (from menu in _context.NavigationMenu
                               where menu.FlagDeleted == false && ruleIds.Contains(menu.MenuUUID.ToString())
                               select menu)
                              .Select(m => new MenuViewModel()
                              {
                                  Id = m.Id.ToString(),
                                  Title = m.Title,
                                  Description = m.Description,
                                  ParentId = m.ParentId.ToString(),
                                  Controller = m.Controller,
                                  Action = m.Action,
                                  IconClass = m.IconClass,
                                  MenuOrder = m.MenuOrder,
                                  MenuUUID = m.MenuUUID.ToString(),
                                  SiteId = m.SiteId,
                              }).Distinct().ToListAsync();

            return menus;
        }

        private async Task<List<string>> GetUserRoleIds(ClaimsPrincipal ctx)
        {
            var userId = GetUserId(ctx);
            var data = await (from role in _context.UserRoles
                              where role.UserId == userId
                              select role.RoleId).ToListAsync();

            return data;
        }

        private string GetUserId(ClaimsPrincipal user)
        {
            return ((ClaimsIdentity)user.Identity).FindFirst(ClaimTypes.NameIdentifier)?.Value;
        }
    }
}
