﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.Data.Models
{
    [Table("AspNetNavigationMenu")]
    public class AspNetNavigationMenu
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        [ForeignKey("Module")]
        public Guid? MenuUUID { get; set; }

        [ForeignKey("ParentMenu")]
        public Guid? ParentId { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string IconClass { get; set; }
        public int MenuOrder { get; set; }
        public int SiteId { get; set; }
        public bool IsExternalLink { get; set; }
        public string URL { get; set; }
        public bool FlagDeleted { get; set; }
        public int? UserCreated { get; set; }
        public DateTime? DateCreated { get; set; } = DateTime.Now;
        public int? UserAmended { get; set; }
        public DateTime? DateAmended { get; set; }

        public AspNetModules Module { get; set; }
        public AspNetNavigationMenu ParentMenu { get; set; }
    }
}
