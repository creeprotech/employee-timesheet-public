﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.Data.Models
{
    [Table("AspNetModules")]
    public class AspNetModules
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string ModuleName { get; set; }
        public string Description { get; set; }
        public int ModuleValue { get; set; }
        public int ControlValue { get; set; }
        public int PermissionValue { get; set; }
        public bool FlagDeleted { get; set; }
        public int? UserCreated { get; set; }
        public DateTime? DateCreated { get; set; } = DateTime.Now;
        public int? UserAmended { get; set; }
        public DateTime? DateAmended { get; set; }
    }
}
