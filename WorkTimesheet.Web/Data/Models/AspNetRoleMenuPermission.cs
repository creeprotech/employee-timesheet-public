﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.Data.Models
{
    [Table("AspNetRoleMenuPermission")]
    public class AspNetRoleMenuPermission
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("Modules")]
        public Guid? PermissionId { get; set; }
        public int PermissionValue { get; set; }

        [ForeignKey("AspNetRoles")]
        public string RoleId { get; set; }
        public bool FlagDeleted { get; set; }
        public int? UserCreated { get; set; }
        public DateTime? DateCreated { get; set; } = DateTime.Now;
        public int? UserAmended { get; set; }
        public DateTime? DateAmended { get; set; }

        public AspNetModules Modules { get; set; }
    }
}
