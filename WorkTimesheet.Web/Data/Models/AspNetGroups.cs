﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.Data.Models
{
    [Table("AspNetGroups")]
    public class AspNetGroups
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }

        [ForeignKey("ParentGroup")]
        public Guid? ParentGroupId { get; set; }
        public int? GroupType { get; set; }

        [ForeignKey("OrganizationUnit")]
        public Guid? OrganizationUnitId { get; set; }
        public bool FlagDeleted { get; set; }
        public int? UserCreated { get; set; }
        public DateTime? DateCreated { get; set; } = DateTime.Now;
        public int? UserAmended { get; set; }
        public DateTime? DateAmended { get; set; }

        public AspNetOrganizationUnits OrganizationUnit { get; set; }
        public AspNetGroups ParentGroup { get; set; }
    }
}
