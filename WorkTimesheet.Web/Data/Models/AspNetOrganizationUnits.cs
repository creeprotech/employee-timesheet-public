﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTimesheet.Web.Data.Models
{
    [Table("AspNetOrganizationUnits")]
    public class AspNetOrganizationUnits
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string OUName { get; set; }
        public string Description { get; set; }

        [ForeignKey("ParentOrganizationUnits")]
        public Guid? ParentOU { get; set; }
        public bool FlagDeleted { get; set; }
        public int? UserCreated { get; set; }
        public DateTime? DateCreated { get; set; } = DateTime.Now;
        public int? UserAmended { get; set; }
        public DateTime? DateAmended { get; set; }

        public AspNetOrganizationUnits ParentOrganizationUnits { get; set; }
    }
}
