﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using WorkTimesheet.Web.ViewModels;
using WorkTimesheet.Web.Data.Helpers;
using Microsoft.AspNetCore.Http;
using WorkTimesheet.Web.Data;

namespace WorkTimesheet.Web.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class LoginModel : PageModel
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly ILogger<LoginModel> _logger;
        private readonly ApplicationDbContext _context;

        public LoginModel(SignInManager<IdentityUser> signInManager, 
            ILogger<LoginModel> logger,
            UserManager<IdentityUser> userManager,
            ApplicationDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _context = context;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public string ReturnUrl { get; set; }

        [TempData]
        public string ErrorMessage { get; set; }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            public string Email { get; set; }

            [Required]
            [DataType(DataType.Password)]
            public string Password { get; set; }

            [Display(Name = "Remember me?")]
            public bool RememberMe { get; set; }
        }

        public async Task OnGetAsync(string returnUrl = null)
        {
            if (!string.IsNullOrEmpty(ErrorMessage))
            {
                ModelState.AddModelError(string.Empty, ErrorMessage);
            }

            returnUrl = returnUrl ?? Url.Content("~/");

            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();

            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");

            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var result = await _signInManager.PasswordSignInAsync(Input.Email, Input.Password, Input.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    SessionViewModel sessionViewModel = new SessionViewModel();
                    var user = _context.Users.Where(u => u.UserName == Input.Email).FirstOrDefault();
                    if(user != null)
                    {
                        sessionViewModel.UserId = user.Id;
                        sessionViewModel.UserName = Input.Email;

                        var userRole = _context.UserRoles.Where(u => u.UserId == user.Id).FirstOrDefault();
                        sessionViewModel.RoleId = userRole.RoleId;

                        var userGroup = _context.UserGroups.Where(u => u.UserId == user.Id).FirstOrDefault();
                        sessionViewModel.GroupId = userGroup.GroupId;

                        var group = _context.Groups.Where(g => g.Id == Guid.Parse(userGroup.GroupId)).FirstOrDefault();
                        sessionViewModel.OUId = group.OrganizationUnitId.ToString();
                        sessionViewModel.GroupName = group.GroupName;

                        SetApplicationSession(sessionViewModel);
                    }
                    _logger.LogInformation("User logged in.");
                    return LocalRedirect(returnUrl);
                }
                if (result.RequiresTwoFactor)
                {
                    return RedirectToPage("./LoginWith2fa", new { ReturnUrl = returnUrl, RememberMe = Input.RememberMe });
                }
                if (result.IsLockedOut)
                {
                    _logger.LogWarning("User account locked out.");
                    return RedirectToPage("./Lockout");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return Page();
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }

        private void SetApplicationSession(SessionViewModel model)
        {
            if (model.UserId != null)
                HttpContext.Session.SetString(AllSessionKeys.UserId, (model.UserId));

            if (model.UserName != null)
                HttpContext.Session.SetString(AllSessionKeys.UserName, (model.UserName));


            if (model.RoleId != null)
                HttpContext.Session.SetString(AllSessionKeys.RoleId, model.RoleId);

            if(model.RoleName != null)
                HttpContext.Session.SetString(AllSessionKeys.RoleName, (model.RoleName));


            if (model.GroupId != null)
                HttpContext.Session.SetString(AllSessionKeys.GroupId, model.GroupId);

            if (model.GroupName != null)
                HttpContext.Session.SetString(AllSessionKeys.GroupName, (model.GroupName));

            if (model.OUId != null)
                HttpContext.Session.SetString(AllSessionKeys.OUId, model.OUId);

            if (model.OUName != null)
                HttpContext.Session.SetString(AllSessionKeys.OUName, (model.OUName));

            SetAuthenticationCookie();
        }

        private void SetAuthenticationCookie()
        {
            string strAuthToken = Guid.NewGuid().ToString();
            HttpContext.Session.SetString(AllSessionKeys.AuthenticationToken, strAuthToken);
            Response.Cookies.Append(AllSessionKeys.AuthenticationToken, strAuthToken);
        }
    }
}
