﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace WorkTimesheet.CryptoKey
{
    class Program
    {
        static void Main(string[] args)
        {
            var hmac = new HMACSHA256();
            var key = Convert.ToBase64String(hmac.Key);
            Console.WriteLine("Key Generated : " + key);
            File.WriteAllText(DateTime.Now.Ticks + ".txt", key);
            Console.WriteLine("Press ENTER key to exit.");
        }
    }
}
