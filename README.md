# Employee Timesheet 

A fully featured Employee Task and Timesheet Management System having a well designed with ASP.NET Core 3.1, C# & MS SQL Server with Identity Management. Its based on the mini-Onion Architecture.

Manage all your Employee Task and its WFH timesheet.

## Technologies Used:

For Frontend: .NET Core 3.1, ASP.NET Core 3.1, KnockoutJS, BootStrap V4.3, JQuery v3.3

For Backend: C#, Unit Of Work, Dapper v2.0, Repositories & Entities Pattern, ASP.NET Core Identity 3.1, Entity Framework Core 3.1, JWT (ASPNETCore.JwtBearer), ASPNETCore AuthorizationHandler

For Database: MS SQL, Store Procedure

### Pre-requisites:
* [Microsoft Visual Studio](https://visualstudio.microsoft.com/vs/community/)
* [Microsoft SQL Server Express](https://www.microsoft.com/en-us/sql-server/sql-server-editions-express)
* [Microsoft SQL Server Management Studio (SSMS)](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-2017)

### Demo

<a target="_blank" href="https://web.creepro.com">Live Demo</a>

# Screenshot

**Login Screen**
[![Alt-Text](/Screenshot/login-form.PNG)]

**Dashboard**
[![Alt-Text](/Screenshot/dashboard.PNG)]

**Role Permission**
[![Alt-Text](/Screenshot/role-permission.PNG)]

**Timetracker Sheet**
[![Alt-Text](/Screenshot/timetracker-form.PNG)]

**List of Values (LOV Form)**
[![Alt-Text](/Screenshot/list-of-values-view.PNG)]

### Looking for full stack engineering services? 
Visit [our portfolio](https://portfolio.creepro.com/) or simply leave us a message to [dev@creepro.com](mailto:dev@creepro.com). We will be happy to work with you!

### From Developers
Made with :heart: by [Creepro Development team](https://www.creepro.com).
We're always happy to receive your feedback!

## Credits
Without these open source software Timesheet woudn't have been possible:

 - [Vali Admin - A Free Bootstrap 4 Dashboard Template](https://pratikborsadiya.in/vali-admin/)
 
## License
[MIT](http://opensource.org/licenses/MIT)

Copyright (c) 2019-present, Creepro Technologies