﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace WorkTimesheet.Entities.Timesheet
{
    [Serializable()]

    public class TimeTracker
    {
        public const string PROC_NAME_GET = "[dbo].[spGetTimeTracker]";
        public const string PROC_NAME_STORE = "[dbo].[spStoreTimeTracker]";
        public TimeTracker() { }

        public Guid Id { get; set; }
        public string UserId { get; set; }
        public DateTime DateStarted { get; set; }
        public DateTime DateEnded { get; set; }
        public int TotalHours { get; set; }
        public int Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Comments { get; set; }
        public int NetworkUsedBy { get; set; }
        public int EnvironmentUsedBy { get; set; }
        public int ApprovalStatus { get; set; }
        public string ApprovalId { get; set; }
        public string IPAddress { get; set; }
        public string UserAgent { get; set; }
        public bool FlagDeleted { get; set; }
        public int UserAction { get; set; }

        public static DataTable CreateDataTable()
        {
            DataTable table = new DataTable("ttTimeTracker");
            table.Columns.Add("Id", typeof(Guid));
            table.Columns.Add("UserId", typeof(string));
            table.Columns.Add("DateStarted", typeof(DateTime));
            table.Columns.Add("DateEnded", typeof(DateTime));
            table.Columns.Add("TotalHours", typeof(int));
            table.Columns.Add("Status", typeof(int));
            table.Columns.Add("CreatedOn", typeof(DateTime));
            table.Columns.Add("Comments", typeof(string));
            table.Columns.Add("NetworkUsedBy", typeof(int));
            table.Columns.Add("EnvironmentUsedBy", typeof(int));
            table.Columns.Add("ApprovalStatus", typeof(int));
            table.Columns.Add("ApprovalId", typeof(string));
            table.Columns.Add("IPAddress", typeof(string));
            table.Columns.Add("UserAgent", typeof(string));
            table.Columns.Add("FlagDeleted", typeof(bool));
            table.Columns.Add("UserAction", typeof(int));
            return table;
        }

        public DataTable ConvertToDataTable()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["UserId"] = UserId;
            row["DateStarted"] = DateStarted;
            row["DateEnded"] = DateEnded;
            row["TotalHours"] = TotalHours;
            row["Status"] = Status;
            row["CreatedOn"] = CreatedOn;
            row["Comments"] = Comments;
            row["NetworkUsedBy"] = NetworkUsedBy;
            row["EnvironmentUsedBy"] = EnvironmentUsedBy;
            row["ApprovalStatus"] = ApprovalStatus;
            row["ApprovalId"] = ApprovalId;
            row["IPAddress"] = IPAddress;
            row["UserAgent"] = UserAgent;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataRow ConvertToDataRow()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["UserId"] = UserId;
            row["DateStarted"] = DateStarted;
            row["DateEnded"] = DateEnded;
            row["TotalHours"] = TotalHours;
            row["Status"] = Status;
            row["CreatedOn"] = CreatedOn;
            row["Comments"] = Comments;
            row["NetworkUsedBy"] = NetworkUsedBy;
            row["EnvironmentUsedBy"] = EnvironmentUsedBy;
            row["ApprovalStatus"] = ApprovalStatus;
            row["ApprovalId"] = ApprovalId;
            row["IPAddress"] = IPAddress;
            row["UserAgent"] = UserAgent;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            return row;
        }
        public static DataTable ConvertToDataTable(List<TimeTracker> data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            foreach (TimeTracker item in data)
            {
                row = table.NewRow();
                row["Id"] = item.Id;
                row["UserId"] = item.UserId;
                row["DateStarted"] = item.DateStarted;
                row["DateEnded"] = item.DateEnded;
                row["TotalHours"] = item.TotalHours;
                row["Status"] = item.Status;
                row["CreatedOn"] = item.CreatedOn;
                row["Comments"] = item.Comments;
                row["NetworkUsedBy"] = item.NetworkUsedBy;
                row["EnvironmentUsedBy"] = item.EnvironmentUsedBy;
                row["ApprovalStatus"] = item.ApprovalStatus;
                row["ApprovalId"] = item.ApprovalId;
                row["IPAddress"] = item.IPAddress;
                row["UserAgent"] = item.UserAgent;
                row["FlagDeleted"] = item.FlagDeleted;
                row["UserAction"] = item.UserAction;
                table.Rows.Add(row);
            }
            return table;
        }

        public static DataTable ConvertToDataTable(TimeTracker data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["UserId"] = data.UserId;
            row["DateStarted"] = data.DateStarted;
            row["DateEnded"] = data.DateEnded;
            row["TotalHours"] = data.TotalHours;
            row["Status"] = data.Status;
            row["CreatedOn"] = data.CreatedOn;
            row["Comments"] = data.Comments;
            row["NetworkUsedBy"] = data.NetworkUsedBy;
            row["EnvironmentUsedBy"] = data.EnvironmentUsedBy;
            row["ApprovalStatus"] = data.ApprovalStatus;
            row["ApprovalId"] = data.ApprovalId;
            row["IPAddress"] = data.IPAddress;
            row["UserAgent"] = data.UserAgent;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table)
        {
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["UserId"] = UserId;
            row["DateStarted"] = DateStarted;
            row["DateEnded"] = DateEnded;
            row["TotalHours"] = TotalHours;
            row["Status"] = Status;
            row["CreatedOn"] = CreatedOn;
            row["Comments"] = Comments;
            row["NetworkUsedBy"] = NetworkUsedBy;
            row["EnvironmentUsedBy"] = EnvironmentUsedBy;
            row["ApprovalStatus"] = ApprovalStatus;
            row["ApprovalId"] = ApprovalId;
            row["IPAddress"] = IPAddress;
            row["UserAgent"] = UserAgent;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table, TimeTracker data)
        {
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["UserId"] = data.UserId;
            row["DateStarted"] = data.DateStarted;
            row["DateEnded"] = data.DateEnded;
            row["TotalHours"] = data.TotalHours;
            row["Status"] = data.Status;
            row["CreatedOn"] = data.CreatedOn;
            row["Comments"] = data.Comments;
            row["NetworkUsedBy"] = data.NetworkUsedBy;
            row["EnvironmentUsedBy"] = data.EnvironmentUsedBy;
            row["ApprovalStatus"] = data.ApprovalStatus;
            row["ApprovalId"] = data.ApprovalId;
            row["IPAddress"] = data.IPAddress;
            row["UserAgent"] = data.UserAgent;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }
    }
}
