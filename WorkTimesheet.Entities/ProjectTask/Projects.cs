using System;
using System.Data;
using System.Collections.Generic;

namespace WorkTimesheet.Entities.ProjectTask
{
    [Serializable()]

    public class Projects
    {
        public const string PROC_NAME_GET = "[dbo].[spGetProjects]";
        public const string PROC_NAME_STORE = "[dbo].[spStoreProjects]";
        public Projects() { }

        public Guid Id { get; set; }
        public string ProjectCode { get; set; }
        public string ProjectName { get; set; }
        public string Description { get; set; }
        public string NatureofIndustry { get; set; }
        public bool FlagDeleted { get; set; }
        public int UserAction { get; set; }

        public static DataTable CreateDataTable()
        {
            DataTable table = new DataTable("ttProjects");
            table.Columns.Add("Id", typeof(Guid));
            table.Columns.Add("ProjectCode", typeof(string));
            table.Columns.Add("ProjectName", typeof(string));
            table.Columns.Add("Description", typeof(string));
            table.Columns.Add("NatureofIndustry", typeof(string));
            table.Columns.Add("FlagDeleted", typeof(bool));
            table.Columns.Add("UserAction", typeof(int));
            return table;
        }

        public DataTable ConvertToDataTable()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["ProjectCode"] = ProjectCode;
            row["ProjectName"] = ProjectName;
            row["Description"] = Description;
            row["NatureofIndustry"] = NatureofIndustry;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataRow ConvertToDataRow()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["ProjectCode"] = ProjectCode;
            row["ProjectName"] = ProjectName;
            row["Description"] = Description;
            row["NatureofIndustry"] = NatureofIndustry;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            return row;
        }
        public static DataTable ConvertToDataTable(List<Projects> data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            foreach (Projects item in data)
            {
                row = table.NewRow();
                row["Id"] = item.Id;
                row["ProjectCode"] = item.ProjectCode;
                row["ProjectName"] = item.ProjectName;
                row["Description"] = item.Description;
                row["NatureofIndustry"] = item.NatureofIndustry;
                row["FlagDeleted"] = item.FlagDeleted;
                row["UserAction"] = item.UserAction;
                table.Rows.Add(row);
            }
            return table;
        }

        public static DataTable ConvertToDataTable(Projects data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["ProjectCode"] = data.ProjectCode;
            row["ProjectName"] = data.ProjectName;
            row["Description"] = data.Description;
            row["NatureofIndustry"] = data.NatureofIndustry;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table)
        {
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["ProjectCode"] = ProjectCode;
            row["ProjectName"] = ProjectName;
            row["Description"] = Description;
            row["NatureofIndustry"] = NatureofIndustry;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table, Projects data)
        {
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["ProjectCode"] = data.ProjectCode;
            row["ProjectName"] = data.ProjectName;
            row["Description"] = data.Description;
            row["NatureofIndustry"] = data.NatureofIndustry;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }
    }
}
