﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace WorkTimesheet.Entities.ProjectTask
{
    [Serializable()]

    public class ProjectTasks
    {
        public const string PROC_NAME_GET = "[dbo].[spGetProjectTasks]";
        public const string PROC_NAME_STORE = "[dbo].[spStoreProjectTasks]";
        public ProjectTasks() { }

        public Guid Id { get; set; }
        public string UserId { get; set; }
        public Guid ProjectId { get; set; }
        public string TaskCode { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public int DaysToBeCompleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? DateStarted { get; set; }
        public DateTime? DateEnded { get; set; }
        public DateTime? DateCompleted { get; set; }
        public int ApprovalStatus { get; set; }
        public string ApprovalId { get; set; }
        public bool FlagDeleted { get; set; }
        public int UserAction { get; set; }

        public static DataTable CreateDataTable()
        {
            DataTable table = new DataTable("ttProjectTasks");
            table.Columns.Add("Id", typeof(Guid));
            table.Columns.Add("UserId", typeof(string));
            table.Columns.Add("ProjectId", typeof(Guid));
            table.Columns.Add("TaskCode", typeof(string));
            table.Columns.Add("Title", typeof(string));
            table.Columns.Add("Description", typeof(string));
            table.Columns.Add("Status", typeof(int));
            table.Columns.Add("DaysToBeCompleted", typeof(int));
            table.Columns.Add("CreatedOn", typeof(DateTime));
            table.Columns.Add("DateStarted", typeof(DateTime));
            table.Columns.Add("DateEnded", typeof(DateTime));
            table.Columns.Add("DateCompleted", typeof(DateTime));
            table.Columns.Add("ApprovalStatus", typeof(int));
            table.Columns.Add("ApprovalId", typeof(string));
            table.Columns.Add("FlagDeleted", typeof(bool));
            table.Columns.Add("UserAction", typeof(int));
            return table;
        }

        public DataTable ConvertToDataTable()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["UserId"] = UserId;
            row["ProjectId"] = ProjectId;
            row["TaskCode"] = TaskCode;
            row["Title"] = Title;
            row["Description"] = Description;
            row["Status"] = Status;
            row["DaysToBeCompleted"] = DaysToBeCompleted;
            row["CreatedOn"] = CreatedOn;
            if(DateStarted == null)
                row["DateStarted"] = DBNull.Value;
            else
                row["DateStarted"] = DateStarted;
            if (DateEnded == null)
                row["DateEnded"] = DBNull.Value;
            else
                row["DateEnded"] = DateEnded;

            if (DateCompleted == null)
                row["DateCompleted"] = DBNull.Value;
            else
                row["DateCompleted"] = DateCompleted;
            row["ApprovalStatus"] = ApprovalStatus;
            row["ApprovalId"] = ApprovalId;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataRow ConvertToDataRow()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["UserId"] = UserId;
            row["ProjectId"] = ProjectId;
            row["TaskCode"] = TaskCode;
            row["Title"] = Title;
            row["Description"] = Description;
            row["Status"] = Status;
            row["DaysToBeCompleted"] = DaysToBeCompleted;
            row["CreatedOn"] = CreatedOn;
            row["DateStarted"] = DateStarted;
            row["DateEnded"] = DateEnded;
            row["DateCompleted"] = DateCompleted;
            row["ApprovalStatus"] = ApprovalStatus;
            row["ApprovalId"] = ApprovalId;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            return row;
        }
        public static DataTable ConvertToDataTable(List<ProjectTasks> data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            foreach (ProjectTasks item in data)
            {
                row = table.NewRow();
                row["Id"] = item.Id;
                row["UserId"] = item.UserId;
                row["ProjectId"] = item.ProjectId;
                row["TaskCode"] = item.TaskCode;
                row["Title"] = item.Title;
                row["Description"] = item.Description;
                row["Status"] = item.Status;
                row["DaysToBeCompleted"] = item.DaysToBeCompleted;
                row["CreatedOn"] = item.CreatedOn;
                row["DateStarted"] = item.DateStarted;
                row["DateEnded"] = item.DateEnded;
                row["DateCompleted"] = item.DateCompleted;
                row["ApprovalStatus"] = item.ApprovalStatus;
                row["ApprovalId"] = item.ApprovalId;
                row["FlagDeleted"] = item.FlagDeleted;
                row["UserAction"] = item.UserAction;
                table.Rows.Add(row);
            }
            return table;
        }

        public static DataTable ConvertToDataTable(ProjectTasks data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["UserId"] = data.UserId;
            row["ProjectId"] = data.ProjectId;
            row["TaskCode"] = data.TaskCode;
            row["Title"] = data.Title;
            row["Description"] = data.Description;
            row["Status"] = data.Status;
            row["DaysToBeCompleted"] = data.DaysToBeCompleted;
            row["CreatedOn"] = data.CreatedOn;
            row["DateStarted"] = data.DateStarted;
            row["DateEnded"] = data.DateEnded;
            row["DateCompleted"] = data.DateCompleted;
            row["ApprovalStatus"] = data.ApprovalStatus;
            row["ApprovalId"] = data.ApprovalId;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table)
        {
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["UserId"] = UserId;
            row["ProjectId"] = ProjectId;
            row["TaskCode"] = TaskCode;
            row["Title"] = Title;
            row["Description"] = Description;
            row["Status"] = Status;
            row["DaysToBeCompleted"] = DaysToBeCompleted;
            row["CreatedOn"] = CreatedOn;
            row["DateStarted"] = DateStarted;
            row["DateEnded"] = DateEnded;
            row["DateCompleted"] = DateCompleted;
            row["ApprovalStatus"] = ApprovalStatus;
            row["ApprovalId"] = ApprovalId;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table, ProjectTasks data)
        {
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["UserId"] = data.UserId;
            row["ProjectId"] = data.ProjectId;
            row["TaskCode"] = data.TaskCode;
            row["Title"] = data.Title;
            row["Description"] = data.Description;
            row["Status"] = data.Status;
            row["DaysToBeCompleted"] = data.DaysToBeCompleted;
            row["CreatedOn"] = data.CreatedOn;
            row["DateStarted"] = data.DateStarted;
            row["DateEnded"] = data.DateEnded;
            row["DateCompleted"] = data.DateCompleted;
            row["ApprovalStatus"] = data.ApprovalStatus;
            row["ApprovalId"] = data.ApprovalId;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }
    }
}
