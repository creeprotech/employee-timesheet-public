using System;
using System.Data;
using System.Collections.Generic;

namespace WorkTimesheet.Entities.MetaData
{
    [Serializable()]

    public class NavigationMenu
    {
        public const string PROC_NAME_GET = "[dbo].[spGetAspNetNavigationMenu]";
        public const string PROC_NAME_STORE = "[dbo].[spStoreAspNetNavigationMenu]";
        public NavigationMenu() { }

        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Guid? ParentId { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string IconClass { get; set; }
        public int MenuOrder { get; set; }
        public Guid MenuUUID { get; set; }
        public int SiteId { get; set; }
        public bool IsExternalLink { get; set; }
        public string URL { get; set; }
        public bool FlagDeleted { get; set; }
        public int UserAction { get; set; }

        public static DataTable CreateDataTable()
        {
            DataTable table = new DataTable("ttAspNetNavigationMenu");
            table.Columns.Add("Id", typeof(Guid));
            table.Columns.Add("Title", typeof(string));
            table.Columns.Add("Description", typeof(string));
            table.Columns.Add("ParentId", typeof(Guid));
            table.Columns.Add("Controller", typeof(string));
            table.Columns.Add("Action", typeof(string));
            table.Columns.Add("IconClass", typeof(string));
            table.Columns.Add("MenuOrder", typeof(int));
            table.Columns.Add("MenuUUID", typeof(Guid));
            table.Columns.Add("SiteId", typeof(int));
            table.Columns.Add("IsExternalLink", typeof(bool));
            table.Columns.Add("URL", typeof(string));
            table.Columns.Add("FlagDeleted", typeof(bool));
            table.Columns.Add("UserAction", typeof(int));
            return table;
        }

        public DataTable ConvertToDataTable()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["Title"] = Title;
            row["Description"] = Description;
            if (ParentId == null)
                row["ParentId"] = DBNull.Value;
            else
                row["ParentId"] = ParentId.Value;
            row["Controller"] = Controller;
            row["Action"] = Action;
            row["IconClass"] = IconClass;
            row["MenuOrder"] = MenuOrder;
            row["MenuUUID"] = MenuUUID;
            row["SiteId"] = SiteId;
            row["IsExternalLink"] = IsExternalLink;
            row["URL"] = URL;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataRow ConvertToDataRow()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["Title"] = Title;
            row["Description"] = Description;
            row["ParentId"] = ParentId;
            row["Controller"] = Controller;
            row["Action"] = Action;
            row["IconClass"] = IconClass;
            row["MenuOrder"] = MenuOrder;
            row["MenuUUID"] = MenuUUID;
            row["SiteId"] = SiteId;
            row["IsExternalLink"] = IsExternalLink;
            row["URL"] = URL;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            return row;
        }
        public static DataTable ConvertToDataTable(List<NavigationMenu> data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            foreach (NavigationMenu item in data)
            {
                row = table.NewRow();
                row["Id"] = item.Id;
                row["Title"] = item.Title;
                row["Description"] = item.Description;
                row["ParentId"] = item.ParentId;
                row["Controller"] = item.Controller;
                row["Action"] = item.Action;
                row["IconClass"] = item.IconClass;
                row["MenuOrder"] = item.MenuOrder;
                row["MenuUUID"] = item.MenuUUID;
                row["SiteId"] = item.SiteId;
                row["IsExternalLink"] = item.IsExternalLink;
                row["URL"] = item.URL;
                row["FlagDeleted"] = item.FlagDeleted;
                row["UserAction"] = item.UserAction;
                table.Rows.Add(row);
            }
            return table;
        }

        public static DataTable ConvertToDataTable(NavigationMenu data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["Title"] = data.Title;
            row["Description"] = data.Description;
            row["ParentId"] = data.ParentId;
            row["Controller"] = data.Controller;
            row["Action"] = data.Action;
            row["IconClass"] = data.IconClass;
            row["MenuOrder"] = data.MenuOrder;
            row["MenuUUID"] = data.MenuUUID;
            row["SiteId"] = data.SiteId;
            row["IsExternalLink"] = data.IsExternalLink;
            row["URL"] = data.URL;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table)
        {
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["Title"] = Title;
            row["Description"] = Description;
            row["ParentId"] = ParentId;
            row["Controller"] = Controller;
            row["Action"] = Action;
            row["IconClass"] = IconClass;
            row["MenuOrder"] = MenuOrder;
            row["MenuUUID"] = MenuUUID;
            row["SiteId"] = SiteId;
            row["IsExternalLink"] = IsExternalLink;
            row["URL"] = URL;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table, NavigationMenu data)
        {
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["Title"] = data.Title;
            row["Description"] = data.Description;
            row["ParentId"] = data.ParentId;
            row["Controller"] = data.Controller;
            row["Action"] = data.Action;
            row["IconClass"] = data.IconClass;
            row["MenuOrder"] = data.MenuOrder;
            row["MenuUUID"] = data.MenuUUID;
            row["SiteId"] = data.SiteId;
            row["IsExternalLink"] = data.IsExternalLink;
            row["URL"] = data.URL;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }
    }
}
