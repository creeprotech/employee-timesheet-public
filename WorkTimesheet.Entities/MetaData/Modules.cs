using System;
using System.Data;
using System.Collections.Generic;

namespace WorkTimesheet.Entities.MetaData
{
    [Serializable()]

    public class Modules
    {
        public const string PROC_NAME_GET = "[dbo].[spGetAspNetModules]";
        public const string PROC_NAME_STORE = "[dbo].[spStoreAspNetModules]";
        public Modules() { }

        public Guid Id { get; set; }
        public string ModuleName { get; set; }
        public string Description { get; set; }
        public int ModuleValue { get; set; }
        public int ControlValue { get; set; }
        public int PermissionValue { get; set; }
        public bool FlagDeleted { get; set; }
        public int UserAction { get; set; }

        public static DataTable CreateDataTable()
        {
            DataTable table = new DataTable("ttAspNetModules");
            table.Columns.Add("Id", typeof(Guid));
            table.Columns.Add("ModuleName", typeof(string));
            table.Columns.Add("Description", typeof(string));
            table.Columns.Add("ModuleValue", typeof(int));
            table.Columns.Add("ControlValue", typeof(int));
            table.Columns.Add("PermissionValue", typeof(int));
            table.Columns.Add("FlagDeleted", typeof(bool));
            table.Columns.Add("UserAction", typeof(int));
            return table;
        }

        public DataTable ConvertToDataTable()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["ModuleName"] = ModuleName;
            row["Description"] = Description;
            row["ModuleValue"] = ModuleValue;
            row["ControlValue"] = ControlValue;
            row["PermissionValue"] = PermissionValue;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataRow ConvertToDataRow()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["ModuleName"] = ModuleName;
            row["Description"] = Description;
            row["ModuleValue"] = ModuleValue;
            row["ControlValue"] = ControlValue;
            row["PermissionValue"] = PermissionValue;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            return row;
        }
        public static DataTable ConvertToDataTable(List<Modules> data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            foreach (Modules item in data)
            {
                row = table.NewRow();
                row["Id"] = item.Id;
                row["ModuleName"] = item.ModuleName;
                row["Description"] = item.Description;
                row["ModuleValue"] = item.ModuleValue;
                row["ControlValue"] = item.ControlValue;
                row["PermissionValue"] = item.PermissionValue;
                row["FlagDeleted"] = item.FlagDeleted;
                row["UserAction"] = item.UserAction;
                table.Rows.Add(row);
            }
            return table;
        }

        public static DataTable ConvertToDataTable(Modules data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["ModuleName"] = data.ModuleName;
            row["Description"] = data.Description;
            row["ModuleValue"] = data.ModuleValue;
            row["ControlValue"] = data.ControlValue;
            row["PermissionValue"] = data.PermissionValue;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table)
        {
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["ModuleName"] = ModuleName;
            row["Description"] = Description;
            row["ModuleValue"] = ModuleValue;
            row["ControlValue"] = ControlValue;
            row["PermissionValue"] = PermissionValue;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table, Modules data)
        {
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["ModuleName"] = data.ModuleName;
            row["Description"] = data.Description;
            row["ModuleValue"] = data.ModuleValue;
            row["ControlValue"] = data.ControlValue;
            row["PermissionValue"] = data.PermissionValue;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }
    }
}
