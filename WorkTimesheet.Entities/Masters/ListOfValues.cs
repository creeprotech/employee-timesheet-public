using System;
using System.Data;
using System.Collections.Generic;

namespace WorkTimesheet.Entities.Masters
{
    [Serializable()]

    public class ListOfValues
    {
        public const string PROC_NAME_GET = "[dbo].[spGetListOfValues]";
        public const string PROC_NAME_STORE = "[dbo].[spStoreListOfValues]";
        public ListOfValues() { }

        public int Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string Label { get; set; }
        public string Value { get; set; }
        public int OrderValue { get; set; }
        public int SiteId { get; set; }
        public bool FlagDeleted { get; set; }
        public int UserAction { get; set; }

        public static DataTable CreateDataTable()
        {
            DataTable table = new DataTable("ttListOfValues");
            table.Columns.Add("Id", typeof(int));
            table.Columns.Add("Type", typeof(string));
            table.Columns.Add("Description", typeof(string));
            table.Columns.Add("Label", typeof(string));
            table.Columns.Add("Value", typeof(string));
            table.Columns.Add("OrderValue", typeof(int));
            table.Columns.Add("SiteId", typeof(int));
            table.Columns.Add("FlagDeleted", typeof(bool));
            table.Columns.Add("UserAction", typeof(int));
            return table;
        }

        public DataTable ConvertToDataTable()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["Type"] = Type;
            row["Description"] = Description;
            row["Label"] = Label;
            row["Value"] = Value;
            row["OrderValue"] = OrderValue;
            row["SiteId"] = SiteId;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataRow ConvertToDataRow()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["Type"] = Type;
            row["Description"] = Description;
            row["Label"] = Label;
            row["Value"] = Value;
            row["OrderValue"] = OrderValue;
            row["SiteId"] = SiteId;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            return row;
        }
        public static DataTable ConvertToDataTable(List<ListOfValues> data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            foreach (ListOfValues item in data)
            {
                row = table.NewRow();
                row["Id"] = item.Id;
                row["Type"] = item.Type;
                row["Description"] = item.Description;
                row["Label"] = item.Label;
                row["Value"] = item.Value;
                row["OrderValue"] = item.OrderValue;
                row["SiteId"] = item.SiteId;
                row["FlagDeleted"] = item.FlagDeleted;
                row["UserAction"] = item.UserAction;
                table.Rows.Add(row);
            }
            return table;
        }

        public static DataTable ConvertToDataTable(ListOfValues data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["Type"] = data.Type;
            row["Description"] = data.Description;
            row["Label"] = data.Label;
            row["Value"] = data.Value;
            row["OrderValue"] = data.OrderValue;
            row["SiteId"] = data.SiteId;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table)
        {
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["Type"] = Type;
            row["Description"] = Description;
            row["Label"] = Label;
            row["Value"] = Value;
            row["OrderValue"] = OrderValue;
            row["SiteId"] = SiteId;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table, ListOfValues data)
        {
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["Type"] = data.Type;
            row["Description"] = data.Description;
            row["Label"] = data.Label;
            row["Value"] = data.Value;
            row["OrderValue"] = data.OrderValue;
            row["SiteId"] = data.SiteId;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }
    }
}
