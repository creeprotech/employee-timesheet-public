﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkTimesheet.Entities.ValueDataType
{
    [Flags]
    public enum ModuleValueType : int
    {
        Masters = 1,
        Operations = 2,
        MetaData = 4,
        ProjectTask = 8,
        Timesheet = 16,
        Reports = 32,
        Settings = 64,
    }
}
