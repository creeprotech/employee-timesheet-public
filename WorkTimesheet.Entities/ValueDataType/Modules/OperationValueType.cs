﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkTimesheet.Entities.ValueDataType.Modules
{
    [Flags]
    public enum OperationValueType : int
    {
        Zone = 1,
        Groups = 2,
        Organization = 4,
        Roles = 8,
        Users = 16,
        RolePermissions = 32,
    }
}
