﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkTimesheet.Entities.ValueDataType.Modules
{
    [Flags]
    public enum ProjectTaskValueType : int
    {
        CurrentTask = 1,
        ProjectList = 2,
        ChangeTaskStatus = 4,
        GetAllTask = 8,
    }
}
