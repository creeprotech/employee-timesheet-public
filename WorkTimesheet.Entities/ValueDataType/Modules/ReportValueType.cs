﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkTimesheet.Entities.ValueDataType.Modules
{
    [Flags]
    public enum ReportValueType : int
    {
        TimesheetReports = 1,
        TaskReports = 2,
        GetAllTimesheetReports = 4,
    }
}
