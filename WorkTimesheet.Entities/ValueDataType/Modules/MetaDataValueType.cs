﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkTimesheet.Entities.ValueDataType.Modules
{
    [Flags]
    public enum MetaDataValueType : int
    {
        Modules = 1,
        Menus = 2,
    }
}
