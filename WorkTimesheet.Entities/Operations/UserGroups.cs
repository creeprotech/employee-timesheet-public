using System;
using System.Data;
using System.Collections.Generic;

namespace WorkTimesheet.Entities.Operations
{
    [Serializable()]

    public class UserGroups
    {
        public const string PROC_NAME_GET = "[dbo].[spGetAspNetUserGroups]";
        public const string PROC_NAME_STORE = "[dbo].[spStoreAspNetUserGroups]";
        public UserGroups() { }

        public int Id { get; set; }
        public string UserId { get; set; }
        public string GroupId { get; set; }
        public bool FlagDeleted { get; set; }
        public int UserAction { get; set; }

        public static DataTable CreateDataTable()
        {
            DataTable table = new DataTable("ttAspNetUserGroups");
            table.Columns.Add("Id", typeof(int));
            table.Columns.Add("UserId", typeof(string));
            table.Columns.Add("GroupId", typeof(string));
            table.Columns.Add("FlagDeleted", typeof(bool));
            table.Columns.Add("UserAction", typeof(int));
            return table;
        }

        public DataTable ConvertToDataTable()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["UserId"] = UserId;
            row["GroupId"] = GroupId;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataRow ConvertToDataRow()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["UserId"] = UserId;
            row["GroupId"] = GroupId;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            return row;
        }
        public static DataTable ConvertToDataTable(List<UserGroups> data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            foreach (UserGroups item in data)
            {
                row = table.NewRow();
                row["Id"] = item.Id;
                row["UserId"] = item.UserId;
                row["GroupId"] = item.GroupId;
                row["FlagDeleted"] = item.FlagDeleted;
                row["UserAction"] = item.UserAction;
                table.Rows.Add(row);
            }
            return table;
        }

        public static DataTable ConvertToDataTable(UserGroups data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["UserId"] = data.UserId;
            row["GroupId"] = data.GroupId;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table)
        {
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["UserId"] = UserId;
            row["GroupId"] = GroupId;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table, UserGroups data)
        {
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["UserId"] = data.UserId;
            row["GroupId"] = data.GroupId;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }
    }
}
