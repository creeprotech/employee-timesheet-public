using System;
using System.Data;
using System.Collections.Generic;

namespace WorkTimesheet.Entities.Operations
{
    [Serializable()]

    public class Groups
    {
        public const string PROC_NAME_GET = "[dbo].[spGetAspNetGroups]";
        public const string PROC_NAME_STORE = "[dbo].[spStoreAspNetGroups]";
        public Groups() { }

        public Guid Id { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }
        public int GroupType { get; set; }
        public Guid? ParentGroupId { get; set; }
        public Guid OrganizationUnitId { get; set; }
        public bool FlagDeleted { get; set; }
        public int UserAction { get; set; }

        public static DataTable CreateDataTable()
        {
            DataTable table = new DataTable("ttAspNetGroups");
            table.Columns.Add("Id", typeof(Guid));
            table.Columns.Add("GroupName", typeof(string));
            table.Columns.Add("Description", typeof(string));
            table.Columns.Add("GroupType", typeof(int));
            table.Columns.Add("ParentGroupId", typeof(Guid));
            table.Columns.Add("OrganizationUnitId", typeof(Guid));
            table.Columns.Add("FlagDeleted", typeof(bool));
            table.Columns.Add("UserAction", typeof(int));
            return table;
        }

        public DataTable ConvertToDataTable()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["GroupName"] = GroupName;
            row["Description"] = Description;
            row["GroupType"] = GroupType;
            if (ParentGroupId == null) { row["ParentGroupId"] = DBNull.Value; }
            else { row["ParentGroupId"] = ParentGroupId; }
            //row["ParentGroupId"] = (ParentGroupId == Guid.Empty ? DBNull.Value : ParentGroupId.Value);
            row["OrganizationUnitId"] = OrganizationUnitId;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataRow ConvertToDataRow()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["GroupName"] = GroupName;
            row["Description"] = Description;
            row["GroupType"] = GroupType;
            row["ParentGroupId"] = ParentGroupId;
            row["OrganizationUnitId"] = OrganizationUnitId;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            return row;
        }
        public static DataTable ConvertToDataTable(List<Groups> data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            foreach (Groups item in data)
            {
                row = table.NewRow();
                row["Id"] = item.Id;
                row["GroupName"] = item.GroupName;
                row["Description"] = item.Description;
                row["GroupType"] = item.GroupType;
                row["ParentGroupId"] = item.ParentGroupId;
                row["OrganizationUnitId"] = item.OrganizationUnitId;
                row["FlagDeleted"] = item.FlagDeleted;
                row["UserAction"] = item.UserAction;
                table.Rows.Add(row);
            }
            return table;
        }

        public static DataTable ConvertToDataTable(Groups data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["GroupName"] = data.GroupName;
            row["Description"] = data.Description;
            row["GroupType"] = data.GroupType;
            row["ParentGroupId"] = data.ParentGroupId;
            row["OrganizationUnitId"] = data.OrganizationUnitId;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table)
        {
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["GroupName"] = GroupName;
            row["Description"] = Description;
            row["GroupType"] = GroupType;
            row["ParentGroupId"] = ParentGroupId;
            row["OrganizationUnitId"] = OrganizationUnitId;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table, Groups data)
        {
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["GroupName"] = data.GroupName;
            row["Description"] = data.Description;
            row["GroupType"] = data.GroupType;
            row["ParentGroupId"] = data.ParentGroupId;
            row["OrganizationUnitId"] = data.OrganizationUnitId;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }
    }
}
