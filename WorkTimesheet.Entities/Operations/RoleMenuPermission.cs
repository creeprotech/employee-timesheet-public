using System;
using System.Data;
using System.Collections.Generic;

namespace WorkTimesheet.Entities.Operations
{
    [Serializable()]

    public class RoleMenuPermission
    {
        public const string PROC_NAME_GET = "[dbo].[spGetAspNetRoleMenuPermission]";
        public const string PROC_NAME_STORE = "[dbo].[spStoreAspNetRoleMenuPermission]";
        public RoleMenuPermission() { }

        public int Id { get; set; }
        public Guid PermissionId { get; set; }
        public int PermissionValue { get; set; }
        public string RoleId { get; set; }
        public bool FlagDeleted { get; set; }
        public int UserAction { get; set; }

        public static DataTable CreateDataTable()
        {
            DataTable table = new DataTable("ttAspNetRoleMenuPermission");
            table.Columns.Add("Id", typeof(int));
            table.Columns.Add("PermissionId", typeof(Guid));
            table.Columns.Add("PermissionValue", typeof(int));
            table.Columns.Add("RoleId", typeof(string));
            table.Columns.Add("FlagDeleted", typeof(bool));
            table.Columns.Add("UserAction", typeof(int));
            return table;
        }

        public DataTable ConvertToDataTable()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["PermissionId"] = PermissionId;
            row["PermissionValue"] = PermissionValue;
            row["RoleId"] = RoleId;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataRow ConvertToDataRow()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["PermissionId"] = PermissionId;
            row["PermissionValue"] = PermissionValue;
            row["RoleId"] = RoleId;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            return row;
        }
        public static DataTable ConvertToDataTable(List<RoleMenuPermission> data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            foreach (RoleMenuPermission item in data)
            {
                row = table.NewRow();
                row["Id"] = item.Id;
                row["PermissionId"] = item.PermissionId;
                row["PermissionValue"] = item.PermissionValue;
                row["RoleId"] = item.RoleId;
                row["FlagDeleted"] = item.FlagDeleted;
                row["UserAction"] = item.UserAction;
                table.Rows.Add(row);
            }
            return table;
        }

        public static DataTable ConvertToDataTable(RoleMenuPermission data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["PermissionId"] = data.PermissionId;
            row["PermissionValue"] = data.PermissionValue;
            row["RoleId"] = data.RoleId;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table)
        {
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["PermissionId"] = PermissionId;
            row["PermissionValue"] = PermissionValue;
            row["RoleId"] = RoleId;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table, RoleMenuPermission data)
        {
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["PermissionId"] = data.PermissionId;
            row["PermissionValue"] = data.PermissionValue;
            row["RoleId"] = data.RoleId;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }
    }
}
