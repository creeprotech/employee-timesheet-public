using System;
using System.Data;
using System.Collections.Generic;

namespace WorkTimesheet.Entities.Operations
{
    [Serializable()]

    public class UserRoles
    {
        public const string PROC_NAME_GET = "[dbo].[spGetAspNetUserRoles]";
        public const string PROC_NAME_STORE = "[dbo].[spStoreAspNetUserRoles]";
        public UserRoles() { }

        public int Id { get; set; }
        public string UserId { get; set; }
        public string RoleId { get; set; }
        public bool FlagDeleted { get; set; }
        public int UserAction { get; set; }

        public static DataTable CreateDataTable()
        {
            DataTable table = new DataTable("ttAspNetUserRoles");
            table.Columns.Add("Id", typeof(int));
            table.Columns.Add("UserId", typeof(string));
            table.Columns.Add("RoleId", typeof(string));
            table.Columns.Add("FlagDeleted", typeof(bool));
            table.Columns.Add("UserAction", typeof(int));
            return table;
        }

        public DataTable ConvertToDataTable()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["UserId"] = UserId;
            row["RoleId"] = RoleId;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataRow ConvertToDataRow()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["UserId"] = UserId;
            row["RoleId"] = RoleId;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            return row;
        }
        public static DataTable ConvertToDataTable(List<UserRoles> data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            foreach (UserRoles item in data)
            {
                row = table.NewRow();
                row["Id"] = item.Id;
                row["UserId"] = item.UserId;
                row["RoleId"] = item.RoleId;
                row["FlagDeleted"] = item.FlagDeleted;
                row["UserAction"] = item.UserAction;
                table.Rows.Add(row);
            }
            return table;
        }

        public static DataTable ConvertToDataTable(UserRoles data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["UserId"] = data.UserId;
            row["RoleId"] = data.RoleId;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table)
        {
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["UserId"] = UserId;
            row["RoleId"] = RoleId;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table, UserRoles data)
        {
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["UserId"] = data.UserId;
            row["RoleId"] = data.RoleId;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }
    }
}
