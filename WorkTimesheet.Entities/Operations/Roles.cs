using System;
using System.Data;
using System.Collections.Generic;

namespace WorkTimesheet.Entities.Operations
{
    [Serializable()]

    public class Roles
    {
        public const string PROC_NAME_GET = "[dbo].[spGetAspNetRoles]";
        public const string PROC_NAME_STORE = "[dbo].[spStoreAspNetRoles]";
        public Roles() { }

        public string Id { get; set; }
        public string Name { get; set; }
        public string NormalizedName { get; set; }
        public string ConcurrencyStamp { get; set; }
        public bool FlagDeleted { get; set; }
        public int UserAction { get; set; }

        public static DataTable CreateDataTable()
        {
            DataTable table = new DataTable("ttAspNetRoles");
            table.Columns.Add("Id", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("NormalizedName", typeof(string));
            table.Columns.Add("ConcurrencyStamp", typeof(string));
            table.Columns.Add("FlagDeleted", typeof(bool));
            table.Columns.Add("UserAction", typeof(int));
            return table;
        }

        public DataTable ConvertToDataTable()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["Name"] = Name;
            row["NormalizedName"] = NormalizedName;
            row["ConcurrencyStamp"] = ConcurrencyStamp;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataRow ConvertToDataRow()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["Name"] = Name;
            row["NormalizedName"] = NormalizedName;
            row["ConcurrencyStamp"] = ConcurrencyStamp;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            return row;
        }
        public static DataTable ConvertToDataTable(List<Roles> data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            foreach (Roles item in data)
            {
                row = table.NewRow();
                row["Id"] = item.Id;
                row["Name"] = item.Name;
                row["NormalizedName"] = item.NormalizedName;
                row["ConcurrencyStamp"] = item.ConcurrencyStamp;
                row["FlagDeleted"] = item.FlagDeleted;
                row["UserAction"] = item.UserAction;
                table.Rows.Add(row);
            }
            return table;
        }

        public static DataTable ConvertToDataTable(Roles data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["Name"] = data.Name;
            row["NormalizedName"] = data.NormalizedName;
            row["ConcurrencyStamp"] = data.ConcurrencyStamp;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table)
        {
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["Name"] = Name;
            row["NormalizedName"] = NormalizedName;
            row["ConcurrencyStamp"] = ConcurrencyStamp;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table, Roles data)
        {
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["Name"] = data.Name;
            row["NormalizedName"] = data.NormalizedName;
            row["ConcurrencyStamp"] = data.ConcurrencyStamp;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }
    }
}
