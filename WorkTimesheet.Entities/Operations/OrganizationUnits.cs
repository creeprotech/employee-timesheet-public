using System;
using System.Data;
using System.Collections.Generic;

namespace WorkTimesheet.Entities.Operations
{
    [Serializable()]

    public class OrganizationUnits
    {
        public const string PROC_NAME_GET = "[dbo].[spGetAspNetOrganizationUnits]";
        public const string PROC_NAME_STORE = "[dbo].[spStoreAspNetOrganizationUnits]";
        public OrganizationUnits() { }

        public Guid Id { get; set; }
        public string OUName { get; set; }
        public string Description { get; set; }
        public Guid? ParentOU { get; set; }
        public bool FlagDeleted { get; set; }
        public int UserAction { get; set; }

        public static DataTable CreateDataTable()
        {
            DataTable table = new DataTable("ttAspNetOrganizationUnits");
            table.Columns.Add("Id", typeof(Guid));
            table.Columns.Add("OUName", typeof(string));
            table.Columns.Add("Description", typeof(string));
            table.Columns.Add("ParentOU", typeof(Guid));
            table.Columns.Add("FlagDeleted", typeof(bool));
            table.Columns.Add("UserAction", typeof(int));
            return table;
        }

        public DataTable ConvertToDataTable()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["OUName"] = OUName;
            row["Description"] = Description;
            row["ParentOU"] = ParentOU;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataRow ConvertToDataRow()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["OUName"] = OUName;
            row["Description"] = Description;
            row["ParentOU"] = ParentOU;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            return row;
        }
        public static DataTable ConvertToDataTable(List<OrganizationUnits> data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            foreach (OrganizationUnits item in data)
            {
                row = table.NewRow();
                row["Id"] = item.Id;
                row["OUName"] = item.OUName;
                row["Description"] = item.Description;
                row["ParentOU"] = item.ParentOU;
                row["FlagDeleted"] = item.FlagDeleted;
                row["UserAction"] = item.UserAction;
                table.Rows.Add(row);
            }
            return table;
        }

        public static DataTable ConvertToDataTable(OrganizationUnits data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["OUName"] = data.OUName;
            row["Description"] = data.Description;
            row["ParentOU"] = data.ParentOU;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table)
        {
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["OUName"] = OUName;
            row["Description"] = Description;
            row["ParentOU"] = ParentOU;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table, OrganizationUnits data)
        {
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["OUName"] = data.OUName;
            row["Description"] = data.Description;
            row["ParentOU"] = data.ParentOU;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }
    }
}
