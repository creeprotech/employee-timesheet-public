using System;
using System.Data;
using System.Collections.Generic;

namespace WorkTimesheet.Entities.Operations
{
    [Serializable()]

    public class Users
    {
        public const string PROC_NAME_GET = "[dbo].[spGetAspNetUsers]";
        public const string PROC_NAME_STORE = "[dbo].[spStoreAspNetUsers]";
        public Users() { }

        public string Id { get; set; }
        public string UserName { get; set; }
        public string NormalizedUserName { get; set; }
        public string Email { get; set; }
        public string NormalizedEmail { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public DateTime? LockoutEnd { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public DateTime? LastLogOn { get; set; }
        public DateTime? PasswordModifiedTime { get; set; }
        public bool FlagDeleted { get; set; }
        public int UserAction { get; set; }

        public static DataTable CreateDataTable()
        {
            DataTable table = new DataTable("ttAspNetUsers");
            table.Columns.Add("Id", typeof(string));
            table.Columns.Add("UserName", typeof(string));
            table.Columns.Add("NormalizedUserName", typeof(string));
            table.Columns.Add("Email", typeof(string));
            table.Columns.Add("NormalizedEmail", typeof(string));
            table.Columns.Add("EmailConfirmed", typeof(bool));
            table.Columns.Add("PasswordHash", typeof(string));
            table.Columns.Add("SecurityStamp", typeof(string));
            table.Columns.Add("ConcurrencyStamp", typeof(string));
            table.Columns.Add("PhoneNumber", typeof(string));
            table.Columns.Add("PhoneNumberConfirmed", typeof(bool));
            table.Columns.Add("TwoFactorEnabled", typeof(bool));
            table.Columns.Add("LockoutEnd", typeof(DateTime));
            table.Columns.Add("LockoutEnabled", typeof(bool));
            table.Columns.Add("AccessFailedCount", typeof(int));
            table.Columns.Add("LastLogOn", typeof(DateTime));
            table.Columns.Add("PasswordModifiedTime", typeof(DateTime));
            table.Columns.Add("FlagDeleted", typeof(bool));
            table.Columns.Add("UserAction", typeof(int));
            return table;
        }

        public DataTable ConvertToDataTable()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["UserName"] = UserName;
            row["NormalizedUserName"] = NormalizedUserName;
            row["Email"] = Email;
            row["NormalizedEmail"] = NormalizedEmail;
            row["EmailConfirmed"] = EmailConfirmed;
            row["PasswordHash"] = PasswordHash;
            row["SecurityStamp"] = SecurityStamp;
            row["ConcurrencyStamp"] = ConcurrencyStamp;
            row["PhoneNumber"] = PhoneNumber;
            row["PhoneNumberConfirmed"] = PhoneNumberConfirmed;
            row["TwoFactorEnabled"] = TwoFactorEnabled;
            row["LockoutEnd"] = DBNull.Value;// (LockoutEnd == null ? new Nullable<DateTime>() : LockoutEnd.Value);
            row["LockoutEnabled"] = LockoutEnabled;
            row["AccessFailedCount"] = AccessFailedCount;
            row["LastLogOn"] = DBNull.Value;//(LastLogOn == null ? new Nullable<DateTime>() : LastLogOn.Value);
            row["PasswordModifiedTime"] = DBNull.Value;//(PasswordModifiedTime == null ? new Nullable<DateTime>() : PasswordModifiedTime.Value); 
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataRow ConvertToDataRow()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["UserName"] = UserName;
            row["NormalizedUserName"] = NormalizedUserName;
            row["Email"] = Email;
            row["NormalizedEmail"] = NormalizedEmail;
            row["EmailConfirmed"] = EmailConfirmed;
            row["PasswordHash"] = PasswordHash;
            row["SecurityStamp"] = SecurityStamp;
            row["ConcurrencyStamp"] = ConcurrencyStamp;
            row["PhoneNumber"] = PhoneNumber;
            row["PhoneNumberConfirmed"] = PhoneNumberConfirmed;
            row["TwoFactorEnabled"] = TwoFactorEnabled;
            row["LockoutEnd"] = LockoutEnd;
            row["LockoutEnabled"] = LockoutEnabled;
            row["AccessFailedCount"] = AccessFailedCount;
            row["LastLogOn"] = LastLogOn;
            row["PasswordModifiedTime"] = PasswordModifiedTime;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            return row;
        }
        public static DataTable ConvertToDataTable(List<Users> data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            foreach (Users item in data)
            {
                row = table.NewRow();
                row["Id"] = item.Id;
                row["UserName"] = item.UserName;
                row["NormalizedUserName"] = item.NormalizedUserName;
                row["Email"] = item.Email;
                row["NormalizedEmail"] = item.NormalizedEmail;
                row["EmailConfirmed"] = item.EmailConfirmed;
                row["PasswordHash"] = item.PasswordHash;
                row["SecurityStamp"] = item.SecurityStamp;
                row["ConcurrencyStamp"] = item.ConcurrencyStamp;
                row["PhoneNumber"] = item.PhoneNumber;
                row["PhoneNumberConfirmed"] = item.PhoneNumberConfirmed;
                row["TwoFactorEnabled"] = item.TwoFactorEnabled;
                row["LockoutEnd"] = item.LockoutEnd;
                row["LockoutEnabled"] = item.LockoutEnabled;
                row["AccessFailedCount"] = item.AccessFailedCount;
                row["LastLogOn"] = item.LastLogOn;
                row["PasswordModifiedTime"] = item.PasswordModifiedTime;
                row["FlagDeleted"] = item.FlagDeleted;
                row["UserAction"] = item.UserAction;
                table.Rows.Add(row);
            }
            return table;
        }

        public static DataTable ConvertToDataTable(Users data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["UserName"] = data.UserName;
            row["NormalizedUserName"] = data.NormalizedUserName;
            row["Email"] = data.Email;
            row["NormalizedEmail"] = data.NormalizedEmail;
            row["EmailConfirmed"] = data.EmailConfirmed;
            row["PasswordHash"] = data.PasswordHash;
            row["SecurityStamp"] = data.SecurityStamp;
            row["ConcurrencyStamp"] = data.ConcurrencyStamp;
            row["PhoneNumber"] = data.PhoneNumber;
            row["PhoneNumberConfirmed"] = data.PhoneNumberConfirmed;
            row["TwoFactorEnabled"] = data.TwoFactorEnabled;
            row["LockoutEnd"] = data.LockoutEnd;
            row["LockoutEnabled"] = data.LockoutEnabled;
            row["AccessFailedCount"] = data.AccessFailedCount;
            row["LastLogOn"] = data.LastLogOn;
            row["PasswordModifiedTime"] = data.PasswordModifiedTime;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table)
        {
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["UserName"] = UserName;
            row["NormalizedUserName"] = NormalizedUserName;
            row["Email"] = Email;
            row["NormalizedEmail"] = NormalizedEmail;
            row["EmailConfirmed"] = EmailConfirmed;
            row["PasswordHash"] = PasswordHash;
            row["SecurityStamp"] = SecurityStamp;
            row["ConcurrencyStamp"] = ConcurrencyStamp;
            row["PhoneNumber"] = PhoneNumber;
            row["PhoneNumberConfirmed"] = PhoneNumberConfirmed;
            row["TwoFactorEnabled"] = TwoFactorEnabled;
            row["LockoutEnd"] = LockoutEnd;
            row["LockoutEnabled"] = LockoutEnabled;
            row["AccessFailedCount"] = AccessFailedCount;
            row["LastLogOn"] = LastLogOn;
            row["PasswordModifiedTime"] = PasswordModifiedTime;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table, Users data)
        {
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["UserName"] = data.UserName;
            row["NormalizedUserName"] = data.NormalizedUserName;
            row["Email"] = data.Email;
            row["NormalizedEmail"] = data.NormalizedEmail;
            row["EmailConfirmed"] = data.EmailConfirmed;
            row["PasswordHash"] = data.PasswordHash;
            row["SecurityStamp"] = data.SecurityStamp;
            row["ConcurrencyStamp"] = data.ConcurrencyStamp;
            row["PhoneNumber"] = data.PhoneNumber;
            row["PhoneNumberConfirmed"] = data.PhoneNumberConfirmed;
            row["TwoFactorEnabled"] = data.TwoFactorEnabled;
            row["LockoutEnd"] = data.LockoutEnd;
            row["LockoutEnabled"] = data.LockoutEnabled;
            row["AccessFailedCount"] = data.AccessFailedCount;
            row["LastLogOn"] = data.LastLogOn;
            row["PasswordModifiedTime"] = data.PasswordModifiedTime;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }
    }
}
