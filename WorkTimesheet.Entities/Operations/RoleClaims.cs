using System;
using System.Data;
using System.Collections.Generic;

namespace WorkTimesheet.Entities.Operations
{
    [Serializable()]

    public class RoleClaims
    {
        public const string PROC_NAME_GET = "[dbo].[spGetAspNetRoleClaims]";
        public const string PROC_NAME_STORE = "[dbo].[spStoreAspNetRoleClaims]";
        public RoleClaims() { }

        public int Id { get; set; }
        public string RoleId { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
        public bool FlagDeleted { get; set; }
        public int UserAction { get; set; }

        public static DataTable CreateDataTable()
        {
            DataTable table = new DataTable("ttAspNetRoleClaims");
            table.Columns.Add("Id", typeof(int));
            table.Columns.Add("RoleId", typeof(string));
            table.Columns.Add("ClaimType", typeof(string));
            table.Columns.Add("ClaimValue", typeof(string));
            table.Columns.Add("FlagDeleted", typeof(bool));
            table.Columns.Add("UserAction", typeof(int));
            return table;
        }

        public DataTable ConvertToDataTable()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["RoleId"] = RoleId;
            row["ClaimType"] = ClaimType;
            row["ClaimValue"] = ClaimValue;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataRow ConvertToDataRow()
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["RoleId"] = RoleId;
            row["ClaimType"] = ClaimType;
            row["ClaimValue"] = ClaimValue;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            return row;
        }
        public static DataTable ConvertToDataTable(List<RoleClaims> data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            foreach (RoleClaims item in data)
            {
                row = table.NewRow();
                row["Id"] = item.Id;
                row["RoleId"] = item.RoleId;
                row["ClaimType"] = item.ClaimType;
                row["ClaimValue"] = item.ClaimValue;
                row["FlagDeleted"] = item.FlagDeleted;
                row["UserAction"] = item.UserAction;
                table.Rows.Add(row);
            }
            return table;
        }

        public static DataTable ConvertToDataTable(RoleClaims data)
        {
            DataTable table = CreateDataTable();
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["RoleId"] = data.RoleId;
            row["ClaimType"] = data.ClaimType;
            row["ClaimValue"] = data.ClaimValue;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table)
        {
            DataRow row = table.NewRow();
            row["Id"] = Id;
            row["RoleId"] = RoleId;
            row["ClaimType"] = ClaimType;
            row["ClaimValue"] = ClaimValue;
            row["FlagDeleted"] = FlagDeleted;
            row["UserAction"] = UserAction;
            table.Rows.Add(row);
            return table;
        }

        public DataTable AppendToDataTable(DataTable table, RoleClaims data)
        {
            DataRow row = table.NewRow();
            row["Id"] = data.Id;
            row["RoleId"] = data.RoleId;
            row["ClaimType"] = data.ClaimType;
            row["ClaimValue"] = data.ClaimValue;
            row["FlagDeleted"] = data.FlagDeleted;
            row["UserAction"] = data.UserAction;
            table.Rows.Add(row);
            return table;
        }
    }
}
